
1. 项目目的

   让同学们（尤其是没有互联网开发经验，OA/MIS）有一个熟悉各种组件的使用场景的一个项目。

2. 项目由来

   1. CRUD  mybatis
   2. 数据  90%修改
   3. 微服务，单机的理解范畴
   4. 各种组件用的有问题
   
   项目结构 35个父+子模块。

   1w-1.5w QPS

3. 项目所涉及到的技术概览

   SpringBoot+微服务
   Dubbo 粘合 Netty http
   Nacos 注册中心+配置管理
   Elasticsearch  商品搜索
   Redis          订单缓存，商品缓存，库存缓存
   Zookeeper
   RabbitMQ       异步落地
   Kafka          消息 数据流
   Maxwell
   SpringCloud Gateway       统一路由+限流+验证过滤
   SpringCloud Nacos Config
   SpringCloud Stream Kafka
   SpringCloud Stream RabbitMQ
   Openresty                 动静分离
   shardingsphere
   Mybatis  Generator（entity/xml/dao）
   
   dao       - Maven项目。mybatis 的xml文件，dao interface
   entity    - Maven项目。表的实体映射 TbOrder
   interface - Maven项目。业务接口
   service   - springboot项目


4. 具体细节
     从搜索-商详-下单-支付
	 
	 搜索

	 Dubbo 注册，暴露，调用。
	 
	 Dubbo暴露实现，还是暴露接口
	 
	 暴露接口，最小依赖。Interface
	 必须要有转换类，暴露给调用者只是转换类  存根类Interface
	 
	 jedis.eval Long  Integer CastException
	 
	 http://localhost/front/     ====> http://localhost:8080/front/    9090
	 http://localhost/backend/   ====> http://localhost:8080/backend/  9099
	 
	 192.168.0.100:8080 backend/
	 192.168.0.100:8081 backend/
	 192.168.0.100:8082 backend/
	 192.168.0.100:8083 backend/
	 192.168.0.100:8084 backend/
	 
	 https://resource.smartisan.com/resource/63ea40e5c64db1c6b1d480c48fe01272.jpg,
	 https://resource.smartisan.com/resource/4b8d00ab6ba508a977a475988e0fdb53.jpg,
	 https://resource.smartisan.com/resource/87ea3888491d172b7d7a0e78e4294b4b.jpg,
	 https://resource.smartisan.com/resource/8d30265188ddd1ba05e34f669c5dcf1c.jpg

	 
	 
	 

