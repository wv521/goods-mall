package ai.yunxi.mall.pay.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
public class Payment implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Long orderId;
    private Long userId;
    private BigDecimal payPrice;
    private BigDecimal postFee;
    private Date createTime;
    private Date updateTime;
    private String status;
    private String finished;
}
