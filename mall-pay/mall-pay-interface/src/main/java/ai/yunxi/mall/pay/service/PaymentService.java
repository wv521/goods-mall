package ai.yunxi.mall.pay.service;

import ai.yunxi.mall.pay.service.dto.Payment;

public interface PaymentService {

    public int createPayment(Payment payment);
}
