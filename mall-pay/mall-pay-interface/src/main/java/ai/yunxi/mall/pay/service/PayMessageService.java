package ai.yunxi.mall.pay.service;

import ai.yunxi.mall.pay.service.dto.Payment;

public interface PayMessageService {

    public boolean send(Payment payment);
}
