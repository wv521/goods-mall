package ai.yunxi.mall.pay.service.impl;

import ai.yunxi.mall.common.exception.CommonException;
import ai.yunxi.mall.pay.domain.entity.TbPayment;
import ai.yunxi.mall.pay.domain.mapper.TbPaymentMapper;
import ai.yunxi.mall.pay.service.PaymentService;
import ai.yunxi.mall.pay.service.dto.Payment;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Resource
    private TbPaymentMapper tbPaymentMapper;

    @Override
    public int createPayment(Payment payment) {
        TbPayment tbPayment = new TbPayment();
        BeanUtils.copyProperties(payment, tbPayment);
        return tbPaymentMapper.insert(tbPayment);
    }
}
