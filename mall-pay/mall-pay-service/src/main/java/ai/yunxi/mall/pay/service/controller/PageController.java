package ai.yunxi.mall.pay.service.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/pay")
public class PageController {

    @RequestMapping("/success")
    public String success(){
        return "success";
    }
}