package ai.yunxi.mall.pay.service.impl;

import ai.yunxi.mall.pay.service.PayMessageService;
import ai.yunxi.mall.pay.service.dto.Payment;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@EnableBinding({Source.class})
public class PayMessageServiceImpl implements PayMessageService {

    @Resource
    private Source source;

    @Override
    public boolean send(Payment payment) {
        ObjectMapper om = new ObjectMapper();
        String content = null;

        try {
            content = om.writeValueAsString(payment);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        if (StringUtils.isBlank(content))
            return false;
        else {
            Message message = MessageBuilder.withPayload(content).build();
            return source.output().send(message);
        }
    }
}
