package ai.yunxi.mall.pay.service.controller;

import ai.yunxi.mall.common.exception.CommonException;
import ai.yunxi.mall.common.utils.CommonResult;
import ai.yunxi.mall.common.utils.ResultCode;
import ai.yunxi.mall.core.service.UserOrderService;
import ai.yunxi.mall.core.service.dto.Order;
import ai.yunxi.mall.pay.service.AlipayConfig;
import ai.yunxi.mall.pay.service.PayMessageService;
import ai.yunxi.mall.pay.service.PaymentService;
import ai.yunxi.mall.pay.service.dto.Payment;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradeCloseRequest;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.response.AlipayTradePagePayResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.shardingsphere.core.strategy.keygen.SnowflakeShardingKeyGenerator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping(value = "/pay/alipay")
public class PayController {

    @DubboReference
    private UserOrderService userOrderService;

    @Resource
    private PaymentService paymentService;

    @Resource
    private PayMessageService payMessageService;

    @Resource
    private AlipayClient alipayClient;

    @Resource
    private AlipayConfig alipayConfig;

    /**
     * 支付宝支付
     * 无返回值，执行成功后回写结果
     *
     * @param orderId
     * @param response
     */
    @PostMapping("/payment")
    public void payment(Long orderId, HttpServletResponse response) {
        // 获取订单
        Order order = userOrderService.getFromCache(orderId);
        if (order != null) {
            // 先初始化支付信息
            Payment payment = new Payment();
            SnowflakeShardingKeyGenerator keyGenerator = new SnowflakeShardingKeyGenerator();
            payment.setId((Long) keyGenerator.generateKey());
            payment.setOrderId(orderId);
            payment.setUserId(order.getUserId());
            payment.setPayPrice(order.getPayment());
            payment.setPostFee(order.getPostFee());
            payment.setCreateTime(new Date());
            payment.setStatus("0");
            payment.setFinished("0");
//            int rtnVal = paymentService.createPayment(payment);
//            if (rtnVal <= 0) {
//                throw new CommonException("创建支付订单失败 orderId:" + orderId);
//            }

            // 支付金额
            BigDecimal money = order.getPayment().add(order.getPostFee());
            // 设置请求参数
            AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
            // 同步通知页面
            alipayRequest.setReturnUrl(alipayConfig.getReturnUrl());
            // 异步通知页面
            alipayRequest.setNotifyUrl(alipayConfig.getNotifyUrl());
            // 封装调用参数
            Map<String, String> map = new HashMap<>();
            map.put("out_trade_no", String.valueOf(order.getOrderId()));
            map.put("total_amount", String.valueOf(money.doubleValue()));
            map.put("subject", order.getItems().get(0).getTitle());
            map.put("body", order.getItems().get(0).getTitle());
            map.put("product_code", "FAST_INSTANT_TRADE_PAY");

            try {
                ObjectMapper om = new ObjectMapper();
                alipayRequest.setBizContent(om.writeValueAsString(map));
                response.setContentType("text/html;charset=utf-8");
                // 生成支付结果页面
                AlipayTradePagePayResponse alipayResponse = alipayClient.pageExecute(alipayRequest);
                if (alipayResponse.isSuccess()) {
                    String result = alipayResponse.getBody();
                    response.getWriter().write(result);
                } else {
                    log.error("支付订单失败，错误信息：{}", alipayResponse.getSubMsg());
                    response.getWriter().write("error");
                }
            } catch (Exception e) {
                log.error("支付订单出错，异常信息：{}", e.getMessage());
                e.printStackTrace();
            }
        }
    }

    /**
     * 付款完成后进行自动跳转（只会进行一次）
     */
    @GetMapping("/return")
    public void alipayReturn(HttpServletRequest request, HttpServletResponse response) {
        // 获取参数
        Map<String, String> params = getPayParams(request);
        try {
            // 验证订单
            if (validateOrder(params)) {
                // 验证成功后，修改订单状态为已支付
                String orderId = params.get("out_trade_no");
                // 获取支付宝交易流水号
                String tradeNo = params.get("trade_no");
                // 更新订单状态
                userOrderService.changeStatus(Long.valueOf(orderId),
                        ResultCode.ALIPAY_TRADE_SUCCESS.getCode());
                //orderInfoService.changeStatus(orderId, "TRADE_SUCCESS", tradeNo);
                response.sendRedirect("/pay/success");
            } else {
                log.error("支付宝同步方法验证失败");
                response.getWriter().write("支付验证失败");
            }
        } catch (Exception e) {
            log.error("支付宝同步方法出错，异常信息：{}", e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 服务器异步通知，获取支付宝POST过来反馈信息
     */
    @PostMapping("/notify")
    public void alipayNotify(HttpServletRequest request, HttpServletResponse response) {
        // 获取参数
        Map<String, String> params = getPayParams(request);
        try {
            // 验证订单成功
            if (validateOrder(params)) {
                //商户订单号
                String orderId = params.get("out_trade_no");
                //支付宝交易号
                String tradeNo = params.get("trade_no");
                //交易状态
                String tradeStatus = params.get("trade_status");

                switch (tradeStatus) {
                    case "WAIT_BUYER_PAY":
                        // 交易创建
                        //orderInfoService.changeStatus(orderId, tradeStatus);
                        break;
                    case "TRADE_CLOSED":
                        // 交易关闭。交易创建但未付款，或付款后全部退款
                        System.out.println("交易关闭..........");
                        //orderInfoService.changeStatus(orderId, tradeStatus);
                        break;
                    case "TRADE_FINISHED":
                        // 交易完成。超过退款日期
                        System.out.println("交易完成..........");
                        // orderInfoService.changeStatus(orderId, tradeStatus);
                        break;
                    case "TRADE_SUCCESS":
                        // 付款成功
                        System.out.println("交易成功..........");
                        // orderInfoService.changeStatus(orderId, tradeStatus, tradeNo);
                        break;
                    default:
                        break;
                }
                response.getWriter().write("success");
            } else {
                log.error("支付异步方法验证失败，错误信息：{}", AlipaySignature.getSignCheckContentV1(params));
                response.getWriter().write("fail");
            }
        } catch (Exception e) {
            log.error("支付异步方法出错，异常信息：{}", e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 交易查询
     *
     * @param orderId  订单ID
     * @param alipayNo 支付宝交易号
     */
    @PostMapping("/query")
    @ResponseBody
    public CommonResult<Object> queryOrder(String orderId, String alipayNo) {
        // 封装请求参数
        AlipayTradeQueryRequest alipayRequest = new AlipayTradeQueryRequest();
        Map<String, String> map = new HashMap<>();
        map.put("out_trade_no", orderId);
        map.put("trade_no", alipayNo);

        try {
            ObjectMapper om = new ObjectMapper();
            alipayRequest.setBizContent(om.writeValueAsString(map));
            // 发送请求
            String json = alipayClient.execute(alipayRequest).getBody();
            Map<String, Object> resultMap = om.readValue(json, Map.class);
            Map<String, String> responseMap = (Map) resultMap.get("alipay_trade_query_response");

            // 获取返回状态码
            String code = responseMap.get("code");
            if ("10000".equals(code)) {
                // 获取查询结果
                String outTradeNo = responseMap.get("out_trade_no");
                Order order = userOrderService.getOrder(Long.valueOf(outTradeNo));
                Map<String, Object> result = new HashMap<>(16);
                result.put("orderInfo", order);
                result.put("buyer_logon_id", responseMap.get("buyer_logon_id"));
                result.put("trade_status", responseMap.get("trade_status"));
                return CommonResult.success(result);
            } else {
                log.error("支付宝交易查询错误，错误码：{}，错误信息：{}", code, responseMap.get("sub_msg"));
                return CommonResult.error(ResultCode.ALIPAY_QUERY_ERROR);
            }
        } catch (Exception e) {
            log.error("支付宝交易查询出错，异常信息：{}", e.getMessage());
            e.printStackTrace();
        }

        return CommonResult.error(ResultCode.ALIPAY_QUERY_ERROR);
    }

    /**
     * 关闭交易
     *
     * @param orderId
     * @param alipayNo
     * @return
     */
    @PostMapping("/close")
    @ResponseBody
    private CommonResult closeOrder(String orderId, String alipayNo) {
        //设置请求参数
        AlipayTradeCloseRequest alipayRequest = new AlipayTradeCloseRequest();
        Map<String, String> map = new HashMap<>(16);
        map.put("out_trade_no", orderId);
        map.put("trade_no", alipayNo);

        try {
            ObjectMapper om = new ObjectMapper();
            alipayRequest.setBizContent(om.writeValueAsString(map));
            // 发送请求
            String json = alipayClient.execute(alipayRequest).getBody();
            // 返回信息
            Map<String, Object> resultMap = om.readValue(json, Map.class);
            Map<String, String> responseMap = (Map) resultMap.get("alipay_trade_close_response");
            // 获取返回码
            String code = responseMap.get("code");
            if ("10000".equals(code)) {
                // 将状态更改为关闭
                String outTradeNo = responseMap.get("out_trade_no");
                System.out.println("交易关闭.........");
                //orderInfoService.changeStatus(outTradeNo, "TRADE_CLOSED");
                return CommonResult.success(null);
            } else {
                log.error("支付宝关闭交易失败。原因：{}", responseMap.get("sub_msg"));
                return CommonResult.error(ResultCode.ALIPAY_CLOSE_ERROR);
            }
        } catch (Exception e) {
            log.error("支付宝关闭交易异常。原因：{}", e.getMessage());
            e.printStackTrace();
        }
        return CommonResult.error(ResultCode.ALIPAY_CLOSE_ERROR);
    }

    /**
     * 订单验证。支付宝同步或异步回调时使用
     */
    private boolean validateOrder(Map<String, String> params) {
        try {
            // 验证签名
            boolean signVerified = AlipaySignature.rsaCheckV1(params, alipayConfig.getPublicKey(),
                    alipayConfig.getCharset(), alipayConfig.getSignType());
            if (!signVerified) {
                return false;
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }

        // 获取订单数据
        String orderId = params.get("out_trade_no");
        Order order = userOrderService.getOrder(Long.valueOf(orderId));
        if (order == null) {
            return false;
        }
        // 判断订单金额是否相等
        BigDecimal total = BigDecimal.valueOf(Double.valueOf(params.get("total_amount")));
        if (total.compareTo(order.getPayment().add(order.getPostFee())) != 0) {
            return false;
        }

        // 判断商户ID是否相等
        String sellerId = params.get("seller_id");
        if (!sellerId.equals(alipayConfig.getUid())) {
            return false;
        }

        // 判断APP_ID是否相等
        String appId = params.get("app_id");
        if (!appId.equals(alipayConfig.getAppId())) {
            return false;
        }

        return true;
    }

    /**
     * 获取支付参数
     */
    private Map<String, String> getPayParams(HttpServletRequest request) {
        Map<String, String> params = new HashMap<>(16);
        Map<String, String[]> requestParams = request.getParameterMap();

        Iterator<String> iter = requestParams.keySet().iterator();
        while (iter.hasNext()) {
            String name = iter.next();
            String[] values = requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }

            // 在出现乱码时使用
            // valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }
        return params;
    }
}
