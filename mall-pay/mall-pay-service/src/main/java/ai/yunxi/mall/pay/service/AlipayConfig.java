package ai.yunxi.mall.pay.service;

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import lombok.Data;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.FileNotFoundException;
import java.io.IOException;


@Data
@Configuration
public class AlipayConfig {

    @Value("${alipay.app_id}")
    private String appId;

    @Value("${alipay.uid}")
    private String uid;

    @Value("${alipay.public_key_file}")
    private String publicKeyFile;

    @Value("${alipay.private_key_file}")
    private String privateKeyFile;

    @Value("${alipay.charset}")
    private String charset;

    @Value("${alipay.fromat}")
    private String format;

    @Value("${alipay.sign_type}")
    private String signType;

    @Value("${alipay.gatewayUrl}")
    private String gatewayUrl;

    @Value("${alipay.notifyUrl}")
    private String notifyUrl;

    @Value("${alipay.returnUrl}")
    private String returnUrl;

    // 公钥Key
    private String publicKey;
    // 私钥Key
    private String privateKey;

    @Bean
    AlipayClient alipayClient() {
        Resource publicRes = new ClassPathResource(publicKeyFile);
        Resource privateRes = new ClassPathResource(privateKeyFile);

        try {
            // 读取公私钥Key
            publicKey = IOUtils.toString(publicRes.getInputStream());
            privateKey = IOUtils.toString(privateRes.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new DefaultAlipayClient(gatewayUrl, appId, privateKey, format,
                charset, publicKey, signType);
    }
}
