package ai.yunxi.mall.pay.domain.mapper;

import ai.yunxi.mall.pay.domain.entity.TbPayment;
import ai.yunxi.mall.pay.domain.entity.TbPaymentExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TbPaymentMapper {
    long countByExample(TbPaymentExample example);

    int deleteByExample(TbPaymentExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TbPayment record);

    int insertSelective(TbPayment record);

    List<TbPayment> selectByExample(TbPaymentExample example);

    TbPayment selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TbPayment record, @Param("example") TbPaymentExample example);

    int updateByExample(@Param("record") TbPayment record, @Param("example") TbPaymentExample example);

    int updateByPrimaryKeySelective(TbPayment record);

    int updateByPrimaryKey(TbPayment record);
}