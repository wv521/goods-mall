package ai.yunxi.mall.front.controller;

import ai.yunxi.mall.common.utils.CommonResult;
import ai.yunxi.mall.core.service.UserOrderService;
import ai.yunxi.mall.core.service.dto.Order;
import ai.yunxi.mall.core.service.dto.PageOrder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "订单")
@RequestMapping(value = "/front/order")
public class OrderController {

    @DubboReference(retries = 0)
    private UserOrderService orderService;

    @ApiOperation(value = "获得用户所有订单")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<PageOrder> getOrderList(String userId,
                                                @RequestParam(defaultValue = "1") int page,
                                                @RequestParam(defaultValue = "5") int size) {
        PageOrder pageOrder = orderService.getOrderList(Long.valueOf(userId), page, size);
        return CommonResult.success(pageOrder);
    }

    @ApiOperation(value = "通过id获取订单")
    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public CommonResult<Order> getOrder(String orderId) {
        Order order = orderService.getOrder(Long.valueOf(orderId));
        return CommonResult.success(order);
    }

    @ApiOperation(value = "创建订单")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public CommonResult<Object> createOrder(@RequestBody Order order) {
        Comparable<?> orderId = orderService.createOrder(order);
        return CommonResult.success(orderId.toString());
    }

    @ApiOperation(value = "取消订单")
    @RequestMapping(value = "/cancel", method = RequestMethod.POST)
    public CommonResult<Object> cancelOrder(@RequestBody Order order) {
        int result = orderService.cancelOrder(order.getOrderId());
        return CommonResult.success(result);
    }

    @ApiOperation(value = "删除订单")
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public CommonResult<Object> deleteOrder(String orderId) {
        int result = orderService.deleteOrder(Long.valueOf(orderId));
        return CommonResult.success(result);
    }
}
