package ai.yunxi.mall.front.controller;

import ai.yunxi.mall.common.utils.CommonResult;
import ai.yunxi.mall.user.service.AddressService;
import ai.yunxi.mall.user.service.dto.AddressInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(tags = "收货地址")
@RequestMapping(value="/front/address")
public class AddressController {

    @DubboReference
    private AddressService addressService;

    @ApiOperation(value = "获得所有收货地址")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<List<AddressInfo>> addressList(Long userId) {
        List<AddressInfo> list = addressService.getAddressList(userId);
        return CommonResult.success(list);
    }

    @ApiOperation(value = "通过Id获得收货地址")
    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public CommonResult<AddressInfo> address(@PathVariable Long id) {
        AddressInfo address = addressService.getAddress(id);
        return CommonResult.success(address);
    }

    @ApiOperation(value = "添加收货地址")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public CommonResult<Object> addAddress(@RequestBody AddressInfo addressInfo) {
        int result = addressService.addAddress(addressInfo);
        return CommonResult.success(result);
    }

    @ApiOperation(value = "编辑收货地址")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public CommonResult<Object> updateAddress(@RequestBody AddressInfo addressInfo) {
        int result = addressService.updateAddress(addressInfo);
        return CommonResult.success(result);
    }

    @ApiOperation(value = "删除收货地址")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public CommonResult<Object> deleteAddress(@PathVariable Long id) {
        int result = addressService.deleteAddress(id);
        return CommonResult.success(result);
    }
}
