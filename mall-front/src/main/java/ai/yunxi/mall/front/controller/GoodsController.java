package ai.yunxi.mall.front.controller;

import ai.yunxi.mall.common.utils.CommonResult;
import ai.yunxi.mall.content.domain.entity.TbPanel;
import ai.yunxi.mall.content.service.ContentService;
import ai.yunxi.mall.content.service.dto.AllGoodsResult;
import ai.yunxi.mall.index.service.SearchProductService;
import ai.yunxi.mall.index.service.dto.SearchResult;
import ai.yunxi.mall.product.service.dto.Product;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@Api(tags = "商品页面展示")
@RequestMapping(value = "/front/goods")
public class GoodsController {

    @DubboReference
    private ContentService contentService;

    @DubboReference
    private SearchProductService searchProductService;

    @ApiOperation(value = "首页内容展示")
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public CommonResult<List<TbPanel>> getProductHome() {
        List<TbPanel> list = contentService.getHome();
        return CommonResult.success(list);
    }

    @ApiOperation(value = "所有商品")
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public CommonResult<AllGoodsResult> getAllProduct(@RequestParam(defaultValue = "0") int page,
                                                      @RequestParam(defaultValue = "20") int size,
                                                      @RequestParam(defaultValue = "") String sort,
                                                      @RequestParam(defaultValue = "") Long categoryId,
                                                      @RequestParam(defaultValue = "0") int priceGt,
                                                      @RequestParam(defaultValue = "99999999") int priceLte) {
        AllGoodsResult allGoodsResult = contentService.getAllProduct(page, size, sort,
                categoryId, priceGt, priceLte);
        return CommonResult.success(allGoodsResult);
    }

    @ApiOperation(value = "商品详情")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public CommonResult<Product> getProduct(@PathVariable Long id) {
        Product product = contentService.getProduct(id);
        return CommonResult.success(product);
    }

    @ApiOperation(value = "通过Index搜索商品")
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public CommonResult<SearchResult> searchProduct(@RequestParam(defaultValue = "") String key,
                                                    @RequestParam(defaultValue = "0") int page,
                                                    @RequestParam(defaultValue = "20") int size,
                                                    @RequestParam(defaultValue = "") String sort,
                                                    @RequestParam(defaultValue = "0") int priceGt,
                                                    @RequestParam(defaultValue = "99999999") int priceLte) {
        SearchResult searchResult = searchProductService.search(key, page, size, sort,
                priceGt, priceLte);
        return CommonResult.success(searchResult);
    }

    @ApiOperation(value = "商品推荐板块")
    @RequestMapping(value = "/recommend", method = RequestMethod.GET)
    public CommonResult<List<TbPanel>> getRecommendGoods() {
        List<TbPanel> list = contentService.getRecommendGoods();
        return CommonResult.success(list);
    }
}
