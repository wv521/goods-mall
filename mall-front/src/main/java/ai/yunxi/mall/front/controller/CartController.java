package ai.yunxi.mall.front.controller;

import ai.yunxi.mall.common.utils.CommonResult;
import ai.yunxi.mall.core.service.CartService;
import ai.yunxi.mall.core.service.dto.Cart;
import ai.yunxi.mall.core.service.dto.CartItem;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Api(tags = "购物车")
@RequestMapping(value="/front/cart")
public class CartController {

    @DubboReference
    private CartService cartService;

    @ApiOperation(value = "添加购物车商品")
    @RequestMapping(value = "/addItem", method = RequestMethod.POST)
    public CommonResult<Object> addCart(@RequestBody Cart cart) {
        int result = cartService.addItem(cart.getUserId(), cart.getProductId(), cart.getProductNum());
        return CommonResult.success(result);
    }

    @ApiOperation(value = "获取购物车商品列表")
    @RequestMapping(value = "/itemList", method = RequestMethod.POST)
    public CommonResult<List<CartItem>> getCartList(@RequestBody Cart cart) {
        List<CartItem> list = cartService.getItems(cart.getUserId());
        return CommonResult.success(list);
    }

    @ApiOperation(value = "编辑购物车商品")
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public CommonResult<Object> updateCartNum(@RequestBody Cart cart) {
        int result = cartService.setItemNum(cart.getUserId(), cart.getProductId(),
                cart.getProductNum(), cart.getChecked());
        return CommonResult.success(result);
    }

    @ApiOperation(value = "是否全选购物车商品")
    @RequestMapping(value = "/checkAll", method = RequestMethod.POST)
    public CommonResult<Object> editCheckAll(@RequestBody Cart cart) {
        int result = cartService.checkAll(cart.getUserId(), cart.getChecked());
        return CommonResult.success(result);
    }

    @ApiOperation(value = "删除购物车商品")
    @RequestMapping(value = "/deleteItem", method = RequestMethod.POST)
    public CommonResult<Object> deleteCartItem(@RequestBody Cart cart) {
        int result = cartService.deleteItem(cart.getUserId(), cart.getProductId());
        return CommonResult.success(result);
    }

    @ApiOperation(value = "删除购物车选中商品")
    @RequestMapping(value = "/deleteChecked", method = RequestMethod.POST)
    public CommonResult<Object> delChecked(@RequestBody Cart cart) {
        cartService.delChecked(cart.getUserId());
        return CommonResult.success(null);
    }
}
