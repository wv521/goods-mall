package ai.yunxi.mall.front.controller;

import ai.yunxi.mall.common.utils.ResultCode;
import ai.yunxi.mall.common.utils.CommonResult;
import ai.yunxi.mall.core.service.dto.CommonDto;
import ai.yunxi.mall.user.service.LoginService;
import ai.yunxi.mall.user.service.MemberService;
import ai.yunxi.mall.user.service.RegisterService;
import ai.yunxi.mall.user.service.dto.Member;
import ai.yunxi.mall.user.service.dto.MemberLogin;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@RestController
@Api(tags = "会员注册登录")
public class MemberController {

    private final static Logger log = LoggerFactory.getLogger(MemberController.class);

    @DubboReference
    private LoginService loginService;

    @DubboReference
    private RegisterService registerService;

    @DubboReference
    private MemberService memberService;

    @ApiOperation(value = "用户登录")
    @RequestMapping(value = "/member/login", method = RequestMethod.POST)
    public CommonResult<Member> login(@RequestBody MemberLogin memberLoginRegist,
                                      HttpServletRequest request) {
        String challenge = memberLoginRegist.getChallenge();
        String validate = memberLoginRegist.getValidate();
        String seccode = memberLoginRegist.getSeccode();

        //自定义参数,可选择添加
        HashMap<String, String> param = new HashMap<>();
        int gtResult = 0;
        // 省略验证过程

        Member member = new Member();
        if (gtResult == 1) {
            // 验证成功
            member = loginService.userLogin(memberLoginRegist.getUserName(), memberLoginRegist.getUserPwd());
        } else {
            // 验证失败
            member.setState(0);
            member.setMessage("验证失败");
        }
        return CommonResult.success(member);
    }

    @ApiOperation(value = "判断用户是否登录")
    @RequestMapping(value = "/member/checkLogin", method = RequestMethod.GET)
    public CommonResult<Member> checkLogin(@RequestParam(defaultValue = "") String token) {
        Member member = loginService.getUserByToken(token);
        return CommonResult.success(member);
    }

    @ApiOperation(value = "退出登录")
    @RequestMapping(value = "/member/loginOut", method = RequestMethod.GET)
    public CommonResult<Object> logout(@RequestParam(defaultValue = "") String token) {
        loginService.logout(token);
        return CommonResult.success(null);
    }

    @ApiOperation(value = "用户注册")
    @RequestMapping(value = "/member/register", method = RequestMethod.POST)
    public CommonResult<Object> register(@RequestBody MemberLogin memberLoginRegist,
                                         HttpServletRequest request) {
        String challenge = memberLoginRegist.getChallenge();
        String validate = memberLoginRegist.getValidate();
        String seccode = memberLoginRegist.getSeccode();

        //自定义参数,可选择添加
        HashMap<String, String> param = new HashMap<>();
        int gtResult = 1;
        // 省略验证过程

        if (gtResult == 1) {
            // 验证成功
            int result = registerService.register(memberLoginRegist.getUserName(), memberLoginRegist.getUserPwd());
            if (result == 0) {
                return CommonResult.error(ResultCode.FAILED, "该用户名已被注册");
            } else if (result == -1) {
                return CommonResult.error(ResultCode.FAILED, "用户名密码不能为空");
            }
            return CommonResult.success(result);
        } else {
            // 验证失败
            return CommonResult.error(ResultCode.FAILED, "验证失败");
        }
    }

    @ApiOperation(value = "用户头像上传")
    @RequestMapping(value = "/member/imageUpload", method = RequestMethod.POST)
    public CommonResult<Object> imageUpload(@RequestBody CommonDto common) {
        String imgPath = memberService.imageUpload(common.getUserId(), common.getToken(), common.getImgData());
        return CommonResult.success(imgPath);
    }
}
