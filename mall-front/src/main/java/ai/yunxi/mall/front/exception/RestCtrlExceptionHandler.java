package ai.yunxi.mall.front.exception;

import ai.yunxi.mall.common.exception.CommonException;
import ai.yunxi.mall.common.utils.ResultCode;
import ai.yunxi.mall.common.utils.CommonResult;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.net.BindException;

@ControllerAdvice
public class RestCtrlExceptionHandler {

    private static Logger log = LoggerFactory.getLogger(RestCtrlExceptionHandler.class);

    @ExceptionHandler(BindException.class)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public CommonResult<Object> bindExceptionHandler(BindException e) {
        String errorMsg = null;
        if (e != null) {
            errorMsg = e.getMessage();
            log.warn(errorMsg);
        }
        return CommonResult.error(ResultCode.VALIDATE_ERR, errorMsg);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @ExceptionHandler(CommonException.class)
    @ResponseBody
    public CommonResult<Object> handleException(CommonException e) {
        String errorMsg = null;
        if (e != null) {
            errorMsg = e.getMesssage();
            log.warn(e.getMessage());
        }
        return CommonResult.error(ResultCode.INTERNAL_ERR, errorMsg);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public CommonResult<Object> handleException(Exception e) {
        String errorMsg = null;
        if (e != null) {
            log.warn(e.getMessage());
            if (e.getMessage() != null && e.getMessage().contains("Maximum upload size")) {
                errorMsg = "上传文件大小超过5MB限制";
            } else if (e.getMessage().contains("CommonException")) {
                errorMsg = e.getMessage();
                errorMsg = StringUtils.substringAfter(errorMsg, "CommonException:");
                errorMsg = StringUtils.substringBefore(errorMsg, "\n");
            } else {
                errorMsg = e.getMessage();
            }
        }
        return CommonResult.error(ResultCode.INTERNAL_ERR, errorMsg);
    }
}
