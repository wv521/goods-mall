<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title></title>
<link rel="stylesheet" type="text/css" href="css/public.css"/>
<link rel="stylesheet" type="text/css" href="css/base.css"/>
<link rel="stylesheet" type="text/css" href="css/buyConfirm.css"/>
<script type="text/javascript" src="js/jquery_cart.js"></script>
<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="js/index.js" type="text/javascript"></script>
<script src="js/unslider.min.js" type="text/javascript"></script>
<script type="text/javascript">
let paymentConfig = {
    orderId: window.localStorage.getItem("orderId"),
    productName: window.localStorage.getItem("productName"),
    totalPrice: window.localStorage.getItem("totalPrice"),
    userName: window.localStorage.getItem("userName"),
    tel: window.localStorage.getItem("tel"),
    streetName: window.localStorage.getItem("streetName")
}

window.onload = function () {
    new tab('test4-input-input_tab1-input_tab2', '-');
    $("span.pay-total").html(paymentConfig.totalPrice);
    $("#order-id").html(paymentConfig.orderId);
    $("#user-name").html(paymentConfig.userName);
    $("#user-tele").html(paymentConfig.tel);
    $("#street-name").html(paymentConfig.streetName);
    $("input[name=orderId]").val(paymentConfig.orderId);
    aler($("input[name=orderId]").val());
}

function tab(o, s, cb, ev) { //tab换类
    var $ = function (o) {
        return document.getElementById(o)
    };
    var css = o.split((s || '_'));
    if (css.length != 4) return;
    this.event = ev || 'onclick';
    o = $(o);
    if (o) {
        this.ITEM = [];
        o.id = css[0];
        var item = o.getElementsByTagName(css[1]);
        var j = 1;
        for (var i = 0; i < item.length; i++) {
            if (item[i].className.indexOf(css[2]) >= 0 || item[i].className.indexOf(css[3]) >= 0) {
                if (item[i].className == css[2]) o['cur'] = item[i];
                item[i].callBack = cb || function () {
                };
                item[i]['css'] = css;
                item[i]['link'] = o;
                this.ITEM[j] = item[i];
                item[i]['Index'] = j++;
                item[i][this.event] = this.ACTIVE;
            }
        }
        return o;
    }
}

tab.prototype = {
    ACTIVE: function () {
        var $ = function (o) {
            return document.getElementById(o)
        };
        this['link']['cur'].className = this['css'][3];
        this.className = this['css'][2];
        try {
            $(this['link']['id'] + '_' + this['link']['cur']['Index']).style.display = 'none';
            $(this['link']['id'] + '_' + this['Index']).style.display = 'block';
        } catch (e) {
        }
        this.callBack.call(this);
        this['link']['cur'] = this;
    }
}
</script>
</head>

<body>
<div class="shortcut_v2013 alink_v2013">
    <div class="w">
        <ul class="fl 1h">
            <li class="fl">
                <div class="menu">
                    <div class="menu_hd">
                        <a href="#">
                            <img src="images/top_phone_icon.png" width="12px" height="16px">手机版
                        </a>
                        <b><em></em></b></div>
                    <div class="menu_bd">
                        <ul>
                            <li><a href="#">安卓版</a></li>
                            <li><a href="#">苹果版</a></li>
                        </ul>
                    </div>
                </div>
            </li>
            <li class="fl"><i class="shortcut_s"></i></li>
            <li class="fl"><div class="menu_hd">您好，欢迎来到Shopping Mall！</div></li>
            <li class="fl"><div class="menu_hd"><a href="#">请登录</a></div></li>
            <li class="fl"><div class="menu_hd"><a href="#">免费注册</a></div></li>
        </ul>
        <ul class="fr 1h">
            <li class="fl"><div class="menu_hd"><a href="#">我的订单</a></div></li>
            <li class="fl"><i class="shortcut_s"></i></li>
            <li class="fl"><div class="menu_hd"><a href="#">个人中心</a></div></li>
            <li class="fl"><i class="shortcut_s"></i></li>
            <li class="fl"><div class="menu_hd"><a href="#">服务中心</a></div></li>
            <li class="fl"><i class="shortcut_s"></i></li>
            <li class="fl">
                <div class="menu">
                    <div class="menu_hd"><a href="#">网站导航</a><b><em></em></b></div>
                    <div class="menu_bd">
                        <ul>
                            <li><a href="#">网站导航</a></li>
                            <li><a href="#">网站导航</a></li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
        <span class="clr"></span>
    </div>
</div>
<div class="header_2013">
    <div class="w">
        <div class="logo_v2013">
            <a href="#">
                <img class="border_r" src="images/logo.png" width="210" height="59">
            </a>
        </div>
        <div class="header_searchbox">
            <form action="#">
                <input name="search" type="text" class="header_search_input" id="find_input" autocomplete="off" x-webkit-speech="" x-webkit-grammar="builtin:search" lang="zh">
                <button type="button" class="header_search_btn">搜索</button>
            </form>
            <ul class="hot_word">
                <li><a class="red" href="#" target="_blank">满99减10元</a></li>
                <li><a target="_blank" href="#">小米电视</a></li>
                <li><a target="_blank" href="#">图书钜惠</a></li>
                <li><a target="_blank" href="#">年货</a></li>
                <li><a target="_blank" href="#">电子产品</a></li>
            </ul>
        </div>
        <div id="cart_box" class="cart_box">
            <a id="cart" class="cart_link" href="#" rel="nofollow">
                <span class="text">去购物车结算</span>
                <img src="images/shopping_icon.png" width="24" height="24" class="cart_gif">
                <!-- 购物车没有物品时，隐藏此num -->
                <!--<span class="num">12</span>-->
                <s class="icon_arrow_right"></s>
            </a>

            <div class="cart_content" id="cart_content">
                <i class="cart-icons"></i>
                <!-- 购物车没有物品时，显示cart_content_null、隐藏cart_content_all -->
                <div class="cart_content_null" style="display: none;">购物车中还没有商品，<br>快去挑选心爱的商品吧！</div>
                <div class="cart_content_all" style="display: block;">
                    <div class="cart_left_time"><span></span></div>
                    <div class="cart_content_center">
                    </div>
                    <div class="con_all">
                        <div class="price_whole"><span>共<span class="num_all">0</span>件商品</span></div>
                        <div><span class="price_gongji">共计<em>￥</em><span class="total_price">0</span></span><a href="#" class="cart_btn" rel="nofollow">去购物车结算</a></div>
                    </div>
                </div>
            </div>
        </div>
        <span class="clr"></span>
    </div>
</div>
<!--订单提交body部分开始-->
<div class="border_top_cart">
    <div class="container payment-con">
        <form action="/pay/alipay/payment" id="pay-form" method="post">
            <div class="order-info">
                <div class="msg">
                    <h3>您的订单已提交成功！等待支付！</h3>
                    <p></p>
                    <p class="post-date"></p>
                </div>
                <div class="info">
                    <p>
                        订单：<span id="order-id"></span>
                        <input type="hidden" name="orderId" />
                    </p>
                    <p>
                        金额：<span class="pay-total"></span>
                    </p>
                    <p>
                        配送：<span id="user-name"></span><span class="line">/</span>
                        <span id="user-tele"></span><span class="line">/</span>
                        <span id="street-name"></span></p>
                </div>
                <div class="icon-box">
                    <i class="iconfont"><img src="images/yes_ok.png"></i>
                </div>
            </div>
            <div class="xm-plain-box">
                <!-- 选择支付方式 -->
                <div class="box-hd bank-title clearfix">
                    <div id="titleWrap" class="title-wrap">
                        <h2 class="title">选择支付方式</h2>
                        <h2 class="title hide ">你还需要继续支付 <em>49.00</em> 元</h2>
                        <span class="tip-tag"></span>
                    </div>
                </div>
                <div class="box-bd" id="bankList">
                    <div class="payment-bd">
                        <form name="ck">
                            <dl class="clearfix payment-box">
                                <dt>
                                    <strong>支付平台</strong>
                                </dt>
                                <dd>
                                    <fieldset id="test4-input-input_tab1-input_tab2" style=" border:none;">
                                        <ul class="payment-list clearfix">
                                            <li>
                                                <input class="input_tab2" name="myradio" id="r2" type="radio" checked="checked"/>
                                                <label for="r2"><img src="images/zfb.png" alt=""/></label>
                                            </li>
                                            <li>
                                                <input class="input_tab2" name="myradio" id="r2" type="radio"/>
                                                <label for="r2"><img src="images/zxzf.png" alt=""/></label>
                                            </li>
                                        </ul>
                                    </fieldset>
                                </dd>
                            </dl>
                        </form>
                    </div>
                </div>
                <div class="box-ft clearfix">
                    <input type="submit" class="btn btn-primary" value="支付订单" id="payBtn">
                    <span class="tip"></span>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="js/base.min.js"></script>
<script type="text/javascript" src="js/buyConfirm.js"></script>
</div>
<!--订单提交body部分结束-->
</body>
</html>
