<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8">
<meta charset="UTF-8">
<title>创建订单</title>
<link rel="stylesheet" type="text/css" href="css/public.css"/>
<link rel="stylesheet" type="text/css" href="css/base.css"/>
<link rel="stylesheet" type="text/css" href="css/checkOut.css"/>
<script type="text/javascript" src="js/jquery_cart.js"></script>
<script type="text/javascript" src="js/base.min.js"></script>
<script type="text/javascript" src="js/checkout.min.js"></script>
<script type="text/javascript" src="js/index.js"></script>
<script type="text/javascript" src="js/unslider.min.js"></script>
<script type="text/javascript">
    let url = "/front/address/list";
    let result = $.ajax({
        url: url,
        data: {
            userId: "63"
        },
        success: function (data) {
            let html = "";
            let result = eval(data);
            $.each(result.data, function (i, item) {
                html += "<dl class=\"item\">\n" +
                    "  <dt>\n" +
                    "    <strong class=\"itemConsignee\">" + item.userName + "</strong>\n" +
                    "    <span class=\"itemTag tag\">" + (i == 0 ? "默认" : "") + "</span>\n" +
                    "  </dt>\n" +
                    "  <dd>\n" +
                    "    <p class=\"tel itemTel\">" + item.tel + "</p>\n" +
                    "    <p class=\"itemRegion\"></p>\n" +
                    "    <p class=\"itemStreet\">" + item.streetName + "</p>\n" +
                    "  </dd>\n" +
                    "  <dd style=\"display:none\">\n" +
                    "    <input type=\"radio\" name=\"Checkout[address]\" class=\"addressId\" value=\"" + item.addressId + "\">\n" +
                    "  </dd>\n" +
                    "</dl>\n";
            });
            $("div#checkoutAddrList").prepend(html);
        },
        error: function (err) {
            alert(err);
        }
    });
    let checkoutConfig = {
        productId: window.localStorage.getItem("productId"),
        productName: window.localStorage.getItem("productName"),
        productImg: window.localStorage.getItem("productImg"),
        unitPrice: window.localStorage.getItem("productPrice"),
        totalNumber: window.localStorage.getItem("number"),
        totalPrice: window.localStorage.getItem("productPrice") * window.localStorage.getItem("number"),
        addressId: '',
        userName:'',
        streetName: '',
        tel: '',
        postage: 10,//运费
        couponDiscount: 0.00,//优惠券抵扣
        postFree: true,//是否免邮
        discountMoney: 0.00,//活动优惠
    };

    $(function () {
        $(".g-info a").html(checkoutConfig.productName);
        $(".item-row .col.col-2").html(checkoutConfig.unitPrice);
        $(".g-pic img").attr("src", checkoutConfig.productImg);
        $(".item-row .col.col-3").html(checkoutConfig.totalNumber);
        $(".item-row .col.col-4").html(checkoutConfig.totalPrice);
        $("#totalDesc").html(checkoutConfig.totalPrice);
        $("#discountDesc").html(checkoutConfig.discountMoney);
        $("#couponDesc").html(checkoutConfig.couponDiscount);
        $("#checkoutAddrList").on("click", "dl", function () {
            $(this).addClass("selected").siblings().removeClass("selected"), $(this).find(".addressId").prop("checked", !0), $(this).siblings().find(".addressId").prop("checked", !1)
            $("#addressId").val($(this).find(".addressId").val());
            $("#userName").val($(this).find(".itemConsignee").html());
            $("#streetName").val($(this).find(".itemStreet").html());
            $("#tel").val($(this).find(".itemTel").html());
            checkoutConfig.userName = $("#userName").val();
            checkoutConfig.streetName = $("#streetName").val();
            checkoutConfig.tel = $("#tel").val();
        })
    });
</script>
</head>

<body>
<div class="shortcut_v2013 alink_v2013">
    <div class="w">
        <ul class="fl 1h">
            <li class="fl">
                <div class="menu">
                    <div class="menu_hd">
                        <a href="#">
                            <img src="images/top_phone_icon.png" width="12px" height="16px">手机版
                        </a>
                        <b><em></em></b></div>
                    <div class="menu_bd">
                        <ul>
                            <li><a href="#">安卓版</a></li>
                            <li><a href="#">苹果版</a></li>
                        </ul>
                    </div>
                </div>
            </li>
            <li class="fl"><i class="shortcut_s"></i></li>
            <li class="fl"><div class="menu_hd">您好，欢迎来到Shopping Mall！</div></li>
            <li class="fl"><div class="menu_hd"><a href="#">请登录</a></div></li>
            <li class="fl"><div class="menu_hd"><a href="#">免费注册</a></div></li>
        </ul>
        <ul class="fr 1h">
            <li class="fl"><div class="menu_hd"><a href="#">我的订单</a></div></li>
            <li class="fl"><i class="shortcut_s"></i></li>
            <li class="fl"><div class="menu_hd"><a href="#">个人中心</a></div></li>
            <li class="fl"><i class="shortcut_s"></i></li>
            <li class="fl"><div class="menu_hd"><a href="#">服务中心</a></div></li>
            <li class="fl"><i class="shortcut_s"></i></li>
            <li class="fl">
                <div class="menu">
                    <div class="menu_hd"><a href="#">网站导航</a><b><em></em></b></div>
                    <div class="menu_bd">
                        <ul>
                            <li><a href="#">网站导航</a></li>
                            <li><a href="#">网站导航</a></li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
        <span class="clr"></span>
    </div>
</div>
<div class="header_2013">
    <div class="w">
        <div class="logo_v2013">
            <a href="#">
                <img class="border_r" src="images/logo.png" width="210" height="59">
            </a>
        </div>
        <div class="header_searchbox">
            <form action="#">
                <input name="search" type="text" class="header_search_input" id="find_input" autocomplete="off" x-webkit-speech="" x-webkit-grammar="builtin:search" lang="zh">
                <button type="button" class="header_search_btn">搜索</button>
            </form>
            <ul class="hot_word">
                <li><a class="red" href="#" target="_blank">满99减10元</a></li>
                <li><a target="_blank" href="#">小米电视</a></li>
                <li><a target="_blank" href="#">图书钜惠</a></li>
                <li><a target="_blank" href="#">年货</a></li>
                <li><a target="_blank" href="#">电子产品</a></li>
            </ul>
        </div>
        <div id="cart_box" class="cart_box">
            <a id="cart" class="cart_link" href="#" rel="nofollow">
                <span class="text">去购物车结算</span>
                <img src="images/shopping_icon.png" width="24" height="24" class="cart_gif">
                <!-- 购物车没有物品时，隐藏此num -->
                <!--<span class="num">12</span>-->
                <s class="icon_arrow_right"></s>
            </a>

            <div class="cart_content" id="cart_content">
                <i class="cart-icons"></i>
                <!-- 购物车没有物品时，显示cart_content_null、隐藏cart_content_all -->
                <div class="cart_content_null" style="display: none;">购物车中还没有商品，<br>快去挑选心爱的商品吧！</div>
                <div class="cart_content_all" style="display: block;">
                    <div class="cart_left_time"><span></span></div>
                    <div class="cart_content_center">
                    </div>
                    <div class="con_all">
                        <div class="price_whole"><span>共<span class="num_all">0</span>件商品</span></div>
                        <div><span class="price_gongji">共计<em>￥</em><span class="total_price">0</span></span><a href="#" class="cart_btn" rel="nofollow">去购物车结算</a></div>
                    </div>
                </div>
            </div>
        </div>
        <span class="clr"></span>
    </div>
</div>
<div class="border_top_cart">
<div class="container">
    <div class="checkout-box">
        <form id="checkoutForm" action="#" method="post">
            <div class="checkout-box-bd">
                <!-- 地址状态 0：默认选择；1：新增地址；2：修改地址 -->
                <input type="hidden" name="Checkout[addressState]" id="addrState" value="0">
                <!-- 收货地址 -->
                <div class="xm-box">
                    <div class="box-hd ">
                        <h2 class="title">收货地址</h2>
                        <!---->
                    </div>
                    <div class="box-bd">
                        <div class="clearfix xm-address-list" id="checkoutAddrList">
                            <div class="item use-new-addr" id="J_useNewAddr" data-state="off">
                                <span class="iconfont icon-add"><img src="images/add_cart.png"></span>
                                使用新地址
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="addressId">
                    <input type="hidden" id="userName">
                    <input type="hidden" id="tel">
                    <input type="hidden" id="streetName">
                </div>
            </div>
            <div class="checkout-box-ft">
                <!-- 商品清单 -->
                <div id="checkoutGoodsList" class="checkout-goods-box">
                    <div class="xm-box">
                        <div class="box-hd">
                            <h2 class="title">确认订单信息</h2>
                        </div>
                        <div class="box-bd">
                            <dl class="checkout-goods-list">
                                <dt class="clearfix">
                                    <span class="col col-1">商品名称</span>
                                    <span class="col col-2">购买单价（元）</span>
                                    <span class="col col-3">购买数量</span>
                                    <span class="col col-4">小计（元）</span>
                                </dt>
                                <dd class="item clearfix">
                                    <div class="item-row">
                                        <div class="col col-1">
                                            <div class="g-pic">
                                                <img src="" width="40" height="40"/>
                                            </div>
                                            <div class="g-info">
                                                <a href="#"></a>
                                            </div>
                                        </div>
                                        <div class="col col-2"></div>
                                        <div class="col col-3"></div>
                                        <div class="col col-4"></div>
                                    </div>
                                </dd>
                            </dl>
                            <div class="checkout-count clearfix">
                                <div class="checkout-count-extend xm-add-buy">
                                    <h3 class="title">会员留言</h3><br/>
                                    <input id="buyerMessage" type="text"/>
                                </div>
                                <!-- checkout-count-extend -->
                                <div class="checkout-price">
                                    <ul>
                                        <li>
                                            订单总额：<span id="totalDesc"></span>
                                        </li>
                                        <li>
                                            活动优惠：<span id="discountDesc"></span>
                                        </li>
                                        <li>
                                            优惠券抵扣：<span id="couponDesc"></span>
                                        </li>
                                        <li>
                                            运费：<span id="postageDesc"></span>
                                        </li>
                                    </ul>
                                    <p class="checkout-total">应付总额：<span><strong id="totalPrice"></strong></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="checkout-confirm">
                    <input type="button" class="btn btn-primary" value="立即下单" id="checkoutToPay"/>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
</body>
</html>
