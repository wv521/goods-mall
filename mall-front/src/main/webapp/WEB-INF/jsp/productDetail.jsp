<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8">
<meta charset="UTF-8">
<title>商品详情</title>
<link rel="stylesheet" href="css/style.css" media="screen">
<link rel="stylesheet" href="css/grid.css" media="screen">
<link rel="stylesheet" href="css/jquery.jqzoom.css">
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery.jqzoom-core.js"></script>
<script type="text/javascript" src="js/jquery.carouFredSel-5.2.2-packed.js"></script>
<script type="text/javascript" src="js/html5.js"></script>
<script>
    $(document).ready(function () {
        $('.jqzoom').jqzoom({
            zoomType: 'standard',
            lens: true,
            preloadImages: true,
            alwaysOn: false
        });
    });
</script>

<script>
    $(document).ready(function () {
        $('#wrapper_tab a').click(function () {
            if ($(this).attr('class') != $('#wrapper_tab').attr('class')) {
                $('#wrapper_tab').attr('class', $(this).attr('class'));
            }
            return false;
        });
    });
</script>

<script>
    $(function () {
        $('#thumblist').carouFredSel({
            prev: '#img_prev',
            next: '#img_next',
            scroll: 1,
            auto: false,
            circular: false,
        });
        $(window).resize();
    });
</script>
<script>
    $(document).ready(function () {
        $("button").click(function () {
            $(this).addClass('click')
        });
    })
</script>
</head>

<body>
<section id="main">
    <div class="container_12">
        <div id="content" class="grid_9">
            <h1 class="page_title">Candles Orange Vanilla</h1>
            <div class="product_page">
                <div class="grid_4 img_slid" id="products">
                    <img class="sale" src="images/sale.png" alt="Sale"/>
                    <div class="preview slides_container">
                        <div class="prev_bg">
                            <a href="images/produkt_slid.png" class="jqzoom" rel='gal1' title="">
                                <img src="images/produkt_slid.png"  title="" alt=""/>
                            </a>
                        </div>
                    </div><!-- .prev -->
                    <ul class="pagination clearfix" id="thumblist">
                        <li><a class="zoomThumbActive" href='javascript:void(0);' rel="{gallery: 'gal1', smallimage: './images/produkt_slid.png',largeimage: './images/produkt_slid.png'}"><img src='images/produkt_slid.png' alt=""></a></li>
                        <li><a href='javascript:void(0);' rel="{gallery: 'gal1', smallimage: './images/produkt_slid1.png',largeimage: './images/produkt_slid1.png'}"><img src='images/produkt_slid1.png' alt=""></a></li>
                        <li><a href='javascript:void(0);' rel="{gallery: 'gal1', smallimage: './images/produkt_slid2.png',largeimage: './images/produkt_slid2.png'}"><img src='images/produkt_slid2.png' alt=""></a></li>
                        <li><a href='javascript:void(0);' rel="{gallery: 'gal1', smallimage: './images/produkt_slid3.png',largeimage: './images/produkt_slid3.png'}"><img src='images/produkt_slid3.png' alt=""></a></li>
                        <li><a href='javascript:void(0);' rel="{gallery: 'gal1', smallimage: './images/produkt_slid4.png',largeimage: './images/produkt_slid4.png'}"><img src='images/produkt_slid4.png' alt=""></a></li>
                    </ul>
                    <div class="next_prev">
                        <a id="img_prev" class="arows" href="#"><span>Prev</span></a>
                        <a id="img_next" class="arows" href="#"><span>Next</span></a>
                    </div><!-- . -->
                </div><!-- .grid_4 -->
                <div class="grid_5">
                    <div class="entry_content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pharetra faucibus congue. Aenean luctus dolor et purus malesuada luctus. Quisque ullamcorper ante viverra lectus fermentum quis rutrum erat sollicitudin. Fusce tortor massa.</p>
                        <div class="ava_price">
                            <div class="price">
                                <div class="price_new">$550.00</div>
                                <div class="price_old">$725.00</div>
                            </div><!-- .price -->
                        </div><!-- .ava_price -->

                        <div class="block_cart">
                            <div class="cart">
                                <a href="#" class="bay">立即购买</a>
                                <input type="text" name="" class="number" value="1" />
                                <span>Quantity:</span>
                            </div>
                            <div class="clear"></div>
                        </div><!-- .block_cart -->
                    </div><!-- .entry_content -->
                </div><!-- .grid_5 -->
                <div class="clear"></div>
                <div class="grid_9" >
                    <div id="wrapper_tab" class="tab1">
                        <a href="#" class="tab1 tab_link">商品介绍</a>
                        <a href="#" class="tab2 tab_link">评论</a>
                        <div class="clear"></div>

                        <div class="tab1 tab_body">
                            {{ description }}
                            <div class="clear"></div>
                        </div><!-- .tab1 .tab_body -->

                        <div class="tab2 tab_body">
                            <div class="clear"></div>
                        </div><!-- .tab2 .tab_body -->
                        <div class="clear"></div>
                    </div><!-- #wrapper_tab -->
                    <div class="clear"></div>
                </div><!-- .grid_9 -->

                <div class="clear"></div>
            </div><!-- .product_page -->
            <div class="clear"></div>
        </div><!-- #content -->
        <div class="clear"></div>
    </div><!-- .container_12 -->
</section><!-- #main -->
<div class="clear"></div>
</body>
</html>