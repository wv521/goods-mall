<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8">
<meta charset="UTF-8">
<title>商品列表</title>
<link rel="stylesheet" href="css/product-list.css"/>
<link rel="stylesheet" href="css/public.css"/>
<link rel="stylesheet" href="css/base.css"/>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/jquery.animate-colors-min.js"></script>
</head>

<body>
<div class="shortcut_v2013 alink_v2013">
    <div class="w">
        <ul class="fl 1h">
            <li class="fl">
                <div class="menu">
                    <div class="menu_hd">
                        <a href="#">
                            <img src="images/top_phone_icon.png" width="12px" height="16px">手机版
                        </a>
                        <b><em></em></b></div>
                    <div class="menu_bd">
                        <ul>
                            <li><a href="#">安卓版</a></li>
                            <li><a href="#">苹果版</a></li>
                        </ul>
                    </div>
                </div>
            </li>
            <li class="fl"><i class="shortcut_s"></i></li>
            <li class="fl"><div class="menu_hd">您好，欢迎来到Shopping Mall！</div></li>
            <li class="fl"><div class="menu_hd"><a href="#">请登录</a></div></li>
            <li class="fl"><div class="menu_hd"><a href="#">免费注册</a></div></li>
        </ul>
        <ul class="fr 1h">
            <li class="fl"><div class="menu_hd"><a href="#">我的订单</a></div></li>
            <li class="fl"><i class="shortcut_s"></i></li>
            <li class="fl"><div class="menu_hd"><a href="#">个人中心</a></div></li>
            <li class="fl"><i class="shortcut_s"></i></li>
            <li class="fl"><div class="menu_hd"><a href="#">服务中心</a></div></li>
            <li class="fl"><i class="shortcut_s"></i></li>
            <li class="fl">
                <div class="menu">
                    <div class="menu_hd"><a href="#">网站导航</a><b><em></em></b></div>
                    <div class="menu_bd">
                        <ul>
                            <li><a href="#">网站导航</a></li>
                            <li><a href="#">网站导航</a></li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
        <span class="clr"></span>
    </div>
</div>
<div class="header_2013">
    <div class="w">
        <div class="logo_v2013">
            <a href="#">
                <img class="border_r" src="images/logo.png" width="210" height="59">
            </a>
        </div>
        <div class="header_searchbox">
            <form action="#">
                <input name="search" type="text" class="header_search_input" id="find_input" autocomplete="off" x-webkit-speech="" x-webkit-grammar="builtin:search" lang="zh">
                <button type="button" class="header_search_btn">搜索</button>
            </form>
            <ul class="hot_word">
                <li><a class="red" href="#" target="_blank">满99减10元</a></li>
                <li><a target="_blank" href="#">小米电视</a></li>
                <li><a target="_blank" href="#">图书钜惠</a></li>
                <li><a target="_blank" href="#">年货</a></li>
                <li><a target="_blank" href="#">电子产品</a></li>
            </ul>
        </div>
        <div id="cart_box" class="cart_box">
            <a id="cart" class="cart_link" href="#" rel="nofollow">
                <span class="text">去购物车结算</span>
                <img src="images/shopping_icon.png" width="24" height="24" class="cart_gif">
                <!-- 购物车没有物品时，隐藏此num -->
                <!--<span class="num">12</span>-->
                <s class="icon_arrow_right"></s>
            </a>

            <div class="cart_content" id="cart_content">
                <i class="cart-icons"></i>
                <!-- 购物车没有物品时，显示cart_content_null、隐藏cart_content_all -->
                <div class="cart_content_null" style="display: none;">购物车中还没有商品，<br>快去挑选心爱的商品吧！</div>
                <div class="cart_content_all" style="display: block;">
                    <div class="cart_left_time"><span></span></div>
                    <div class="cart_content_center">
                    </div>
                    <div class="con_all">
                        <div class="price_whole"><span>共<span class="num_all">0</span>件商品</span></div>
                        <div><span class="price_gongji">共计<em>￥</em><span class="total_price">0</span></span><a href="#" class="cart_btn" rel="nofollow">去购物车结算</a></div>
                    </div>
                </div>
            </div>
        </div>
        <span class="clr"></span>
    </div>
</div>
<div class="yHeader">
    <div class="shop_Nav">
        <div class="pullDown">
            <h2 class="pullDownTitle"><i></i>全部商品分类</h2>
        </div>

        <ul class="Menu_box">
            <li><a href="" target="_blank" class="yMenua">首页</a></li>
            <li><a href="" target="_blank">秒杀</a></li>
            <li><a href="" target="_blank">大划算</a></li>
            <li><a href="" target="_blank">生鲜</a></li>
            <li><a href="" target="_blank">超市</a></li>
        </ul>
        <div class="fr r_icon"><i class="i01"></i><span>30天退货</span><i class="i02"></i><span>满59包邮</span></div>
    </div>
</div>
<div class="border_top_cart">
<div id="main_show_box">
    <div id="floor_1">
        <div class="nav">
            <div class="w">
                <a href="javascript:;" onclick="reset()">综合排序</a>
                <a href="javascript:;" onclick="sortByPrice(1)">价格从低到高</a>
                <a href="javascript:;" onclick="sortByPrice(-1)" >价格从高到低</a>
                <input type="hidden" id="sort" />
                <div class="price-interval">
                    <input type="number" size="3" id="ltePrice" class="input" placeholder="价格">
                    <span style="margin: 0 5px"> - </span>
                    <input type="number" size="3" id="gtePrice" class="input" placeholder="价格">
                    <input id="find" type="button" class="main-btn" value=" 确定 " onclick="reset()"/>
                </div>
            </div>
        </div>
    </div>
    <div class="floor_goods_wrap">
        <ul>
        </ul>
    </div>
</div>
</div>
<script type="text/javascript" src="js/product-list.js"></script>
<script type="text/javascript">
    function reset(){
        $("#sort").reset();
        $("#ltePrice").reset();
        $("#gtePrice").reset();
        search();
    }
    function sortByPrice(sort){
        $("#sort").val(sort);
        search();
    }
    function search() {
        var url = "/front/goods/search";
        var result = $.ajax({
            url: url,
            data: {
                key: $("#find_input").val(),
                ltePrice: $("#ltePrice").val(),
                gtePrice: $("#gtePrice").val(),
                sort: $("#sort").val()
            },
            success: function (data) {
                let html = "";
                let result = eval(data);
                $.each(result.data.itemList, function (i, item) {
                    let productImage = item.productImageBig;
                    let index = item.productImageBig.indexOf(",");
                    if (index > 0) {
                        productImage = item.productImageBig.substr(0, index);
                    }
                    html += "<li class=\"floor_goods_wrap_li\">\n" +
                        "  <a class=\"floor_goods_img\" href=\"/product/" + item.productId + ".html\"><img src=\"" + productImage + " \"width=\"160\" height=\"160\"></a>\n" +
                        "  <a class=\"floor_goods_tit\">" + item.productName + "</a>\n" +
                        "  <a class=\"floor_goods_txt\">" + item.subTitle + "</a>\n" +
                        "  <a class=\"floor_goods_price\">" + item.salePrice + "</a>\n" +
                        "</li>";
                })
                html += "<div style=\"clear:both;\"></div>";
                $("div.floor_goods_wrap ul").html(html);
            },
            error: function (data) {
            }
        });
    }

    $("button.header_search_btn").on("click", function (){
        search();
    });

    $(document).ready(function () {
        search();
    });
</script>
<script type="text/javascript" src="js/index.js"></script>
<script type="text/javascript" src="js/unslider.min.js"></script>
</body>
</html>