package ai.yunxi.mall.content.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class Panel {
    private Integer id;
    private String name;
    private Integer type;
    private Integer sortOrder;
    private Integer position;
    private Integer limitNum;
    private Integer status;
    private String remark;
    private Date created;
    private Date updated;
}
