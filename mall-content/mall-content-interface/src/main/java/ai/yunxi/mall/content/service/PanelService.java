package ai.yunxi.mall.content.service;

import ai.yunxi.mall.common.TreeNode;
import ai.yunxi.mall.content.service.dto.Panel;

import java.util.List;

public interface PanelService {

    /**
     * 通过id获取板块
     *
     * @param id
     * @return
     */
    Panel getById(int id);

    /**
     * 获取板块类目
     *
     * @param position
     * @param showAll
     * @return
     */
    List<TreeNode> getPanelList(int position, boolean showAll);

    /**
     * 添加板块
     *
     * @param panel
     * @return
     */
    int addPanel(Panel panel);

    /**
     * 更新板块
     *
     * @param panel
     * @return
     */
    int updatePanel(Panel panel);

    /**
     * 删除板块
     *
     * @param id
     * @return
     */
    int deletePanel(int id);
}
