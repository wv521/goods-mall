package ai.yunxi.mall.content.service;

import ai.yunxi.mall.common.ServiceResult;
import ai.yunxi.mall.content.domain.entity.TbPanel;
import ai.yunxi.mall.content.service.dto.AllGoodsResult;
import ai.yunxi.mall.content.service.dto.PanelContent;
import ai.yunxi.mall.product.service.dto.Product;

import java.util.List;

public interface ContentService {

    /**
     * 添加板块内容
     *
     * @param panelContent
     * @return
     */
    int addPanelContent(PanelContent panelContent);

    /**
     * 通过panelId获取板块具体内容
     *
     * @param panelId
     * @return
     */
    ServiceResult getPanelContentListByPanelId(int panelId);

    /**
     * 删除板块内容
     *
     * @param id
     * @return
     */
    int deletePanelContent(int id);

    /**
     * 编辑板块内容
     *
     * @param panelContent
     * @return
     */
    int updateContent(PanelContent panelContent);

    /**
     * 通过id获取板块内容
     *
     * @param id
     * @return
     */
    PanelContent getTbPanelContentById(int id);

    /**
     * 获取首页数据
     *
     * @return
     */
    List<TbPanel> getHome();

    /**
     * 获取商品推荐板块
     *
     * @return
     */
    List<TbPanel> getRecommendGoods();

    /**
     * 获取商品详情
     *
     * @param id
     * @return
     */
    Product getProduct(Long id);

    /**
     * 分页多条件获取全部商品
     *
     * @param page
     * @param size
     * @param sort
     * @param priceGt
     * @param priceLte
     * @return
     */
    AllGoodsResult getAllProduct(int page, int size, String sort, Long cid, int priceGt, int priceLte);

    /**
     * 获取首页缓存
     *
     * @return
     */
    String getIndexRedis();

    /**
     * 同步首页缓存
     *
     * @return
     */
    int updateIndexRedis();

    /**
     * 获取推荐板块缓存
     *
     * @return
     */
    String getRecommendRedis();

    /**
     * 同步推荐板块缓存
     *
     * @return
     */
    int updateRecommendRedis();
}
