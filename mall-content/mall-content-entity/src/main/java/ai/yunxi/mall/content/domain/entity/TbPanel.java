package ai.yunxi.mall.content.domain.entity;

import ai.yunxi.mall.common.TreeNode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Setter
@Getter
public class TbPanel implements Serializable {
    private Integer id;
    private String name;
    private Integer type;
    private Integer sortOrder;
    private Integer position;
    private Integer limitNum;
    private Integer status;
    private String remark;
    private Date created;
    private Date updated;
    private List<TbPanelContent> panelContents;

    public static TreeNode transform(TbPanel tbPanel) {
        TreeNode treeNode = new TreeNode();
        treeNode.setId(tbPanel.getId());
        treeNode.setIsParent(false);
        treeNode.setpId(0);
        treeNode.setName(tbPanel.getName());
        treeNode.setSortOrder(tbPanel.getSortOrder());
        treeNode.setStatus(tbPanel.getStatus());
        treeNode.setRemark(tbPanel.getRemark());
        treeNode.setLimitNum(tbPanel.getLimitNum());
        treeNode.setType(tbPanel.getType());
        return treeNode;
    }
}