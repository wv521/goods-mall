package ai.yunxi.mall.content.service.impl;

import ai.yunxi.mall.common.ServiceResult;
import ai.yunxi.mall.common.exception.CommonException;
import ai.yunxi.mall.common.redis.JedisClient;
import ai.yunxi.mall.content.domain.entity.TbPanel;
import ai.yunxi.mall.content.domain.entity.TbPanelContent;
import ai.yunxi.mall.content.domain.entity.TbPanelContentExample;
import ai.yunxi.mall.content.domain.entity.TbPanelExample;
import ai.yunxi.mall.content.domain.mapper.TbPanelContentMapper;
import ai.yunxi.mall.content.domain.mapper.TbPanelMapper;
import ai.yunxi.mall.content.service.ContentService;
import ai.yunxi.mall.content.service.dto.AllGoodsResult;
import ai.yunxi.mall.content.service.dto.PanelContent;
import ai.yunxi.mall.product.service.ProductService;
import ai.yunxi.mall.product.service.dto.Product;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@DubboService
public class ContentServiceImpl implements ContentService {

    private final static Logger log = LoggerFactory.getLogger(PanelServiceImpl.class);

    @DubboReference
    private ProductService productService;
    @Resource
    private TbPanelMapper tbPanelMapper;
    @Resource
    private TbPanelContentMapper tbPanelContentMapper;

    @Resource
    private JedisClient jedisClient;

    @Value("${PRODUCT_HOME}")
    private String PRODUCT_HOME;

    @Value("${RECOMEED_PANEL_ID}")
    private Integer RECOMEED_PANEL_ID;

    @Value("${RECOMEED_PANEL}")
    private String RECOMEED_PANEL;

    @Override
    public int addPanelContent(PanelContent panelContent) {
        TbPanelContent tbPanelContent = new TbPanelContent();
        BeanUtils.copyProperties(panelContent, tbPanelContent);
        tbPanelContent.setCreated(new Date());
        tbPanelContent.setUpdated(new Date());
        if (tbPanelContentMapper.insert(tbPanelContent) != 1) {
            throw new CommonException("添加首页板块内容失败");
        }
        //同步缓存
        deleteHomeRedis();
        return 1;
    }

    @Override
    public ServiceResult getPanelContentListByPanelId(int panelId) {
        ServiceResult result = new ServiceResult();
        List<TbPanelContent> list = new ArrayList<>();

        TbPanelContentExample example = new TbPanelContentExample();
        TbPanelContentExample.Criteria criteria = example.createCriteria();
        //条件查询
        criteria.andPanelIdEqualTo(panelId);
        list = tbPanelContentMapper.selectByExample(example);
        for (TbPanelContent content : list) {
            if (content.getProductId() != null) {
                Product product = productService.getById(content.getProductId());
                content.setProductName(product.getTitle());
                content.setSalePrice(product.getPrice());
                content.setSubTitle(product.getSubTitle());
            }
        }

        result.setData(list);
        return result;
    }

    @Override
    public int deletePanelContent(int id) {
        if (tbPanelContentMapper.deleteByPrimaryKey(id) != 1) {
            throw new CommonException("删除首页板块失败");
        }
        //同步缓存
        deleteHomeRedis();
        return 1;
    }

    @Override
    public int updateContent(PanelContent panelContent) {
        TbPanelContent tbPanelContent = new TbPanelContent();
        BeanUtils.copyProperties(panelContent, tbPanelContent);
        PanelContent old = getTbPanelContentById(panelContent.getId());
        if (StringUtils.isBlank(panelContent.getPicUrl())) {
            tbPanelContent.setPicUrl(old.getPicUrl());
        }
        if (StringUtils.isBlank(panelContent.getPicUrl2())) {
            tbPanelContent.setPicUrl2(old.getPicUrl2());
        }
        if (StringUtils.isBlank(panelContent.getPicUrl3())) {
            tbPanelContent.setPicUrl3(old.getPicUrl3());
        }
        tbPanelContent.setCreated(old.getCreated());
        tbPanelContent.setUpdated(new Date());
        if (tbPanelContentMapper.updateByPrimaryKey(tbPanelContent) != 1) {
            throw new CommonException("更新板块内容失败");
        }
        //同步缓存
        deleteHomeRedis();
        return 1;
    }

    @Override
    public PanelContent getTbPanelContentById(int id) {
        TbPanelContent tbPanelContent = tbPanelContentMapper.selectByPrimaryKey(id);
        if (tbPanelContent == null) {
            throw new CommonException("通过id获取板块内容失败");
        }
        PanelContent panelContent = new PanelContent();
        BeanUtils.copyProperties(tbPanelContent, panelContent);
        return panelContent;
    }

    @Override
    public List<TbPanel> getHome() {
        List<TbPanel> list = new ArrayList<>();

        //查询缓存
        try {
            //有缓存则读取
            String json = jedisClient.get(PRODUCT_HOME);
            if (json != null) {
                ObjectMapper om = new ObjectMapper();
                list = om.readValue(json, new TypeReference<List<TbPanel>>() {
                });
                log.info("读取了首页缓存");
                return list;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //没有缓存
        TbPanelExample example = new TbPanelExample();
        TbPanelExample.Criteria criteria = example.createCriteria();
        //条件查询
        criteria.andPositionEqualTo(0);
        criteria.andStatusEqualTo(1);
        example.setOrderByClause("sort_order");
        list = tbPanelMapper.selectByExample(example);
        for (TbPanel tbPanel : list) {
            TbPanelContentExample exampleContent = new TbPanelContentExample();
            exampleContent.setOrderByClause("sort_order");
            TbPanelContentExample.Criteria criteriaContent = exampleContent.createCriteria();
            //条件查询
            criteriaContent.andPanelIdEqualTo(tbPanel.getId());
            List<TbPanelContent> contentList = tbPanelContentMapper.selectByExample(exampleContent);
            for (TbPanelContent content : contentList) {
                if (content.getProductId() != null) {
                    Product product = productService.getById(content.getProductId());
                    content.setProductName(product.getTitle());
                    content.setSalePrice(product.getPrice());
                    content.setSubTitle(product.getSubTitle());
                }
            }

            tbPanel.setPanelContents(contentList);
        }

        //把结果添加至缓存
        try {
            ObjectMapper om = new ObjectMapper();
            jedisClient.set(PRODUCT_HOME, om.writeValueAsString(list));
            log.info("添加了首页缓存");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public List<TbPanel> getRecommendGoods() {
        List<TbPanel> list = new ArrayList<>();
        //查询缓存
        try {
            //有缓存则读取
            String json = jedisClient.get(RECOMEED_PANEL);
            if (json != null) {
                ObjectMapper om = new ObjectMapper();
                list = om.readValue(json, new TypeReference<List<TbPanel>>() {
                });
                log.info("读取了推荐板块缓存");
                return list;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        list = getTbPanelAndContentsById(RECOMEED_PANEL_ID);
        //把结果添加至缓存
        try {
            ObjectMapper om = new ObjectMapper();
            jedisClient.set(RECOMEED_PANEL, om.writeValueAsString(list));
            log.info("添加了推荐板块缓存");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    List<TbPanel> getTbPanelAndContentsById(Integer panelId) {
        List<TbPanel> list = new ArrayList<>();
        TbPanelExample example = new TbPanelExample();
        TbPanelExample.Criteria criteria = example.createCriteria();
        //条件查询
        criteria.andIdEqualTo(panelId);
        criteria.andStatusEqualTo(1);
        list = tbPanelMapper.selectByExample(example);
        for (TbPanel tbPanel : list) {
            TbPanelContentExample exampleContent = new TbPanelContentExample();
            exampleContent.setOrderByClause("sort_order");
            TbPanelContentExample.Criteria criteriaContent = exampleContent.createCriteria();
            //条件查询
            criteriaContent.andPanelIdEqualTo(tbPanel.getId());
            List<TbPanelContent> contentList = tbPanelContentMapper.selectByExample(exampleContent);
            for (TbPanelContent content : contentList) {
                if (content.getProductId() != null) {
                    Product product = productService.getById(content.getProductId());
                    content.setProductName(product.getTitle());
                    content.setSalePrice(product.getPrice());
                    content.setSubTitle(product.getSubTitle());
                }
            }

            tbPanel.setPanelContents(contentList);
        }
        return list;
    }

    @Override
    public Product getProduct(Long id) {
        return productService.getById(id);
    }

    @Override
    public AllGoodsResult getAllProduct(int page, int size, String sort, Long categoryId,
                                        int gtePrice, int LtePrice) {
        AllGoodsResult allGoodsResult = new AllGoodsResult();
        List<Product> list = new ArrayList<>();
        //分页执行查询返回结果
        if (page <= 0) {
            page = 1;
        }
        PageHelper.startPage(page, size);

        //判断条件
        String orderCol = "created";
        String orderDir = "desc";
        if (sort.equals("1")) {
            orderCol = "price";
            orderDir = "asc";
        } else if (sort.equals("-1")) {
            orderCol = "price";
            orderDir = "desc";
        } else {
            orderCol = "created";
            orderDir = "desc";
        }

        List<Product> productList = productService.searchList(categoryId, orderCol, orderDir, gtePrice, LtePrice);
        PageInfo<Product> pageInfo = new PageInfo<>(productList);
        allGoodsResult.setData(list);
        allGoodsResult.setTotal((int) pageInfo.getTotal());
        return allGoodsResult;
    }

    @Override
    public String getIndexRedis() {
        try {
            String json = jedisClient.get(PRODUCT_HOME);
            return json;
        } catch (Exception e) {
            log.error(e.toString());
        }
        return "";
    }

    @Override
    public int updateIndexRedis() {
        deleteHomeRedis();
        return 1;
    }

    @Override
    public String getRecommendRedis() {
        try {
            String json = jedisClient.get(RECOMEED_PANEL);
            return json;
        } catch (Exception e) {
            log.error(e.toString());
        }
        return "";
    }

    @Override
    public int updateRecommendRedis() {
        try {
            jedisClient.del(RECOMEED_PANEL);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 1;
    }

    /**
     * 同步首页缓存
     */
    public void deleteHomeRedis() {
        try {
            jedisClient.del(PRODUCT_HOME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
