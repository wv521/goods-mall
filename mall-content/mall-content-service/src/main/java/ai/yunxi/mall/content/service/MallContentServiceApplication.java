package ai.yunxi.mall.content.service;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "ai.yunxi")
@MapperScan("ai.yunxi.mall.content.domain.mapper")
@EnableDubbo(scanBasePackages = {"ai.yunxi.mall.content.service.impl"})
public class MallContentServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MallContentServiceApplication.class, args);
    }

}
