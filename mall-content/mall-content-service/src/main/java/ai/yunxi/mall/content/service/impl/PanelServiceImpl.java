package ai.yunxi.mall.content.service.impl;

import ai.yunxi.mall.common.TreeNode;
import ai.yunxi.mall.common.exception.CommonException;
import ai.yunxi.mall.common.redis.JedisClient;
import ai.yunxi.mall.content.domain.entity.TbPanel;
import ai.yunxi.mall.content.domain.entity.TbPanelExample;
import ai.yunxi.mall.content.domain.mapper.TbPanelMapper;
import ai.yunxi.mall.content.service.PanelService;
import ai.yunxi.mall.content.service.dto.Panel;
import org.apache.dubbo.config.annotation.DubboService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@DubboService
public class PanelServiceImpl implements PanelService {

    private final static Logger log = LoggerFactory.getLogger(PanelServiceImpl.class);

    @Resource
    private TbPanelMapper tbPanelMapper;
    @Resource
    private JedisClient jedisClient;

    @Value("${PRODUCT_HOME}")
    private String PRODUCT_HOME;

    @Override
    public Panel getById(int id) {
        TbPanel tbPanel = tbPanelMapper.selectByPrimaryKey(id);
        if (tbPanel == null) {
            throw new CommonException("通过id获得板块失败");
        }
        Panel pane = new Panel();
        BeanUtils.copyProperties(tbPanel, pane);
        return pane;
    }

    @Override
    public List<TreeNode> getPanelList(int position, boolean showAll) {
        TbPanelExample example = new TbPanelExample();
        TbPanelExample.Criteria criteria = example.createCriteria();
        if (position == 0 && !showAll) {
            //除去非轮播
            criteria.andTypeNotEqualTo(0);
        } else if (position == -1) {
            //仅含轮播
            position = 0;
            criteria.andTypeEqualTo(0);
        }
        //首页板块
        criteria.andPositionEqualTo(position);
        example.setOrderByClause("sort_order");
        List<TbPanel> panelList = tbPanelMapper.selectByExample(example);
        List<TreeNode> list = new ArrayList<>();
        for (TbPanel tbPanel : panelList) {
            TreeNode treeNode = TbPanel.transform(tbPanel);
            list.add(treeNode);
        }

        return list;
    }

    @Override
    public int addPanel(Panel panel) {
        if (panel.getType() == 0) {
            TbPanelExample example = new TbPanelExample();
            TbPanelExample.Criteria criteria = example.createCriteria();
            criteria.andTypeEqualTo(0);
            List<TbPanel> list = tbPanelMapper.selectByExample(example);
            if (list != null && list.size() > 0) {
                throw new CommonException("已有轮播图板块,轮播图仅能添加1个!");
            }
        }

        TbPanel tbPanel = new TbPanel();
        BeanUtils.copyProperties(panel, tbPanel);
        tbPanel.setCreated(new Date());
        tbPanel.setUpdated(new Date());
        if (tbPanelMapper.insert(tbPanel) != 1) {
            throw new CommonException("添加板块失败");
        }
        //同步缓存
        deleteHomeRedis();
        return 1;
    }

    @Override
    public int updatePanel(Panel panel) {
        Panel old = getById(panel.getId());
        TbPanel tbPanel = new TbPanel();
        BeanUtils.copyProperties(panel, tbPanel);
        tbPanel.setUpdated(new Date());
        if (tbPanelMapper.updateByPrimaryKey(tbPanel) != 1) {
            throw new CommonException("更新板块失败");
        }
        //同步缓存
        deleteHomeRedis();
        return 1;
    }

    @Override
    public int deletePanel(int id) {
        if (tbPanelMapper.deleteByPrimaryKey(id) != 1) {
            throw new CommonException("删除内容分类失败");
        }
        //同步缓存
        deleteHomeRedis();
        return 1;
    }

    /**
     * 同步首页缓存
     */
    public void deleteHomeRedis() {
        try {
            jedisClient.del(PRODUCT_HOME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
