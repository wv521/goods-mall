package ai.yunxi.mall.core.domain.mapper;

import ai.yunxi.mall.core.domain.entity.TbCart;
import ai.yunxi.mall.core.domain.entity.TbCartExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TbCartMapper {
    long countByExample(TbCartExample example);

    int deleteByExample(TbCartExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TbCart record);

    int insertSelective(TbCart record);

    List<TbCart> selectByExample(TbCartExample example);

    TbCart selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TbCart record,
                                 @Param("example") TbCartExample example);

    int updateByExample(@Param("record") TbCart record,
                        @Param("example") TbCartExample example);

    int updateByPrimaryKeySelective(TbCart record);

    int updateByPrimaryKey(TbCart record);

    List<TbCart> selectByCondition(@Param("userId") long userId);
}