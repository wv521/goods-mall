package ai.yunxi.mall.core.domain.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
public class TbCart implements Serializable {
    private Long id;
    private long userId;
    private long productId;
    private BigDecimal price;
    private Integer num;
    private Date created;
    private Date updated;
}