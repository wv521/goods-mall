package ai.yunxi.mall.core.domain.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class TbPermission implements Serializable {
    private Integer id;
    private Integer roleId;
    private String name;
    private String permission;
}