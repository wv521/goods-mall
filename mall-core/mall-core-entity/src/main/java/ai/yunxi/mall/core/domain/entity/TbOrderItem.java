package ai.yunxi.mall.core.domain.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
public class TbOrderItem implements Serializable {
    private Long id;
    private Long itemId;
    private Long orderId;
    private Integer num;
    private String title;
    private BigDecimal price;
    private BigDecimal totalFee;
    private String picPath;
    private Integer total;
}