package ai.yunxi.mall.core.domain.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class TbShiroFilter implements Serializable {
    private Integer id;
    private String name;
    private String perms;
    private Integer sortOrder;
}