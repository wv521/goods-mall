package ai.yunxi.mall.core.domain.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class TbRole implements Serializable {
    private Integer id;
    private String name;
    private String description;
    private Integer[] roles;
}