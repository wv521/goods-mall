package ai.yunxi.mall.core.domain.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
public class TbManager implements Serializable {
    private Long id;
    private String username;
    private String password;
    private String phone;
    private String email;
    private Date created;
    private Date updated;
    private String sex;
    private String address;
    private Integer state;
    private String description;
    private Integer roleId;
    private String file;
    private String roleNames;
}