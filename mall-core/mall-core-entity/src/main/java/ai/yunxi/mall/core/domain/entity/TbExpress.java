package ai.yunxi.mall.core.domain.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
public class TbExpress implements Serializable {
    private Integer id;
    private String expressName;
    private Integer sortOrder;
    private Date created;
    private Date updated;
}