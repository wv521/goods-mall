package ai.yunxi.mall.core.service.impl;

import ai.yunxi.mall.common.ServiceResult;
import ai.yunxi.mall.common.exception.CommonException;
import ai.yunxi.mall.common.redis.JedisClient;
import ai.yunxi.mall.core.service.dto.Order;
import ai.yunxi.mall.core.domain.entity.*;
import ai.yunxi.mall.core.domain.mapper.TbOrderItemMapper;
import ai.yunxi.mall.core.domain.mapper.TbOrderMapper;
import ai.yunxi.mall.core.domain.mapper.TbOrderShippingMapper;
import ai.yunxi.mall.core.service.OrderService;
import ai.yunxi.mall.core.service.dto.OrderItem;
import ai.yunxi.mall.user.service.dto.AddressInfo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.BeanUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@DubboService
public class OrderServiceImpl implements OrderService {

    @Resource
    private TbOrderMapper tbOrderMapper;

    @Resource
    private TbOrderItemMapper tbOrderItemMapper;

    @Resource
    private TbOrderShippingMapper tbOrderShippingMapper;

    @Resource
    private JedisClient jedisClient;

    @Resource
    private EmailServiceImpl emailService;

    @Override
    public ServiceResult getOrderList(int draw, int start, int length, String search, String orderCol, String orderDir) {
        ServiceResult result = new ServiceResult();
        //分页
        PageHelper.startPage(start / length + 1, length);
        List<TbOrder> list = tbOrderMapper.selectByMulti("%" + search + "%", orderCol, orderDir);
        PageInfo<TbOrder> pageInfo = new PageInfo<>(list);
        result.setRecordsFiltered((int) pageInfo.getTotal());
        result.setRecordsTotal((int) pageInfo.getTotal());
        result.setDraw(draw);
        result.setData(list);
        return result;
    }

    @Override
    public Order getOrder(Long orderId) {
        Order order = new Order();
        TbOrder tbOrder = tbOrderMapper.selectByPrimaryKey(orderId);
        if (tbOrder == null) {
            throw new CommonException("通过id获取订单失败");
        }

        BeanUtils.copyProperties(tbOrder, order);
        //address
        TbOrderShipping tbOrderShipping = tbOrderShippingMapper.selectByPrimaryKey(tbOrder.getOrderId());
        AddressInfo address = new AddressInfo();
        address.setUserName(tbOrderShipping.getReceiverName());
        address.setStreetName(tbOrderShipping.getReceiverAddress());
        address.setTel(tbOrderShipping.getReceiverPhone());
        order.setAddressInfo(address);
        //goodsList
        TbOrderItemExample exampleItem = new TbOrderItemExample();
        TbOrderItemExample.Criteria criteriaItem = exampleItem.createCriteria();
        criteriaItem.andOrderIdEqualTo(tbOrder.getOrderId());
        List<TbOrderItem> items = tbOrderItemMapper.selectByExample(exampleItem);
        List<OrderItem> listProduct = items.stream().map(tbOrderItem -> {
            OrderItem orderItem = new OrderItem();
            BeanUtils.copyProperties(tbOrderItem, orderItem);
            return orderItem;
        }).collect(Collectors.toList());
        order.setItems(listProduct);
        return order;
    }

    @Override
    public Long countOrder() {
        TbOrderExample example = new TbOrderExample();
        return tbOrderMapper.countByExample(example);
    }

    @Override
    public int deliver(Long orderId, String shippingName, String shippingCode, BigDecimal postFee) {
        TbOrder o = tbOrderMapper.selectByPrimaryKey(orderId);
        o.setShippingName(shippingName);
        o.setShippingCode(shippingCode);
        o.setPostFee(postFee);
        o.setConsignTime(new Date());
        o.setUpdateTime(new Date());
        // 0、未付款，1、已付款，2、未发货，3、已发货，4、交易成功，5、交易关闭
        o.setStatus(3);
        tbOrderMapper.updateByPrimaryKey(o);
        return 1;
    }

    @Override
    public int remark(Long orderId, String message) {
        TbOrder tbOrder = tbOrderMapper.selectByPrimaryKey(orderId);
        tbOrder.setBuyerMessage(message);
        tbOrder.setUpdateTime(new Date());
        tbOrderMapper.updateByPrimaryKey(tbOrder);
        return 1;
    }

    @Override
    public int cancelByAdmin(Long orderId) {
        TbOrder tbOrder = tbOrderMapper.selectByPrimaryKey(orderId);
        tbOrder.setCloseTime(new Date());
        tbOrder.setUpdateTime(new Date());
        // 0、未付款，1、已付款，2、未发货，3、已发货，4、交易成功，5、交易关闭
        tbOrder.setStatus(5);
        tbOrderMapper.updateByPrimaryKey(tbOrder);
        return 1;
    }
}
