package ai.yunxi.mall.core.service.impl;

import ai.yunxi.mall.core.domain.entity.TbExpress;
import ai.yunxi.mall.core.domain.entity.TbExpressExample;
import ai.yunxi.mall.core.domain.mapper.TbExpressMapper;
import ai.yunxi.mall.core.service.ExpressService;
import ai.yunxi.mall.core.service.dto.Express;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@DubboService
public class ExpressServiceImpl implements ExpressService {
    @Resource
    private TbExpressMapper tbExpressMapper;

    @Override
    public List<Express> getExpressList() {
        TbExpressExample example = new TbExpressExample();
        example.setOrderByClause("sort_order asc");
        List<TbExpress> list = tbExpressMapper.selectByExample(example);
        return list.stream().map(tbExpress -> {
            Express express = new Express();
            BeanUtils.copyProperties(tbExpress, express);
            return express;
        }).collect(Collectors.toList());
    }

    @Override
    public int addExpress(Express express) {
        TbExpress tbExpress = new TbExpress();
        BeanUtils.copyProperties(express, tbExpress);
        tbExpress.setCreated(new Date());
        tbExpressMapper.insert(tbExpress);
        return 1;
    }

    @Override
    public int updateExpress(Express express) {
        TbExpress tbExpress = new TbExpress();
        BeanUtils.copyProperties(express, tbExpress);
        tbExpress.setUpdated(new Date());
        tbExpressMapper.updateByPrimaryKey(tbExpress);
        return 1;
    }

    @Override
    public int deleteExpress(int id) {
        tbExpressMapper.deleteByPrimaryKey(id);
        return 1;
    }
}
