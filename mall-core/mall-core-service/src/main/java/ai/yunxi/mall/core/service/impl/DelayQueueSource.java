package ai.yunxi.mall.core.service.impl;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface DelayQueueSource {

    String OUTPUT = "delayOutput";

    @Output(DelayQueueSource.OUTPUT)
    MessageChannel output();
}
