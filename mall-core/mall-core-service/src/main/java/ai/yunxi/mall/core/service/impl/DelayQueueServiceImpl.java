package ai.yunxi.mall.core.service.impl;

import ai.yunxi.mall.common.exception.CommonException;
import ai.yunxi.mall.core.domain.entity.TbOrder;
import ai.yunxi.mall.core.domain.entity.TbOrderItem;
import ai.yunxi.mall.core.domain.entity.TbOrderShipping;
import ai.yunxi.mall.core.domain.mapper.TbOrderItemMapper;
import ai.yunxi.mall.core.domain.mapper.TbOrderMapper;
import ai.yunxi.mall.core.domain.mapper.TbOrderShippingMapper;
import ai.yunxi.mall.core.service.DelayQueueService;
import ai.yunxi.mall.core.service.UserOrderService;
import ai.yunxi.mall.core.service.dto.Order;
import ai.yunxi.mall.core.service.dto.OrderShipping;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Slf4j
@Service
@EnableBinding({DelayQueueSource.class, DelayQueueSink.class})
public class DelayQueueServiceImpl implements DelayQueueService {

    @Resource
    private UserOrderService userOrderService;

    @Resource
    private DelayQueueSource source;

    @Override
    public void send(Order order, long delay) {
        Message<Order> message = MessageBuilder.withPayload(order)
                .setHeader("x-delay", delay).build();
        source.output().send(message);
        log.info("发布延迟队列消息，OrderId: " + order.getOrderId());
    }

    @Override
    @StreamListener(DelayQueueSink.INPUT)
    public void receive(String message) {
        ObjectMapper om = new ObjectMapper();
        Order order = null;

        try {
            order = om.readValue(message, Order.class);
        } catch (JsonProcessingException e) {
            throw new CommonException("生成订单信息失败，OrderId: " + order.getOrderId());
        }

        log.info("订单超时未支付，OrderId: " + order.getOrderId());
        //if (userOrderService.saveOrder(order) != 1)
        //    throw new CommonException("保存订单信息失败，OrderId:" + order.getOrderId());
    }
}
