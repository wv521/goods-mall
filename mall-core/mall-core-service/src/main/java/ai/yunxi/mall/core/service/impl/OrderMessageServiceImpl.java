package ai.yunxi.mall.core.service.impl;

import ai.yunxi.mall.common.exception.CommonException;
import ai.yunxi.mall.common.redis.JedisClient;
import ai.yunxi.mall.core.domain.entity.TbOrder;
import ai.yunxi.mall.core.domain.entity.TbOrderItem;
import ai.yunxi.mall.core.domain.entity.TbOrderShipping;
import ai.yunxi.mall.core.domain.mapper.TbOrderItemMapper;
import ai.yunxi.mall.core.domain.mapper.TbOrderMapper;
import ai.yunxi.mall.core.domain.mapper.TbOrderShippingMapper;
import ai.yunxi.mall.core.service.OrderMessageService;
import ai.yunxi.mall.core.service.UserOrderService;
import ai.yunxi.mall.core.service.dto.Order;
import ai.yunxi.mall.core.service.dto.OrderShipping;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Slf4j
@Service
@EnableBinding({Source.class, Sink.class})
public class OrderMessageServiceImpl implements OrderMessageService {

    @Resource
    private UserOrderService userOrderService;

    @Resource
    private Source source;

    @Override
    public boolean send(Order order) {
        ObjectMapper om = new ObjectMapper();
        String message = null;

        try {
            message = om.writeValueAsString(order);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        if (StringUtils.isBlank(message))
            return false;
        else {
            Message msg = MessageBuilder.withPayload(message).build();
            return source.output().send(msg);
        }
    }

    @Override
    @StreamListener(Sink.INPUT)
    public void receive(String message) {
        ObjectMapper om = new ObjectMapper();
        Order order = null;

        try {
            order = om.readValue(message, Order.class);
            userOrderService.cancelOrder(order.getOrderId());
        } catch (JsonProcessingException e) {
            throw new CommonException("生成订单信息失败");
        }

        if (userOrderService.saveOrder(order) != 1)
            throw new CommonException("保存订单信息失败，OrderId:" + order.getOrderId());
    }
}
