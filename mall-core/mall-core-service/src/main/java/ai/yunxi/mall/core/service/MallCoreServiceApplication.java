package ai.yunxi.mall.core.service;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScans;

@SpringBootApplication(scanBasePackages = "ai.yunxi")
@MapperScan("ai.yunxi.mall.core.domain.mapper")
@EnableDubbo(scanBasePackages = {"ai.yunxi.mall.core.service.impl"})
public class MallCoreServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MallCoreServiceApplication.class, args);
    }

}
