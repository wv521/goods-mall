package ai.yunxi.mall.core.service.impl;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.SubscribableChannel;

interface DelayQueueSink {

    String INPUT = "delayInput";

    @Input(DelayQueueSink.INPUT)
    SubscribableChannel input();
}
