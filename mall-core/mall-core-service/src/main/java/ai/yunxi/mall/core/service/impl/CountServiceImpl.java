package ai.yunxi.mall.core.service.impl;

import ai.yunxi.mall.common.Constants;
import ai.yunxi.mall.common.utils.DateUtils;
import ai.yunxi.mall.core.service.dto.OrderChartData;
import ai.yunxi.mall.core.domain.mapper.TbOrderMapper;
import ai.yunxi.mall.core.service.CountService;
import org.apache.dubbo.config.annotation.DubboService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.*;

@DubboService
public class CountServiceImpl implements CountService {

    private final static Logger log = LoggerFactory.getLogger(CountServiceImpl.class);

    @Autowired
    private TbOrderMapper tbOrderMapper;

    @Override
    public List<OrderChartData> getOrderCountData(int type, Date startTime, Date endTime, int year) {
        List<OrderChartData> fullData = new ArrayList<>();
        if (type == Constants.THIS_WEEK) {
            //本周
            Map<Date, BigDecimal> data = tbOrderMapper.selectOrderChart(DateUtils.getBeginDayOfWeek(), DateUtils.getEndDayOfWeek());
            fullData = getFullData(data, DateUtils.getBeginDayOfWeek(), DateUtils.getEndDayOfWeek());
        } else if (type == Constants.THIS_MONTH) {
            //本月
            Map<Date, BigDecimal> data = tbOrderMapper.selectOrderChart(DateUtils.getBeginDayOfMonth(), DateUtils.getEndDayOfMonth());
            fullData = getFullData(data, DateUtils.getBeginDayOfMonth(), DateUtils.getEndDayOfMonth());
        } else if (type == Constants.LAST_MONTH) {
            //上个月
            Map<Date, BigDecimal> data = tbOrderMapper.selectOrderChart(DateUtils.getBeginDayOfLastMonth(), DateUtils.getEndDayOfLastMonth());
            fullData = getFullData(data, DateUtils.getBeginDayOfLastMonth(), DateUtils.getEndDayOfLastMonth());
        } else if (type == Constants.CUSTOM_DATE) {
            //自定义
            Map<Date, BigDecimal> data = tbOrderMapper.selectOrderChart(startTime, endTime);
            fullData = getFullData(data, startTime, endTime);
        } else if (type == Constants.CUSTOM_YEAR) {
            Map<Date, BigDecimal> data = tbOrderMapper.selectOrderChartByYear(year);
            fullData = getFullYearData(data, year);
        }
        return fullData;
    }

    /**
     * 无数据补0
     *
     * @param startTime
     * @param endTime
     */
    public List<OrderChartData> getFullData(Map<Date, BigDecimal> data, Date startTime,
                                            Date endTime) {
        List<OrderChartData> fullData = new ArrayList<>();
        //相差
        long betweenDay = DateUtils.betweenDays(startTime, endTime);
        //起始时间
        Date everyday = startTime;
        for (int i = 0; i <= betweenDay; i++) {
            if (data.containsKey(everyday)) {
                OrderChartData chartData = new OrderChartData();
                chartData.setTime(everyday);
                chartData.setMoney(data.get(everyday));
                fullData.add(chartData);
            } else {
                OrderChartData orderChartData = new OrderChartData();
                orderChartData.setTime(everyday);
                orderChartData.setMoney(new BigDecimal("0"));
                fullData.add(orderChartData);
            }

            // 时间+1天
            everyday = DateUtils.nextDay(everyday);
        }
        return fullData;
    }

    /**
     * 无数据补0
     *
     * @param data
     * @param year
     * @return
     */
    public List<OrderChartData> getFullYearData(Map<Date, BigDecimal> data, int year) {
        List<OrderChartData> fullData = new ArrayList<>();
        //起始月份
        Date everyMonth = DateUtils.getBeginDayOfYear(year);
        for (int i = 0; i < 12; i++) {
            if (data.containsKey(everyMonth)) {
                OrderChartData chartData = new OrderChartData();
                chartData.setTime(everyMonth);
                chartData.setMoney(data.get(everyMonth));
                fullData.add(chartData);
            } else {
                OrderChartData orderChartData = new OrderChartData();
                orderChartData.setTime(everyMonth);
                orderChartData.setMoney(new BigDecimal("0"));
                fullData.add(orderChartData);
            }

            // 月份+1
            everyMonth = DateUtils.nextMonth(everyMonth);
        }
        return fullData;
    }
}
