package ai.yunxi.mall.core.service.impl;

import ai.yunxi.mall.core.domain.entity.TbCart;
import ai.yunxi.mall.core.domain.entity.TbCartExample;
import ai.yunxi.mall.core.domain.mapper.TbCartMapper;
import ai.yunxi.mall.core.service.CartService;
import ai.yunxi.mall.core.service.dto.CartItem;
import ai.yunxi.mall.product.service.ProductService;
import ai.yunxi.mall.product.service.dto.Product;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@DubboService
public class CartServiceImpl implements CartService {

    private final static Logger log = LoggerFactory.getLogger(CartServiceImpl.class);

    @DubboReference
    private ProductService productService;

    @Resource
    private TbCartMapper tbCartMapper;

    @Override
    public int addItem(long userId, long productId, int num) {
        //如果不存在，根据商品id取商品信息
        Product product = productService.getById(productId);
        if (product == null) {
            return 0;
        }

        TbCartExample tbCartExample = new TbCartExample();
        TbCartExample.Criteria criteria = tbCartExample.createCriteria();
        criteria.andUserIdEqualTo(userId);
        criteria.andProductIdEqualTo(productId);

        List<TbCart> list = tbCartMapper.selectByExample(tbCartExample);
        TbCart tbCart = new TbCart();
        tbCart.setUserId(userId);
        tbCart.setProductId(productId);
        if (null != null && !list.isEmpty()) {
            tbCart.setNum(list.get(0).getNum() + 1);
            tbCart.setUpdated(new Date());
        } else {
            tbCart.setNum(1);
            tbCart.setCreated(new Date());
        }

        return tbCartMapper.insert(tbCart);
    }

    @Override
    public List<CartItem> getItems(long userId) {
        TbCartExample tbCartExample = new TbCartExample();
        TbCartExample.Criteria criteria = tbCartExample.createCriteria();
        criteria.andUserIdEqualTo(userId);
        List<TbCart> list = tbCartMapper.selectByExample(tbCartExample);
        List<CartItem> itemList = list.stream().map(tbCart -> {
            CartItem cartItem = new CartItem();
            cartItem.setItemId(tbCart.getProductId());
            cartItem.setPurchaseNum(tbCart.getNum());
            return cartItem;
        }).collect(Collectors.toList());
        return itemList;
    }

    @Override
    public int setItemNum(long userId, long productId, int num, String checked) {
        TbCartExample tbCartExample = new TbCartExample();
        TbCartExample.Criteria criteria = tbCartExample.createCriteria();
        criteria.andUserIdEqualTo(userId);
        criteria.andProductIdEqualTo(productId);
        List<TbCart> list = tbCartMapper.selectByExample(tbCartExample);

        if (null != list && !list.isEmpty()) {
            TbCart tbCart = list.get(0);
            tbCart.setNum(num);
            return tbCartMapper.updateByPrimaryKey(tbCart);
        } else
            return 0;
    }

    @Override
    public int checkAll(long userId, String checked) {
        return 1;
    }

    @Override
    public int deleteItem(long userId, long productId) {
        TbCartExample tbCartExample = new TbCartExample();
        TbCartExample.Criteria criteria = tbCartExample.createCriteria();
        criteria.andUserIdEqualTo(userId);
        criteria.andProductIdEqualTo(productId);
        tbCartMapper.deleteByExample(tbCartExample);
        return 1;
    }

    @Override
    public int delChecked(long userId) {
        return 1;
    }
}
