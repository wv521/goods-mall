package ai.yunxi.mall.core.service.impl;

import ai.yunxi.mall.common.ServiceResult;
import ai.yunxi.mall.common.exception.CommonException;
import ai.yunxi.mall.core.domain.entity.*;
import ai.yunxi.mall.core.domain.mapper.TbBaseMapper;
import ai.yunxi.mall.core.domain.mapper.TbLogMapper;
import ai.yunxi.mall.core.domain.mapper.TbOrderItemMapper;
import ai.yunxi.mall.core.domain.mapper.TbShiroFilterMapper;
import ai.yunxi.mall.core.service.SystemService;
import ai.yunxi.mall.core.service.dto.Base;
import ai.yunxi.mall.core.service.dto.OrderItem;
import ai.yunxi.mall.core.service.dto.ShiroFilter;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@DubboService
public class SystemServiceImpl implements SystemService {

    @Resource
    private TbShiroFilterMapper tbShiroFilterMapper;
    @Resource
    private TbBaseMapper tbBaseMapper;
    @Resource
    private TbOrderItemMapper tbOrderItemMapper;

    @Value("${BASE_ID}")
    private String BASE_ID;

    @Override
    public List<ShiroFilter> getShiroFilter() {
        TbShiroFilterExample example = new TbShiroFilterExample();
        example.setOrderByClause("sort_order");
        List<TbShiroFilter> list = new ArrayList<>();// tbShiroFilterMapper.selectByExample(example);
        if (list == null) {
            throw new CommonException("获取shiro过滤链失败");
        }
        return list.stream().map(tbShiroFilter -> {
            ShiroFilter shiroFilter = new ShiroFilter();
            BeanUtils.copyProperties(tbShiroFilter, shiroFilter);
            return shiroFilter;
        }).collect(Collectors.toList());
    }

    @Override
    public Long countShiroFilter() {
        TbShiroFilterExample example = new TbShiroFilterExample();
        Long result = tbShiroFilterMapper.countByExample(example);
        if (result == null) {
            throw new CommonException("获取shiro过滤链数目失败");
        }
        return result;
    }

    @Override
    public int addShiroFilter(ShiroFilter shiroFilter) {
        TbShiroFilter tbShiroFilter = new TbShiroFilter();
        BeanUtils.copyProperties(shiroFilter, tbShiroFilter);
        if (tbShiroFilterMapper.insert(tbShiroFilter) != 1) {
            throw new CommonException("添加shiro过滤链失败");
        }
        return 1;
    }

    @Override
    public int updateShiroFilter(ShiroFilter shiroFilter) {
        TbShiroFilter tbShiroFilter = new TbShiroFilter();
        BeanUtils.copyProperties(shiroFilter, tbShiroFilter);
        if (tbShiroFilterMapper.updateByPrimaryKey(tbShiroFilter) != 1) {
            throw new CommonException("更新shiro过滤链失败");
        }
        return 1;
    }

    @Override
    public int deleteShiroFilter(int id) {
        if (tbShiroFilterMapper.deleteByPrimaryKey(id) != 1) {
            throw new CommonException("删除shiro过滤链失败");
        }
        return 1;
    }

    @Override
    public Base getBase() {
        TbBase tbBase = tbBaseMapper.selectByPrimaryKey(Integer.valueOf(BASE_ID));
        if (tbBase == null) {
            throw new CommonException("获取基础设置失败");
        }
        Base base = new Base();
        BeanUtils.copyProperties(tbBase, base);
        return base;
    }

    @Override
    public int updateBase(Base base) {
        TbBase tbBase = new TbBase();
        BeanUtils.copyProperties(base, tbBase);
        if (tbBaseMapper.updateByPrimaryKey(tbBase) != 1) {
            throw new CommonException("更新基础设置失败");
        }
        return 1;
    }

    @Override
    public OrderItem getWeekHot() {
        List<TbOrderItem> list = tbOrderItemMapper.getWeekHot();
        if (list == null) {
            throw new CommonException("获取热销商品数据失败");
        }
        if (list.size() == 0) {
            OrderItem orderItem = new OrderItem();
            orderItem.setTotal(0);
            orderItem.setTitle("暂无数据");
            orderItem.setPicPath("");
            return orderItem;
        }
        OrderItem orderItem = new OrderItem();
        BeanUtils.copyProperties(list.get(0), orderItem);
        return orderItem;
    }
}
