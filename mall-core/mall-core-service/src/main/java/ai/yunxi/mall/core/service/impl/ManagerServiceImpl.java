package ai.yunxi.mall.core.service.impl;

import ai.yunxi.mall.common.ServiceResult;
import ai.yunxi.mall.common.exception.CommonException;
import ai.yunxi.mall.core.domain.entity.*;
import ai.yunxi.mall.core.domain.mapper.TbManagerMapper;
import ai.yunxi.mall.core.domain.mapper.TbPermissionMapper;
import ai.yunxi.mall.core.domain.mapper.TbRoleMapper;
import ai.yunxi.mall.core.domain.mapper.TbRolePermMapper;
import ai.yunxi.mall.core.service.ManagerService;
import ai.yunxi.mall.core.service.dto.Manager;
import ai.yunxi.mall.core.service.dto.Permission;
import ai.yunxi.mall.core.service.dto.Role;
import org.apache.dubbo.config.annotation.DubboService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@DubboService
public class ManagerServiceImpl implements ManagerService {

    private static final Logger log = LoggerFactory.getLogger(ManagerServiceImpl.class);

    @Resource
    private TbManagerMapper tbManagerMapper;
    @Resource
    private TbRoleMapper tbRoleMapper;
    @Resource
    private TbPermissionMapper tbPermissionMapper;
    @Resource
    private TbRolePermMapper tbRolePermMapper;

    @Override
    public Manager getUserByUsername(String username) {
        List<TbManager> list;
        TbManagerExample example = new TbManagerExample();
        TbManagerExample.Criteria criteria = example.createCriteria();
        criteria.andUsernameEqualTo(username);
        criteria.andStateEqualTo(1);
        try {
            list = tbManagerMapper.selectByExample(example);
        } catch (Exception e) {
            throw new CommonException("通过ID获取用户信息失败");
        }

        Manager manager = new Manager();
        if (!list.isEmpty()) {
            BeanUtils.copyProperties(list.get(0), manager);
            return manager;
        }
        return null;
    }

    @Override
    public Set<String> getRoles(String username) {
        return tbManagerMapper.getRoles(username);
    }

    @Override
    public Set<String> getPermissions(String username) {
        return tbManagerMapper.getPermissions(username);
    }

    @Override
    public ServiceResult getRoleList() {
        ServiceResult result = new ServiceResult();
        List<Role> list = new ArrayList<>();
        TbRoleExample example = new TbRoleExample();
        List<TbRole> list1 = tbRoleMapper.selectByExample(example);
        if (list1 == null) {
            throw new CommonException("获取角色列表失败");
        }

        for (TbRole tbRole : list1) {
            Role role = new Role();
            role.setId(tbRole.getId());
            role.setName(tbRole.getName());
            role.setDescription(tbRole.getDescription());
            List<String> permissions = tbManagerMapper.getPermsByRoleId(tbRole.getId());
            String names = "";
            if (permissions.size() > 1) {
                names += permissions.get(0);
                for (int i = 1; i < permissions.size(); i++) {
                    names += "|" + permissions.get(i);
                }
            } else if (permissions.size() == 1) {
                names += permissions.get(0);
            }

            role.setPermissions(names);
            list.add(role);
        }
        result.setData(list);
        return result;
    }

    @Override
    public List<Role> getAllRoles() {
        TbRoleExample example = new TbRoleExample();
        List<TbRole> list = tbRoleMapper.selectByExample(example);
        if (list == null) {
            throw new CommonException("获取所有角色列表失败");
        }
        return list.stream().map(tbRole -> {
            Role role = new Role();
            BeanUtils.copyProperties(tbRole, role);
            return role;
        }).collect(Collectors.toList());
    }

    @Override
    public int addRole(Role role) {
        TbRole tbRole = new TbRole();
        BeanUtils.copyProperties(role, tbRole);
        if (getRoleByRoleName(tbRole.getName()) != null) {
            throw new CommonException("该角色名已存在");
        }
        if (tbRoleMapper.insert(tbRole) != 1) {
            throw new CommonException("添加角色失败");
        }
        if (tbRole.getRoles() != null) {
            Role newRole = getRoleByRoleName(tbRole.getName());
            for (int i = 0; i < tbRole.getRoles().length; i++) {
                TbRolePerm tbRolePerm = new TbRolePerm();
                tbRolePerm.setRoleId(newRole.getId());
                tbRolePerm.setPermissionId(tbRole.getRoles()[i]);
                if (tbRolePermMapper.insert(tbRolePerm) != 1) {
                    throw new CommonException("添加角色-权限失败");
                }
            }
        }
        return 1;
    }

    @Override
    public Role getRoleByRoleName(String roleName) {
        TbRoleExample example = new TbRoleExample();
        TbRoleExample.Criteria criteria = example.createCriteria();
        criteria.andNameEqualTo(roleName);
        List<TbRole> list = new ArrayList<>();
        try {
            list = tbRoleMapper.selectByExample(example);
        } catch (Exception e) {
            throw new CommonException("通过角色名获取角色失败");
        }
        if (!list.isEmpty()) {
            Role role = new Role();
            BeanUtils.copyProperties(list.get(0), role);
            return role;
        }
        return null;
    }

    @Override
    public boolean getRoleByEditName(int id, String roleName) {
        TbRole tbRole = tbRoleMapper.selectByPrimaryKey(id);
        Role newRole = null;
        if (tbRole == null) {
            throw new CommonException("通过ID获取角色失败");
        }
        if (!tbRole.getName().equals(roleName)) {
            newRole = getRoleByRoleName(roleName);
        }
        return newRole == null;
    }

    @Override
    public int updateRole(Role role) {
        TbRole tbRole = new TbRole();
        BeanUtils.copyProperties(role, tbRole);
        if (!getRoleByEditName(role.getId(), role.getName())) {
            throw new CommonException("该角色名已存在");
        }
        if (tbRoleMapper.updateByPrimaryKey(tbRole) != 1) {
            throw new CommonException("更新角色失败");
        }
        if (tbRole.getRoles() != null) {
            //删除已有角色-权限
            TbRolePermExample example = new TbRolePermExample();
            TbRolePermExample.Criteria criteria = example.createCriteria();
            criteria.andRoleIdEqualTo(tbRole.getId());
            List<TbRolePerm> list = tbRolePermMapper.selectByExample(example);
            if (list != null) {
                for (TbRolePerm tbRolePerm : list) {
                    if (tbRolePermMapper.deleteByPrimaryKey(tbRolePerm.getId()) != 1) {
                        throw new CommonException("删除角色权限失败");
                    }
                }
            }
            //新增
            for (int i = 0; i < tbRole.getRoles().length; i++) {
                TbRolePerm tbRolePerm = new TbRolePerm();
                tbRolePerm.setRoleId(tbRole.getId());
                tbRolePerm.setPermissionId(tbRole.getRoles()[i]);

                if (tbRolePermMapper.insert(tbRolePerm) != 1) {
                    throw new CommonException("编辑角色-权限失败");
                }
            }
        } else {
            TbRolePermExample example = new TbRolePermExample();
            TbRolePermExample.Criteria criteria = example.createCriteria();
            criteria.andRoleIdEqualTo(tbRole.getId());
            List<TbRolePerm> list = tbRolePermMapper.selectByExample(example);
            if (list != null) {
                for (TbRolePerm tbRolePerm : list) {
                    if (tbRolePermMapper.deleteByPrimaryKey(tbRolePerm.getId()) != 1) {
                        throw new CommonException("删除角色权限失败");
                    }
                }
            }
        }
        return 1;
    }

    @Override
    public int deleteRole(int id) {
        List<String> list = tbRoleMapper.getUsedRoles(id);
        if (list == null) {
            throw new CommonException("查询用户角色失败");
        }
        if (list.size() > 0) {
            return 0;
        }
        if (tbRoleMapper.deleteByPrimaryKey(id) != 1) {
            throw new CommonException("删除角色失败");
        }
        TbRolePermExample example = new TbRolePermExample();
        TbRolePermExample.Criteria criteria = example.createCriteria();
        criteria.andRoleIdEqualTo(id);
        List<TbRolePerm> list1 = tbRolePermMapper.selectByExample(example);
        if (list1 == null) {
            throw new CommonException("查询角色权限失败");
        }
        for (TbRolePerm tbRolePerm : list1) {
            if (tbRolePermMapper.deleteByPrimaryKey(tbRolePerm.getId()) != 1) {
                throw new CommonException("删除角色权限失败");
            }
        }
        return 1;
    }

    @Override
    public Long countRole() {
        TbRoleExample example = new TbRoleExample();
        return tbRoleMapper.countByExample(example);
    }

    @Override
    public ServiceResult getPermissionList() {
        ServiceResult result = new ServiceResult();
        TbPermissionExample example = new TbPermissionExample();
        List<TbPermission> list = tbPermissionMapper.selectByExample(example);
        if (list == null) {
            throw new CommonException("获取权限列表失败");
        }
        result.setSuccess(true);
        result.setData(list);
        return result;
    }

    @Override
    public int addPermission(Permission permission) {
        TbPermission tbPermission = new TbPermission();
        BeanUtils.copyProperties(permission, tbPermission);
        if (tbPermissionMapper.insert(tbPermission) != 1) {
            throw new CommonException("添加权限失败");
        }
        return 1;
    }

    @Override
    public int updatePermission(Permission permission) {
        TbPermission tbPermission = new TbPermission();
        BeanUtils.copyProperties(permission, tbPermission);
        if (tbPermissionMapper.updateByPrimaryKey(tbPermission) != 1) {
            throw new CommonException("更新权限失败");
        }
        return 1;
    }

    @Override
    public int deletePermission(int id) {
        if (tbPermissionMapper.deleteByPrimaryKey(id) != 1) {
            throw new CommonException("删除权限失败");
        }
        TbRolePermExample example = new TbRolePermExample();
        TbRolePermExample.Criteria criteria = example.createCriteria();
        criteria.andPermissionIdEqualTo(id);
        tbRolePermMapper.deleteByExample(example);
        return 1;
    }

    @Override
    public Long countPermission() {
        TbPermissionExample example = new TbPermissionExample();
        return tbPermissionMapper.countByExample(example);
    }

    @Override
    public ServiceResult getUserList() {
        ServiceResult result = new ServiceResult();
        TbManagerExample example = new TbManagerExample();
        List<TbManager> list = tbManagerMapper.selectByExample(example);
        if (list == null) {
            throw new CommonException("获取用户列表失败");
        }
        for (TbManager tbManager : list) {
            String names = "";
            Iterator it = getRoles(tbManager.getUsername()).iterator();
            while (it.hasNext()) {
                names += it.next() + " ";
            }
            tbManager.setPassword("");
            tbManager.setRoleNames(names);
        }
        result.setData(list);
        return result;
    }


    @Override
    public boolean getUserByName(String username) {
        TbManagerExample example = new TbManagerExample();
        TbManagerExample.Criteria criteria = example.createCriteria();
        criteria.andUsernameEqualTo(username);
        List<TbManager> list = tbManagerMapper.selectByExample(example);
        return list.size() == 0;
    }

    @Override
    public boolean getUserByPhone(String phone) {
        TbManagerExample example = new TbManagerExample();
        TbManagerExample.Criteria criteria = example.createCriteria();
        criteria.andPhoneEqualTo(phone);
        List<TbManager> list = tbManagerMapper.selectByExample(example);
        return list.size() == 0;
    }

    @Override
    public boolean getUserByEmail(String emaill) {
        TbManagerExample example = new TbManagerExample();
        TbManagerExample.Criteria criteria = example.createCriteria();
        criteria.andEmailEqualTo(emaill);
        List<TbManager> list = tbManagerMapper.selectByExample(example);
        return list.size() == 0;
    }

    @Override
    public int addUser(Manager manager) {
        if (!getUserByName(manager.getUsername())) {
            throw new CommonException("用户名已存在");
        }
        if (!getUserByPhone(manager.getPhone())) {
            throw new CommonException("手机号已存在");
        }
        if (!getUserByEmail(manager.getEmail())) {
            throw new CommonException("邮箱已存在");
        }
        String md5Pass = DigestUtils.md5DigestAsHex(manager.getPassword().getBytes());
        manager.setPassword(md5Pass);
        manager.setState(1);
        manager.setCreated(new Date());
        manager.setUpdated(new Date());
        TbManager tbManager = new TbManager();
        BeanUtils.copyProperties(manager, tbManager);
        if (tbManagerMapper.insert(tbManager) != 1) {
            throw new CommonException("添加用户失败");
        }
        return 1;
    }

    @Override
    public Manager getUserById(Long id) {
        TbManager tbManager = tbManagerMapper.selectByPrimaryKey(id);
        if (tbManager == null) {
            throw new CommonException("通过ID获取用户失败");
        }
        tbManager.setPassword("");
        Manager manager = new Manager();
        BeanUtils.copyProperties(tbManager, manager);
        return manager;
    }

    @Override
    public int updateUser(Manager manager) {
        TbManager old = tbManagerMapper.selectByPrimaryKey(manager.getId());
        manager.setPassword(old.getPassword());
        manager.setState(old.getState());
        manager.setCreated(old.getCreated());
        manager.setUpdated(new Date());
        TbManager tbManager = new TbManager();
        BeanUtils.copyProperties(manager, tbManager);
        if (tbManagerMapper.updateByPrimaryKey(tbManager) != 1) {
            throw new CommonException("更新用户失败");
        }
        return 1;
    }

    @Override
    public int changeUserState(Long id, int state) {
        TbManager tbManager = tbManagerMapper.selectByPrimaryKey(id);
        tbManager.setState(state);
        tbManager.setUpdated(new Date());
        if (tbManagerMapper.updateByPrimaryKey(tbManager) != 1) {
            throw new CommonException("更新用户状态失败");
        }
        return 1;
    }

    @Override
    public int changePassword(Manager manager) {
        TbManager old = tbManagerMapper.selectByPrimaryKey(manager.getId());
        old.setUpdated(new Date());
        String md5Pass = DigestUtils.md5DigestAsHex(manager.getPassword().getBytes());
        old.setPassword(md5Pass);
        if (tbManagerMapper.updateByPrimaryKey(old) != 1) {
            throw new CommonException("修改用户密码失败");
        }
        return 1;
    }

    @Override
    public boolean getUserByEditName(Long id, String username) {
        TbManager tbManager = tbManagerMapper.selectByPrimaryKey(id);
        boolean result = true;
        if (tbManager.getUsername() == null || !tbManager.getUsername().equals(username)) {
            result = getUserByName(username);
        }
        return result;
    }

    @Override
    public boolean getUserByEditPhone(Long id, String phone) {
        TbManager tbManager = tbManagerMapper.selectByPrimaryKey(id);
        boolean result = true;
        if (tbManager.getPhone() == null || !tbManager.getPhone().equals(phone)) {
            result = getUserByPhone(phone);
        }
        return result;
    }

    @Override
    public boolean getUserByEditEmail(Long id, String email) {
        TbManager tbManager = tbManagerMapper.selectByPrimaryKey(id);
        boolean result = true;
        if (tbManager.getEmail() == null || !tbManager.getEmail().equals(email)) {
            result = getUserByEmail(email);
        }
        return result;
    }

    @Override
    public int deleteUser(Long userId) {
        if (tbManagerMapper.deleteByPrimaryKey(userId) != 1) {
            throw new CommonException("删除用户失败");
        }
        return 1;
    }

    @Override
    public Long countUser() {
        TbManagerExample example = new TbManagerExample();
        Long result = tbManagerMapper.countByExample(example);
        if (result == null) {
            throw new CommonException("统计用户数失败");
        }
        return result;
    }
}
