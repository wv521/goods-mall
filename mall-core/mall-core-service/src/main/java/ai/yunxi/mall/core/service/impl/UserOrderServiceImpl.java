package ai.yunxi.mall.core.service.impl;

import ai.yunxi.mall.common.exception.CommonException;
import ai.yunxi.mall.common.redis.JedisClient;
import ai.yunxi.mall.common.redis.RedisLock;
import ai.yunxi.mall.core.domain.entity.*;
import ai.yunxi.mall.core.domain.mapper.TbOrderItemMapper;
import ai.yunxi.mall.core.domain.mapper.TbOrderMapper;
import ai.yunxi.mall.core.domain.mapper.TbOrderShippingMapper;
import ai.yunxi.mall.core.service.DelayQueueService;
import ai.yunxi.mall.core.service.OrderMessageService;
import ai.yunxi.mall.core.service.UserOrderService;
import ai.yunxi.mall.core.service.dto.Order;
import ai.yunxi.mall.core.service.dto.OrderItem;
import ai.yunxi.mall.core.service.dto.OrderShipping;
import ai.yunxi.mall.core.service.dto.PageOrder;
import ai.yunxi.mall.product.service.ProductStockService;
import ai.yunxi.mall.user.service.MemberService;
import ai.yunxi.mall.user.service.dto.AddressInfo;
import ai.yunxi.mall.user.service.dto.Member;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.shardingsphere.core.strategy.keygen.SnowflakeShardingKeyGenerator;
import org.apache.shardingsphere.spi.keygen.ShardingKeyGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@DubboService
public class UserOrderServiceImpl implements UserOrderService {

    private final static Logger log = LoggerFactory.getLogger(UserOrderServiceImpl.class);

    @DubboReference
    private MemberService memberService;

    @DubboReference
    private ProductStockService productStockService;

    @Resource
    private OrderMessageService orderMessageService;

    @Resource
    private DelayQueueService delayQueueService;

    @Resource
    private TbOrderMapper tbOrderMapper;
    @Resource
    private TbOrderItemMapper tbOrderItemMapper;
    @Resource
    private TbOrderShippingMapper tbOrderShippingMapper;

    @Resource
    private JedisClient jedisClient;

    @Resource
    private RedisLock redisLock;

    @Value("${ORDER_INFO}")
    private String ORDER_INFO;

    @Value("${ORDER_EXPIRE}")
    private int ORDER_EXPIRE;

    @Override
    public PageOrder getOrderList(Long userId, int page, int size) {
        //分页
        if (page <= 0) {
            page = 1;
        }

        PageHelper.startPage(page, size);
        PageOrder pageOrder = new PageOrder();
        List<Order> list = new ArrayList<>();
        TbOrderExample example = new TbOrderExample();
        TbOrderExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(userId);
        example.setOrderByClause("create_time DESC");
        List<TbOrder> listOrder = tbOrderMapper.selectByExample(example);
        for (TbOrder tbOrder : listOrder) {
            Order order = new Order();
            BeanUtils.copyProperties(tbOrder, order);
            //address
            TbOrderShipping tbOrderShipping = tbOrderShippingMapper.selectByPrimaryKey(tbOrder.getOrderId());
            AddressInfo address = new AddressInfo();
            address.setUserName(tbOrderShipping.getReceiverName());
            address.setStreetName(tbOrderShipping.getReceiverAddress());
            address.setTel(tbOrderShipping.getReceiverPhone());
            order.setAddressInfo(address);
            //goodsList
            TbOrderItemExample exampleItem = new TbOrderItemExample();
            TbOrderItemExample.Criteria criteriaItem = exampleItem.createCriteria();
            criteriaItem.andOrderIdEqualTo(tbOrder.getOrderId());
            List<TbOrderItem> items = tbOrderItemMapper.selectByExample(exampleItem);
            List<OrderItem> listProduct = items.stream().map(tbOrderItem -> {
                OrderItem orderItem = new OrderItem();
                BeanUtils.copyProperties(tbOrderItem, orderItem);
                return orderItem;
            }).collect(Collectors.toList());
            order.setItems(listProduct);
            list.add(order);
        }
        PageInfo<Order> pageInfo = new PageInfo<>(list);
        pageOrder.setTotal(getOrderCount(userId));
        pageOrder.setData(list);
        return pageOrder;
    }

    @Override
    public Order getFromCache(Long orderId) {
        Order order = null;
        try {
            if (redisLock.tryLock(ORDER_INFO + "READLOCK:" + orderId, String.valueOf(orderId))) {
                // 读取缓存
                String json = jedisClient.get(ORDER_INFO + orderId);
                if (json != null) {
                    try {
                        ObjectMapper om = new ObjectMapper();
                        order = om.readValue(json, Order.class);
                    } catch (JsonProcessingException e) {
                        throw new CommonException("通过id获取订单缓存失败");
                    }
                }
            }
        } finally {
            redisLock.unlock(ORDER_INFO + "READLOCK:" + +orderId, String.valueOf(orderId));
        }
        return order;
    }

    @Override
    public Order getOrder(Long orderId) {
        Order order = new Order();
        // 先读取缓存
        String json = jedisClient.get(ORDER_INFO + orderId);
        if (json != null) {
            try {
                ObjectMapper om = new ObjectMapper();
                order = om.readValue(json, Order.class);
                return order;
            } catch (JsonProcessingException e) {
                throw new CommonException("通过id获取订单缓存失败");
            }
        }

        TbOrder tbOrder = tbOrderMapper.selectByPrimaryKey(orderId);
        if (tbOrder == null) {
            throw new CommonException("通过id获取订单失败");
        }

        BeanUtils.copyProperties(tbOrder, order);
        //address
        TbOrderShipping tbOrderShipping = tbOrderShippingMapper.selectByPrimaryKey(tbOrder.getOrderId());
        AddressInfo address = new AddressInfo();
        address.setUserName(tbOrderShipping.getReceiverName());
        address.setStreetName(tbOrderShipping.getReceiverAddress());
        address.setTel(tbOrderShipping.getReceiverPhone());
        order.setAddressInfo(address);
        //goodsList
        TbOrderItemExample exampleItem = new TbOrderItemExample();
        TbOrderItemExample.Criteria criteriaItem = exampleItem.createCriteria();
        criteriaItem.andOrderIdEqualTo(tbOrder.getOrderId());
        List<TbOrderItem> items = tbOrderItemMapper.selectByExample(exampleItem);
        List<OrderItem> listProduct = items.stream().map(tbOrderItem -> {
            OrderItem orderItem = new OrderItem();
            BeanUtils.copyProperties(tbOrderItem, orderItem);
            return orderItem;
        }).collect(Collectors.toList());
        order.setItems(listProduct);
        return order;
    }

    @Override
    public Long createOrder(Order order) {
        Member member = memberService.getById(Long.valueOf(order.getUserId()));
        if (member == null) {
            throw new CommonException("获取下单用户失败");
        }

        // 生成全局唯一订单编号
        ShardingKeyGenerator keyGenerator = new SnowflakeShardingKeyGenerator();
        Long orderId = (Long) keyGenerator.generateKey();
        order.setOrderId(orderId);

        // 扣减库存
        List<String> productIds = order.getItems().stream().map(orderItem -> {
            return String.valueOf(orderItem.getItemId());
        }).collect(Collectors.toList());
        List<String> productNums = order.getItems().stream().map(orderItem -> {
            return String.valueOf(orderItem.getNum());
        }).collect(Collectors.toList());
        int result = productStockService.decrease(orderId, productIds, productNums);
        if (result < 0) {
            log.error("扣减商品库存失败");
            throw new CommonException("扣减库存失败");
        }

        // 设置订单状态
        // 0、未付款，1、已付款，2、未发货，
        // 3、已发货，4、交易成功，5、交易关闭，6、交易失败
        order.setStatus(0);
        order.getItems().stream().forEach(orderItem -> {
            orderItem.setId((Long) keyGenerator.generateKey());
            orderItem.setOrderId(orderId);
            orderItem.setTotalFee(orderItem.getPrice().multiply(BigDecimal.valueOf(orderItem.getNum())));
        });

        // 设置物流信息
        Date date = new Date();
        order.getOrderShipping().setOrderId(orderId);
        order.getOrderShipping().setCreated(date);
        order.getOrderShipping().setUpdated(date);

        // 保存订单到缓存
        try {
            ObjectMapper om = new ObjectMapper();
            jedisClient.set(ORDER_INFO + orderId, om.writeValueAsString(order));
            log.info("保存了订单：" + orderId + " 到缓存");
            //重置商品缓存时间
            jedisClient.expire(ORDER_INFO + orderId, ORDER_EXPIRE);
        } catch (JsonProcessingException e) {
            log.error("保存订单：" + orderId + " 到缓存失败");
            throw new CommonException("保存订单失败");
        }

        // 发延时队列
        delayQueueService.send(order, ORDER_EXPIRE * 1000);
        return orderId;
    }

    @Transactional
    public int saveOrder(Order order) {
        // 保存订单信息
        TbOrder tbOrder = new TbOrder();
        BeanUtils.copyProperties(order, tbOrder);
        if (tbOrderMapper.insert(tbOrder) != 1) {
            throw new CommonException("保存订单失败");
        }

        // 保存订单商品信息
        order.getItems().stream().forEach(orderItem -> {
            TbOrderItem tbOrderItem = new TbOrderItem();
            BeanUtils.copyProperties(orderItem, tbOrderItem);
            if (tbOrderItemMapper.insert(tbOrderItem) != 1) {
                throw new CommonException("保存订单商品失败");
            }
        });

        // 保存配送信息
        OrderShipping orderShipping = order.getOrderShipping();
        TbOrderShipping tbOrderShipping = new TbOrderShipping();
        BeanUtils.copyProperties(orderShipping, tbOrderShipping);
        if (tbOrderShippingMapper.insert(tbOrderShipping) != 1) {
            throw new CommonException("保存配送信息失败");
        }
        return 1;
    }

    @Override
    public int changeStatus(Long orderId, Integer status) {
        return 0;
    }

    @Override
    public int cancelOrder(Long orderId) {
        try {
            if (redisLock.tryLock(ORDER_INFO + "READLOCK:" + orderId, String.valueOf(orderId))) {
                // 读取缓存
                String json = jedisClient.get(ORDER_INFO + orderId);
                if (json != null) {
                    try {
                        ObjectMapper om = new ObjectMapper();
                        Order order = om.readValue(json, Order.class);
                        saveOrder(order);
                        jedisClient.del(ORDER_INFO + orderId);
                        return 1;
                    } catch (JsonProcessingException e) {
                        throw new CommonException("通过id获取订单缓存失败");
                    }
                }
            }
        } finally {
            redisLock.unlock(ORDER_INFO + "READLOCK:" + +orderId, String.valueOf(orderId));
        }
        return -1;
    }

    @Override
    public int deleteOrder(Long orderId) {
        return tbOrderMapper.deleteByPrimaryKey(String.valueOf(orderId));
    }

    public int getOrderCount(Long userId) {
        TbOrderExample example = new TbOrderExample();
        TbOrderExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(userId);
        List<TbOrder> listOrder = tbOrderMapper.selectByExample(example);
        if (listOrder != null) {
            return listOrder.size();
        }
        return 0;
    }
}
