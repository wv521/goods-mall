package ai.yunxi.mall.core.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class Role implements Serializable {
    private Integer id;
    private String name;
    private String permissions;
    private String description;
    private Integer[] roles;
}
