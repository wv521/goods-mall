package ai.yunxi.mall.core.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
public class Express implements Serializable {
    private Integer id;
    private String expressName;
    private Integer sortOrder;
    private Date created;
    private Date updated;
}
