package ai.yunxi.mall.core.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
public class ChartData implements Serializable {
    private List<Object> xDatas;
    private List<Object> yDatas;
    private Object countAll;
}
