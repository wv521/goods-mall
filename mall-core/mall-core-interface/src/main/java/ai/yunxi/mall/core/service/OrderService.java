package ai.yunxi.mall.core.service;

import ai.yunxi.mall.common.ServiceResult;
import ai.yunxi.mall.core.service.dto.Order;

import java.math.BigDecimal;

public interface OrderService {

    /**
     * 获得订单列表
     *
     * @param draw
     * @param start
     * @param length
     * @param search
     * @param orderCol
     * @param orderDir
     * @return
     */
    ServiceResult getOrderList(int draw, int start, int length, String search, String orderCol, String orderDir);

    /**
     * 获得单个订单
     *
     * @param orderId
     * @return
     */
    Order getOrder(Long orderId);

    /**
     * 统计订单数
     *
     * @return
     */
    Long countOrder();

    /**
     * 发货
     *
     * @param orderId
     * @param shippingName
     * @param shippingCode
     * @param postFee
     * @return
     */
    int deliver(Long orderId, String shippingName, String shippingCode, BigDecimal postFee);

    /**
     * 备注
     *
     * @param orderId
     * @param message
     * @return
     */
    int remark(Long orderId, String message);

    /**
     * 取消订单
     *
     * @param orderId
     * @return
     */
    int cancelByAdmin(Long orderId);
}
