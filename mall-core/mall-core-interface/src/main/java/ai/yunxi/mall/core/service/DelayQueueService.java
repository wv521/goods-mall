package ai.yunxi.mall.core.service;

import ai.yunxi.mall.core.service.dto.Order;

public interface DelayQueueService {

    public void send(Order order, long delay);

    public void receive(String message);
}
