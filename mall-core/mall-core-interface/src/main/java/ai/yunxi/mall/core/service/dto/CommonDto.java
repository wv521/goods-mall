package ai.yunxi.mall.core.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class CommonDto implements Serializable {
    private Long userId;
    private String imgData;
    private String token;
}
