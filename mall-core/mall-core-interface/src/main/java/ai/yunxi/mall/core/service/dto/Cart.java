package ai.yunxi.mall.core.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class Cart implements Serializable {
    private Long userId;
    private Long productId;
    private String checked;
    private int productNum;
}
