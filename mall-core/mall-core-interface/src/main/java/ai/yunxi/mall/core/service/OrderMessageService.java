package ai.yunxi.mall.core.service;

import ai.yunxi.mall.core.service.dto.Order;

public interface OrderMessageService {
    public boolean send(Order order);

    public void receive(String message);
}
