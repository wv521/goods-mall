package ai.yunxi.mall.core.service.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Permission {
    private Integer id;
    private Integer roleId;
    private String name;
    private String permission;
}
