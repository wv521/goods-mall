package ai.yunxi.mall.core.service;

import ai.yunxi.mall.common.ServiceResult;
import ai.yunxi.mall.core.domain.entity.TbBase;
import ai.yunxi.mall.core.domain.entity.TbLog;
import ai.yunxi.mall.core.domain.entity.TbOrderItem;
import ai.yunxi.mall.core.domain.entity.TbShiroFilter;
import ai.yunxi.mall.core.service.dto.Base;
import ai.yunxi.mall.core.service.dto.OrderItem;
import ai.yunxi.mall.core.service.dto.ShiroFilter;

import java.util.List;

public interface SystemService {

    /**
     * 获得shiro过滤链配置
     *
     * @return
     */
    List<ShiroFilter> getShiroFilter();

    /**
     * 统计过滤链数目
     *
     * @return
     */
    Long countShiroFilter();

    /**
     * 添加shiro过滤链
     *
     * @param shiroFilter
     * @return
     */
    int addShiroFilter(ShiroFilter shiroFilter);

    /**
     * 更新shiro过滤链
     *
     * @param shiroFilter
     * @return
     */
    int updateShiroFilter(ShiroFilter shiroFilter);

    /**
     * 删除shiro过滤链
     *
     * @param id
     * @return
     */
    int deleteShiroFilter(int id);

    /**
     * 获取网站基础设置
     *
     * @return
     */
    Base getBase();

    /**
     * 更新网站基础设置
     *
     * @param base
     * @return
     */
    int updateBase(Base base);

    /**
     * 获取本周热销商品
     *
     * @return
     */
    OrderItem getWeekHot();
}
