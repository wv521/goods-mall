package ai.yunxi.mall.core.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class Dict implements Serializable {
    private Integer id;
    private String dict;
    private Integer type;
}
