package ai.yunxi.mall.core.service;

import ai.yunxi.mall.core.domain.entity.TbExpress;
import ai.yunxi.mall.core.service.dto.Express;

import java.util.List;

public interface ExpressService {

    /**
     * 获取快递列表
     *
     * @return
     */
    List<Express> getExpressList();

    /**
     * 添加快递
     *
     * @param express
     * @return
     */
    int addExpress(Express express);

    /**
     * 更新快递
     *
     * @param express
     * @return
     */
    int updateExpress(Express express);

    /**
     * 删除快递
     *
     * @param id
     * @return
     */
    int deleteExpress(int id);
}
