package ai.yunxi.mall.core.service.dto;

import ai.yunxi.mall.core.domain.entity.TbOrderItem;
import ai.yunxi.mall.product.service.dto.Product;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
public class CartItem implements Serializable {
    private Long itemId;
    private BigDecimal salePrice;
    private Integer purchaseNum;
    private String checked;
    private String itemName;
    private String itemImg;

    public static CartItem transform(Product product) {
        CartItem cartItem = new CartItem();
        cartItem.setItemId(product.getProductId());
        cartItem.setItemName(product.getTitle());
        cartItem.setSalePrice(product.getPrice());
        cartItem.setItemImg(product.getImage());
        return cartItem;
    }

    public static CartItem transform(TbOrderItem tbOrderItem) {
        CartItem cartItem = new CartItem();
        cartItem.setItemId(Long.valueOf(tbOrderItem.getItemId()));
        cartItem.setItemName(tbOrderItem.getTitle());
        cartItem.setSalePrice(tbOrderItem.getPrice());
        cartItem.setItemImg(tbOrderItem.getPicPath());
        return cartItem;
    }
}
