package ai.yunxi.mall.core.service;

import ai.yunxi.mall.core.domain.entity.TbOrder;
import ai.yunxi.mall.core.service.dto.Order;
import ai.yunxi.mall.core.service.dto.PageOrder;

public interface UserOrderService {

    /**
     * 分页获得用户订单
     *
     * @param userId
     * @param page
     * @param size
     * @return
     */
    PageOrder getOrderList(Long userId, int page, int size);

    /**
     * 获得单个订单
     *
     * @param orderId
     * @return
     */
    Order getOrder(Long orderId);

    /**
     * 从缓存中获取订单
     *
     * @param orderId
     * @return
     */
    public Order getFromCache(Long orderId);

    /**
     * 创建订单
     *
     * @param order
     * @return
     */
    Comparable<?> createOrder(Order order);

    /**
     * 保存订单到数据库
     *
     * @param order
     * @return
     */
    public int saveOrder(Order order);

    /**
     * 更改订单状态
     *
     * @param orderId
     * @param status
     * @return
     */
    public int changeStatus(Long orderId, Integer status);

    /**
     * 取消订单
     *
     * @param orderId
     * @return
     */
    int cancelOrder(Long orderId);

    /**
     * 删除订单
     *
     * @param orderId
     * @return
     */
    int deleteOrder(Long orderId);
}
