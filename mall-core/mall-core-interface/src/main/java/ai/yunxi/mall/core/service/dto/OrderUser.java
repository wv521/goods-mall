package ai.yunxi.mall.core.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Setter
@Getter
public class OrderUser implements Serializable {
    private String userId;
    private Long addressId;
    private String tel;
    private String userName;
    private String streetName;
    private BigDecimal orderTotal;
    private List<CartItem> items;
}
