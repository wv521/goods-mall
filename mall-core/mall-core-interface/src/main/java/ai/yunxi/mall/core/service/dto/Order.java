package ai.yunxi.mall.core.service.dto;

import ai.yunxi.mall.user.service.dto.AddressInfo;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Setter
@Getter
public class Order implements Serializable {
    private Long orderId;
    private BigDecimal payment;
    private Integer paymentType;
    private BigDecimal postFee;
    private Integer status;
    private Date createTime;
    private Date updateTime;
    private Date paymentTime;
    private Date consignTime;
    private Date endTime;
    private Date closeTime;
    private String shippingName;
    private String shippingCode;
    private Long userId;
    private String buyerMessage;
    private String buyerNick;
    private Boolean buyerComment;

    private AddressInfo addressInfo;
    private List<OrderItem> items;
    private OrderShipping orderShipping;
}
