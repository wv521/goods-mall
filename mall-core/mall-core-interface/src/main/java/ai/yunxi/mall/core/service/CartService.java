package ai.yunxi.mall.core.service;

import ai.yunxi.mall.core.service.dto.CartItem;

import java.util.List;

public interface CartService {

    /**
     * 添加
     *
     * @param userId
     * @param productId
     * @param num
     * @return
     */
    int addItem(long userId, long productId, int num);

    /**
     * 获取
     *
     * @param userId
     * @return
     */
    List<CartItem> getItems(long userId);

    /**
     * 更新
     *
     * @param userId
     * @param productId
     * @param num
     * @param checked
     * @return
     */
    int setItemNum(long userId, long productId, int num, String checked);

    /**
     * 删除单个
     *
     * @param userId
     * @param productId
     * @return
     */
    int deleteItem(long userId, long productId);

    /**
     * 全选反选
     *
     * @param userId
     * @param checked
     * @return
     */
    int checkAll(long userId, String checked);

    /**
     * 删除全部勾选的
     *
     * @param userId
     * @return
     */
    int delChecked(long userId);
}
