package ai.yunxi.mall.user.domain.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
public class TbMember implements Serializable {
    private Long id;
    private String username;
    private String password;
    private String phone;
    private String email;
    private Date created;
    private Date updated;
    private String sex;
    private String address;
    private Integer state;
    private String file;
    private String description;
    private Integer points;
    private Long balance;
}