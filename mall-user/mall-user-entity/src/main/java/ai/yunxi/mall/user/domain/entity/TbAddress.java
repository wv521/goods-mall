package ai.yunxi.mall.user.domain.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class TbAddress implements Serializable {
    private Long addressId;
    private Long userId;
    private String userName;
    private String tel;
    private String streetName;
    private Boolean isDefault;
}