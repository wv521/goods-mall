package ai.yunxi.mall.user.service;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "ai.yunxi")
@MapperScan("ai.yunxi.mall.user.domain.mapper")
@EnableDubbo(scanBasePackages = {"ai.yunxi.mall.user.service.impl"})
public class MallUserServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MallUserServiceApplication.class, args);
    }

}
