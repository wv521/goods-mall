package ai.yunxi.mall.user.service.impl;

import ai.yunxi.mall.common.ServiceResult;
import ai.yunxi.mall.common.exception.CommonException;
import ai.yunxi.mall.common.redis.JedisClient;
import ai.yunxi.mall.user.domain.entity.TbMember;
import ai.yunxi.mall.user.domain.entity.TbMemberExample;
import ai.yunxi.mall.user.domain.mapper.TbMemberMapper;
import ai.yunxi.mall.user.service.LoginService;
import ai.yunxi.mall.user.service.MemberService;
import ai.yunxi.mall.user.service.dto.Member;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.dubbo.config.annotation.DubboService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@DubboService
public class MemberServiceImpl implements MemberService {

    final static Logger log = LoggerFactory.getLogger(MemberServiceImpl.class);

    @Resource
    private LoginService loginService;
    @Resource
    private TbMemberMapper tbMemberMapper;

    @Resource
    private JedisClient jedisClient;

    @Override
    public Member getById(long memberId) {
        Member member = new Member();
        try {
            TbMember tbMember = tbMemberMapper.selectByPrimaryKey(memberId);
            return Member.transform(tbMember);
        } catch (Exception e) {
            throw new CommonException("ID获取会员信息失败");
        }
    }

    @Override
    public ServiceResult getList(int draw, int start, int length, String search,
                                 String minDate, String maxDate, String orderCol,
                                 String orderDir) {
        ServiceResult result = new ServiceResult();

        try {
            //分页
            PageHelper.startPage(start / length + 1, length);
            List<Member> list = tbMemberMapper.selectByMemberInfo("%" + search + "%",
                    minDate, maxDate, orderCol, orderDir).stream().map(tbMember -> {
                return Member.transform(tbMember);
            }).collect(Collectors.toList());

            PageInfo<Member> pageInfo = new PageInfo<>();
            result.setRecordsFiltered((int) pageInfo.getTotal());
            result.setRecordsTotal(getMemberCount().getRecordsTotal());
            result.setDraw(draw);
            result.setData(list);
        } catch (Exception e) {
            throw new CommonException("加载用户列表失败");
        }

        return result;
    }

    @Override
    public ServiceResult getRemoveList(int draw, int start, int length, String search,
                                       String minDate, String maxDate, String orderCol,
                                       String orderDir) {
        ServiceResult result = new ServiceResult();

        try {
            //分页执行查询返回结果
            PageHelper.startPage(start / length + 1, length);
            List<Member> list = tbMemberMapper.selectByRemoveMemberInfo("%" + search + "%",
                    minDate, maxDate, orderCol, orderDir).stream().map(tbMember -> {
                return Member.transform(tbMember);
            }).collect(Collectors.toList());

            PageInfo<Member> pageInfo = new PageInfo<>(list);
            result.setRecordsFiltered((int) pageInfo.getTotal());
            result.setRecordsTotal(getRemoveCount().getRecordsTotal());
            result.setDraw(draw);
            result.setData(list);
        } catch (Exception e) {
            throw new CommonException("加载删除用户列表失败");
        }

        return result;
    }

    @Override
    public Member getByUsername(String username) {
        TbMemberExample example = new TbMemberExample();
        TbMemberExample.Criteria criteria = example.createCriteria();
        criteria.andUsernameEqualTo(username);

        try {
            List<TbMember> list = tbMemberMapper.selectByExample(example);
            if (null != list && !list.isEmpty()) {
                TbMember tbMember = list.get(0);
                return Member.transform(tbMember);
            }
        } catch (Exception e) {
            throw new CommonException("ID获取会员信息失败");
        }

        return null;
    }

    @Override
    public Member getByPhone(String phone) {
        TbMemberExample example = new TbMemberExample();
        TbMemberExample.Criteria criteria = example.createCriteria();
        criteria.andPhoneEqualTo(phone);
        try {
            List<TbMember> list = tbMemberMapper.selectByExample(example);
            if (null != list && !list.isEmpty()) {
                TbMember tbMember = list.get(0);
                return Member.transform(tbMember);
            }
        } catch (Exception e) {
            throw new CommonException("Phone获取会员信息失败");
        }

        return null;
    }

    @Override
    public Member getByEmail(String email) {
        TbMemberExample example = new TbMemberExample();
        TbMemberExample.Criteria criteria = example.createCriteria();
        criteria.andEmailEqualTo(email);
        try {
            List<TbMember> list = tbMemberMapper.selectByExample(example);
            if (null != list && !list.isEmpty()) {
                TbMember tbMember = list.get(0);
                return Member.transform(tbMember);
            }
        } catch (Exception e) {
            throw new CommonException("Email获取会员信息失败");
        }

        return null;
    }

    @Override
    public ServiceResult getMemberCount() {
        ServiceResult result = new ServiceResult();
        TbMemberExample example = new TbMemberExample();
        TbMemberExample.Criteria criteria = example.createCriteria();
        criteria.andStateNotEqualTo(2);
        try {
            result.setRecordsTotal((int) tbMemberMapper.countByExample(example));
        } catch (Exception e) {
            throw new CommonException("统计会员数失败");
        }

        return result;
    }

    @Override
    public ServiceResult getRemoveCount() {
        ServiceResult result = new ServiceResult();
        TbMemberExample example = new TbMemberExample();
        TbMemberExample.Criteria criteria = example.createCriteria();
        criteria.andStateEqualTo(2);
        try {
            result.setRecordsTotal((int) tbMemberMapper.countByExample(example));
        } catch (Exception e) {
            throw new CommonException("统计移除会员数失败");
        }

        return result;
    }

    @Override
    public Member addMember(Member member) {
        TbMember tbMember = Member.transform(member);
        if (getByUsername(tbMember.getUsername()) != null) {
            throw new CommonException("用户名已被注册");
        }
        if (getByPhone(tbMember.getPhone()) != null) {
            throw new CommonException("手机号已被注册");
        }
        if (getByEmail(tbMember.getEmail()) != null) {
            throw new CommonException("邮箱已被注册");
        }

        tbMember.setState(1);
        tbMember.setCreated(new Date());
        tbMember.setUpdated(new Date());
        String md5Pass = DigestUtils.md5DigestAsHex(tbMember.getPassword().getBytes());
        tbMember.setPassword(md5Pass);
        if (tbMemberMapper.insert(tbMember) != 1) {
            throw new CommonException("添加用户失败");
        }
        return getByPhone(tbMember.getPhone());
    }

    @Override
    public Member updateMember(Long id, Member member) {
        TbMember tbMember = Member.transform(member);
        tbMember.setId(id);
        tbMember.setUpdated(new Date());
        Member oldMember = getById(id);
        tbMember.setState(oldMember.getState());
        tbMember.setCreated(oldMember.getCreated());
        if (tbMember.getPassword() == null || tbMember.getPassword() == "") {
            tbMember.setPassword(oldMember.getPassword());
        } else {
            String md5Pass = DigestUtils.md5DigestAsHex(tbMember.getPassword().getBytes());
            tbMember.setPassword(md5Pass);
        }

        if (tbMemberMapper.updateByPrimaryKey(tbMember) != 1) {
            throw new CommonException("更新会员信息失败");
        }
        return getById(id);
    }

    @Override
    public Member changePassword(Long id, Member member) {
        TbMember tbMember = tbMemberMapper.selectByPrimaryKey(id);
        String md5Pass = DigestUtils.md5DigestAsHex(member.getPassword().getBytes());
        tbMember.setPassword(md5Pass);
        tbMember.setUpdated(new Date());
        if (tbMemberMapper.updateByPrimaryKey(tbMember) != 1) {
            throw new CommonException("修改会员密码失败");
        }
        return getById(id);
    }

    @Override
    public Member updateState(Long id, Integer state) {
        TbMember tbMember = tbMemberMapper.selectByPrimaryKey(id);
        tbMember.setState(state);
        tbMember.setUpdated(new Date());
        if (tbMemberMapper.updateByPrimaryKey(tbMember) != 1) {
            throw new CommonException("修改会员状态失败");
        }
        return getById(id);
    }

    @Override
    public int deleteMember(Long id) {
        if (tbMemberMapper.deleteByPrimaryKey(id) != 1) {
            throw new CommonException("删除会员失败");
        }
        return 0;
    }

    @Override
    public Member getByEditEmail(Long id, String email) {
        Member member = getById(id);
        Member newMember = null;
        if (member.getEmail() == null || !member.getEmail().equals(email)) {
            newMember = getByEmail(email);
        }
        return newMember;
    }

    @Override
    public Member getByEditPhone(Long id, String phone) {
        Member member = getById(id);
        Member newMember = null;
        if (member.getPhone() == null || !member.getPhone().equals(phone)) {
            newMember = getByPhone(phone);
        }
        return newMember;
    }

    @Override
    public Member getByEditUsername(Long id, String username) {
        Member member = getById(id);
        Member newMember = null;
        if (member.getUsername() == null || !member.getUsername().equals(username)) {
            newMember = getByUsername(username);
        }
        return newMember;
    }

    @Override
    public String imageUpload(Long userId, String token, String imgData) {
        //过滤data:URL
        String base64 = "";
        String imgPath = "";

        Member member = this.getById(userId);
        member.setFile(imgPath);
        if (member == null) {
            throw new CommonException("通过id获取用户失败");
        }
        if (this.updateMember(userId, member) == null) {
            throw new CommonException("更新用户头像失败");
        }

        ObjectMapper om = new ObjectMapper();
        //更新缓存
        member = loginService.getUserByToken(token);
        member.setFile(imgPath);
        try {
            jedisClient.set("SESSION:" + token, om.writeValueAsString(member));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return imgPath;
    }
}
