package ai.yunxi.mall.user.service.impl;

import ai.yunxi.mall.common.exception.CommonException;
import ai.yunxi.mall.user.service.dto.Member;
import ai.yunxi.mall.user.service.MemberService;
import ai.yunxi.mall.user.service.RegisterService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;

@DubboService
public class RegisterServiceImpl implements RegisterService {

    @Resource
    private MemberService memberService;

    @Override
    public boolean checkData(String param, int type) {
        //1：用户名 2：手机号 3：邮箱
        if (type == 1) {
            if (memberService.getByUsername(param) != null) {
                return true;
            }
        } else if (type == 2) {
            if (memberService.getByPhone(param) != null) {
                return true;
            }
        } else if (type == 3) {
            if (memberService.getByEmail(param) != null) {
                return true;
            }
        }

        return false;
    }

    @Override
    public int register(String userName, String userPwd) {
        Member member = new Member();
        member.setUsername(userName);
        // 用户名密码不能为空
        if (userName.isEmpty() || userPwd.isEmpty()) {
            return -1;
        }
        boolean result = checkData(userName, 1);
        // 该用户名已被注册
        if (!result) {
            return 0;
        }

        // MD5加密
        String md5Pass = DigestUtils.md5DigestAsHex(userPwd.getBytes());
        member.setPassword(md5Pass);
        member.setState(1);
        //member.setCreated(new Date());
        //member.setUpdated(new Date());
        if (memberService.addMember(member) == null) {
            throw new CommonException("注册用户失败");
        }
        return 1;
    }
}
