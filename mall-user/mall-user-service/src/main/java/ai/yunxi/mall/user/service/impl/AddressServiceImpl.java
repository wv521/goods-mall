package ai.yunxi.mall.user.service.impl;

import ai.yunxi.mall.common.exception.CommonException;
import ai.yunxi.mall.user.domain.entity.TbAddress;
import ai.yunxi.mall.user.domain.entity.TbAddressExample;
import ai.yunxi.mall.user.domain.mapper.TbAddressMapper;
import ai.yunxi.mall.user.service.AddressService;
import ai.yunxi.mall.user.service.dto.AddressInfo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.BeanUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@DubboService
public class AddressServiceImpl implements AddressService {

    @Resource
    private TbAddressMapper tbAddressMapper;

    @Override
    public List<AddressInfo> getAddressList(Long userId) {
        List<TbAddress> list = new ArrayList<>();
        TbAddressExample example = new TbAddressExample();
        TbAddressExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(userId);
        list = tbAddressMapper.selectByExample(example);
        if (list == null) {
            throw new CommonException("获取默认地址列表失败");
        }

        return list.stream().map(tbAddress -> {
            AddressInfo addressInfo = new AddressInfo();
            BeanUtils.copyProperties(tbAddress, addressInfo);
            return addressInfo;
        }).sorted(Comparator.comparing(AddressInfo::getIsDefault).reversed())
                .collect(Collectors.toList());
    }

    @Override
    public AddressInfo getAddress(Long addressId) {
        TbAddress tbAddress = tbAddressMapper.selectByPrimaryKey(addressId);
        if (tbAddress == null) {
            throw new CommonException("通过id获取地址失败");
        }
        AddressInfo addressInfo = new AddressInfo();
        BeanUtils.copyProperties(tbAddress, addressInfo);
        return addressInfo;
    }

    @Override
    public int addAddress(AddressInfo addressInfo) {
        //设置唯一默认
        setDefault(addressInfo);
        TbAddress tbAddress = new TbAddress();
        BeanUtils.copyProperties(addressInfo, tbAddress);
        if (tbAddressMapper.insert(tbAddress) != 1) {
            throw new CommonException("添加地址失败");
        }
        return 1;
    }

    @Override
    public int updateAddress(AddressInfo addressInfo) {
        //设置唯一默认
        setDefault(addressInfo);
        TbAddress tbAddress = new TbAddress();
        BeanUtils.copyProperties(addressInfo, tbAddress);
        if (tbAddressMapper.updateByPrimaryKey(tbAddress) != 1) {
            throw new CommonException("更新地址失败");
        }
        return 1;
    }

    @Override
    public int deleteAddress(Long id) {
        if (tbAddressMapper.deleteByPrimaryKey(id) != 1) {
            throw new CommonException("删除地址失败");
        }
        return 1;
    }

    public void setDefault(AddressInfo addressInfo) {
        TbAddress tbAddress = new TbAddress();
        BeanUtils.copyProperties(addressInfo, tbAddress);
        //设置唯一默认
        if (tbAddress.getIsDefault()) {
            TbAddressExample example = new TbAddressExample();
            TbAddressExample.Criteria criteria = example.createCriteria();
            criteria.andUserIdEqualTo(tbAddress.getUserId());
            List<TbAddress> list = tbAddressMapper.selectByExample(example);
            for (TbAddress tbAddr : list) {
                tbAddr.setIsDefault(false);
                tbAddressMapper.updateByPrimaryKey(tbAddr);
            }
        }
    }
}
