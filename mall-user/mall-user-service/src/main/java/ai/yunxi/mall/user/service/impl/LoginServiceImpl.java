package ai.yunxi.mall.user.service.impl;

import ai.yunxi.mall.common.redis.JedisClient;
import ai.yunxi.mall.user.service.LoginService;
import ai.yunxi.mall.user.service.MemberService;
import ai.yunxi.mall.user.service.dto.Member;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.UUID;

@DubboService
public class LoginServiceImpl implements LoginService {

    @Resource
    private MemberService memberService;
    @Resource
    private JedisClient jedisClient;

    @Value("${SESSION_EXPIRE}")
    private Integer SESSION_EXPIRE;

    @Override
    public Member userLogin(String username, String password) {
        Member member = memberService.getByUsername(username);
        if (member == null) {
            member.setState(0);
            member.setMessage("用户名或密码错误");
            return member;
        }
        //md5加密
        if (!DigestUtils.md5DigestAsHex(password.getBytes()).equals(member.getPassword())) {
            member.setState(0);
            member.setMessage("用户名或密码错误");
            return member;
        }
        String token = UUID.randomUUID().toString();
        member.setToken(token);
        member.setState(1);

        ObjectMapper om = new ObjectMapper();
        try {
            // 用户信息写入redis：key："SESSION:token" value："user"
            jedisClient.set("SESSION:" + token, om.writeValueAsString(member));
            jedisClient.expire("SESSION:" + token, SESSION_EXPIRE);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return member;
    }

    @Override
    public Member getUserByToken(String token) {
        String json = jedisClient.get("SESSION:" + token);
        if (json == null) {
            Member member = new Member();
            member.setState(0);
            member.setMessage("用户登录已过期");
            return member;
        }

        ObjectMapper om = new ObjectMapper();
        //重置过期时间
        jedisClient.expire("SESSION:" + token, SESSION_EXPIRE);
        Member member = null;
        try {
            member = om.readValue(json, Member.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return member;
    }

    @Override
    public int logout(String token) {
        jedisClient.del("SESSION:" + token);
        return 1;
    }
}
