package ai.yunxi.mall.user.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class AddressInfo implements Serializable {
    private Long addressId;
    private Long userId;
    private String userName;
    private String tel;
    private String streetName;
    private Boolean isDefault;
}
