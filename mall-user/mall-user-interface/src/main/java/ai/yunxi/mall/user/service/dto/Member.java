package ai.yunxi.mall.user.service.dto;

import ai.yunxi.mall.user.domain.entity.TbMember;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
public class Member implements Serializable {
    private Long id;
    private String username;
    private String password;
    private String phone;
    private String email;
    private String sex;
    private String province;
    private String city;
    private String district;
    private String address;
    private String file;
    private String description;
    private Integer points;
    private Long balance;
    private int state;
    private String token;
    private String message;
    private Date created;
    private Date updated;

    public static Member transform(TbMember tbMemer) {
        Member member = new Member();
        member.setId(tbMemer.getId());
        member.setUsername(tbMemer.getUsername());
        member.setEmail(tbMemer.getEmail());
        member.setPhone(tbMemer.getPhone());
        member.setAddress(tbMemer.getAddress());
        member.setBalance(tbMemer.getBalance());
        member.setFile(tbMemer.getFile());
        member.setPoints(tbMemer.getPoints());
        member.setSex(tbMemer.getSex());
        member.setDescription(tbMemer.getDescription());
        return member;
    }

    public static TbMember transform(Member member) {
        TbMember tbMember = new TbMember();
        if (!member.getUsername().isEmpty()) {
            tbMember.setUsername(member.getUsername());
        }
        if (!member.getPassword().isEmpty()) {
            tbMember.setPassword(member.getPassword());
        }
        if (!member.getPhone().isEmpty()) {
            tbMember.setPhone(member.getPhone());
        }
        if (!member.getEmail().isEmpty()) {
            tbMember.setEmail(member.getEmail());
        }
        if (!member.getSex().isEmpty()) {
            tbMember.setSex(member.getSex());
        }
        if (!member.getDescription().isEmpty()) {
            tbMember.setDescription(member.getDescription());
        }
        if (!member.getProvince().isEmpty()) {
            tbMember.setAddress(member.getProvince() + " "
                    + member.getCity() + " " + member.getDistrict());
        }
        return tbMember;
    }
}
