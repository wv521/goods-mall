package ai.yunxi.mall.user.service;

import ai.yunxi.mall.common.ServiceResult;
import ai.yunxi.mall.user.service.dto.Member;

public interface MemberService {

    /**
     * 根据ID获取会员信息
     *
     * @param memberId
     * @return
     */
    Member getById(long memberId);

    /**
     * 分页获得会员列表
     *
     * @param draw
     * @param start
     * @param length
     * @param search
     * @return
     */
    ServiceResult getList(int draw, int start, int length, String search,
                          String minDate, String maxDate, String orderCol, String orderDir);

    /**
     * 分页获得移除会员列表
     *
     * @param draw
     * @param start
     * @param length
     * @param search
     * @param minDate
     * @param maxDate
     * @param orderCol
     * @param orderDir
     * @return
     */
    ServiceResult getRemoveList(int draw, int start, int length, String search,
                                String minDate, String maxDate, String orderCol,
                                String orderDir);

    /**
     * 获得所有会员总数
     *
     * @return
     */
    ServiceResult getMemberCount();

    /**
     * 获得删除会员
     *
     * @return
     */
    ServiceResult getRemoveCount();

    /**
     * 通过邮件获取
     *
     * @param email
     * @return
     */
    Member getByEmail(String email);

    /**
     * 通过手机获取
     *
     * @param phone
     * @return
     */
    Member getByPhone(String phone);

    /**
     * 通过获取名获取
     *
     * @param username
     * @return
     */
    Member getByUsername(String username);

    /**
     * 添加会员
     *
     * @param member
     * @return
     */
    Member addMember(Member member);

    /**
     * 更新会员信息
     *
     * @param id
     * @param member
     * @return
     */
    Member updateMember(Long id, Member member);

    /**
     * 修改会员密码
     *
     * @param id
     * @param member
     * @return
     */
    Member changePassword(Long id, Member member);

    /**
     * 修改会员状态
     *
     * @param id
     * @param state
     * @return
     */
    Member updateState(Long id, Integer state);

    /**
     * 彻底删除会员
     *
     * @param id
     * @return
     */
    int deleteMember(Long id);

    /**
     * 验证编辑邮箱是否存在
     *
     * @param id
     * @param email
     * @return
     */
    Member getByEditEmail(Long id, String email);

    /**
     * 验证编辑手机是否存在
     *
     * @param id
     * @param phone
     * @return
     */
    Member getByEditPhone(Long id, String phone);

    /**
     * 验证编辑用户名是否存在
     *
     * @param id
     * @param username
     * @return
     */
    Member getByEditUsername(Long id, String username);

    /**
     * 上传头像
     *
     * @param userId
     * @param token
     * @param imgData
     * @return
     */
    String imageUpload(Long userId, String token, String imgData);
}
