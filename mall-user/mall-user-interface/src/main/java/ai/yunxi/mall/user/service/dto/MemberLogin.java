package ai.yunxi.mall.user.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class MemberLogin implements Serializable {
    private String userName;
    private String userPwd;
    private String challenge;
    private String validate;
    private String seccode;
    private String statusKey;
}
