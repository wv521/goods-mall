package ai.yunxi.mall.user.service;

import ai.yunxi.mall.user.domain.entity.TbAddress;
import ai.yunxi.mall.user.service.dto.AddressInfo;

import java.util.List;

public interface AddressService {

    /**
     * 获取地址
     *
     * @param userId
     * @return
     */
    List<AddressInfo> getAddressList(Long userId);

    /**
     * 获取单个
     *
     * @param addressId
     * @return
     */
    AddressInfo getAddress(Long addressId);

    /**
     * 添加
     *
     * @param addressInfo
     * @return
     */
    int addAddress(AddressInfo addressInfo);

    /**
     * 更新
     *
     * @param addressInfo
     * @return
     */
    int updateAddress(AddressInfo addressInfo);

    /**
     * 删除
     *
     * @param id
     * @return
     */
    int deleteAddress(Long id);
}
