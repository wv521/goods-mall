package ai.yunxi.mall.product.domain.mapper;

import ai.yunxi.mall.product.domain.entity.TbProductLog;
import ai.yunxi.mall.product.domain.entity.TbProductLogExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TbProductLogMapper {
    long countByExample(TbProductLogExample example);

    int deleteByExample(TbProductLogExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TbProductLog record);

    int insertSelective(TbProductLog record);

    List<TbProductLog> selectByExample(TbProductLogExample example);

    TbProductLog selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TbProductLog record, @Param("example") TbProductLogExample example);

    int updateByExample(@Param("record") TbProductLog record, @Param("example") TbProductLogExample example);

    int updateByPrimaryKeySelective(TbProductLog record);

    int updateByPrimaryKey(TbProductLog record);
}