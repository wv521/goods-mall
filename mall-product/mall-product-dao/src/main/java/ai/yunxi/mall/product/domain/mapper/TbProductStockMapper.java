package ai.yunxi.mall.product.domain.mapper;

import ai.yunxi.mall.product.domain.entity.TbProduct;
import ai.yunxi.mall.product.domain.entity.TbProductStock;
import ai.yunxi.mall.product.domain.entity.TbProductStockExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TbProductStockMapper {
    long countByExample(TbProductStockExample example);

    int deleteByExample(TbProductStockExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TbProductStock record);

    int insertSelective(TbProductStock record);

    List<TbProductStock> selectByExample(TbProductStockExample example);

    TbProductStock selectByPrimaryKey(Long id);

    List<TbProductStock> selectAllByLimit(@Param("start") int start, @Param("pageSize") int pageSize);

    int decreaseStockById(@Param("productId") Long productId, @Param("num") Integer num);

    int increaseStockById(@Param("productId") Long productId, @Param("num") Integer num);

    int updateByExampleSelective(@Param("record") TbProductStock record, @Param("example") TbProductStockExample example);

    int updateByExample(@Param("record") TbProductStock record, @Param("example") TbProductStockExample example);

    int updateByPrimaryKeySelective(TbProductStock record);

    int updateByPrimaryKey(TbProductStock record);
}