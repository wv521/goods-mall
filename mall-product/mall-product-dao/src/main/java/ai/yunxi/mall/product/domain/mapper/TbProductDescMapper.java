package ai.yunxi.mall.product.domain.mapper;

import ai.yunxi.mall.product.domain.entity.TbProductDesc;
import ai.yunxi.mall.product.domain.entity.TbProductDescExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TbProductDescMapper {
    long countByExample(TbProductDescExample example);

    int deleteByExample(TbProductDescExample example);

    int deleteByPrimaryKey(Long productId);

    int insert(TbProductDesc record);

    int insertSelective(TbProductDesc record);

    List<TbProductDesc> selectByExampleWithBLOBs(TbProductDescExample example);

    List<TbProductDesc> selectByExample(TbProductDescExample example);

    TbProductDesc selectByPrimaryKey(Long productId);

    int updateByExampleSelective(@Param("record") TbProductDesc record, @Param("example") TbProductDescExample example);

    int updateByExampleWithBLOBs(@Param("record") TbProductDesc record, @Param("example") TbProductDescExample example);

    int updateByExample(@Param("record") TbProductDesc record, @Param("example") TbProductDescExample example);

    int updateByPrimaryKeySelective(TbProductDesc record);

    int updateByPrimaryKeyWithBLOBs(TbProductDesc record);

    int updateByPrimaryKey(TbProductDesc record);
}