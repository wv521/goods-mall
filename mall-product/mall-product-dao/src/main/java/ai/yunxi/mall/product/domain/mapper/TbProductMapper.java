package ai.yunxi.mall.product.domain.mapper;

import ai.yunxi.mall.product.domain.entity.TbProduct;
import ai.yunxi.mall.product.domain.entity.TbProductExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TbProductMapper {
    long countByExample(TbProductExample example);

    int deleteByExample(TbProductExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TbProduct record);

    int insertSelective(TbProduct record);

    List<TbProduct> selectByExample(TbProductExample example);

    TbProduct selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TbProduct record, @Param("example") TbProductExample example);

    int updateByExample(@Param("record") TbProduct record, @Param("example") TbProductExample example);

    int updateByPrimaryKeySelective(TbProduct record);

    int updateByPrimaryKey(TbProduct record);

    List<TbProduct> selectAllByLimit(@Param("start") int start, @Param("pageSize") int pageSize);

    List<TbProduct> selectByCondition(@Param("categoryId") int categoryId, @Param("search") String search,
                                          @Param("orderCol") String orderCol, @Param("orderDir") String orderDir);

    List<TbProduct> selectByMultiCondition(@Param("categoryId") int categoryId, @Param("search") String search,
                                               @Param("minDate") String minDate, @Param("maxDate") String maxDate,
                                               @Param("orderCol") String orderCol, @Param("orderDir") String orderDir);

    List<TbProduct> selectItemFront(@Param("categoryId") Long categoryId,
                                    @Param("column") String column, @Param("direction") String direction,
                                    @Param("priceGt") int priceGt, @Param("priceLte") int priceLte);
}