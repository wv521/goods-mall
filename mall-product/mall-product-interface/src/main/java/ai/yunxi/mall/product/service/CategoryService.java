package ai.yunxi.mall.product.service;

import ai.yunxi.mall.common.TreeNode;
import ai.yunxi.mall.product.service.dto.Category;

import java.util.List;

public interface CategoryService {

    /**
     * 通过id获取
     *
     * @param id
     * @return
     */
    Category getById(Long id);

    /**
     * 获得分类树
     *
     * @param parentId
     * @return
     */
    List<TreeNode> getList(int parentId);

    /**
     * 添加分类
     *
     * @param category
     * @return
     */
    int addCategory(Category category);

    /**
     * 编辑分类
     *
     * @param category
     * @return
     */
    int updateCategory(Category category);

    /**
     * 删除单个分类
     *
     * @param id
     */
    void deleteCategory(Long id);

    /**
     * 递归删除
     *
     * @param id
     */
    void deleteZTree(Long id);
}
