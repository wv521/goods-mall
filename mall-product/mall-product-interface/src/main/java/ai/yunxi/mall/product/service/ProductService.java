package ai.yunxi.mall.product.service;

import ai.yunxi.mall.common.ServiceResult;
import ai.yunxi.mall.product.domain.entity.TbProduct;
import ai.yunxi.mall.product.service.dto.Product;

import java.util.List;

public interface ProductService {

    /**
     * 通过ID获取商品包含详情
     *
     * @param id
     * @return
     */
    Product getById(Long id);

    /**
     * 通过ID获取不包含详情
     *
     * @param id
     * @return
     */
    Product getNormalById(Long id);

    /**
     * 分页排序获取商品列表
     *
     * @param draw
     * @param start
     * @param length
     * @param categroyId
     * @param search
     * @param orderCol
     * @param orderDir
     * @return
     */
    ServiceResult getList(int draw, int start, int length, int categroyId,
                          String search, String orderCol, String orderDir);

    /**
     * 分页获取所有记录
     *
     * @param start
     * @param pageSize
     * @return
     */
    List<Product> getList(int start, int pageSize);

    /**
     * 多条件查询获取商品列表
     *
     * @param draw
     * @param start
     * @param length
     * @param categroyId
     * @param search
     * @param minDate
     * @param maxDate
     * @param orderCol
     * @param orderDir
     * @return
     */
    ServiceResult searchList(int draw, int start, int length, int categroyId,
                             String search, String minDate, String maxDate,
                             String orderCol, String orderDir);

    List<Product> searchList(Long categoryId, String column, String direction,
                             int priceGt, int priceLte);

    /**
     * 获取商品总数
     *
     * @return
     */
    ServiceResult getAllCount();

    /**
     * 修改商品状态
     *
     * @param id
     * @param state
     * @return
     */
    Product updateState(Long id, Integer state);

    /**
     * 彻底删除商品
     *
     * @param id
     * @return
     */
    int deleteProduct(Long id);

    /**
     * 添加商品
     *
     * @param product
     * @return
     */
    Product addProduct(Product product);

    /**
     * 更新商品
     *
     * @param id
     * @param product
     * @return
     */
    Product updateProduct(Long id, Product product);
}
