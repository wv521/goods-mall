package ai.yunxi.mall.product.service.dto;

import ai.yunxi.mall.product.domain.entity.TbProduct;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Setter
@Getter
public class Product implements Serializable {
    private Long productId;
    private String title;
    private String subTitle;
    private BigDecimal price;
    private Long categoryId;
    private String categoryName;
    private Integer num;
    private Integer limitNum;
    private String detail;
    private Byte status;
    private Date created;
    private Date updated;
    private String image;
    private List<?> smallImages;

    public static Product fromTbProduct(TbProduct tbProduct) {
        Product product = new Product();
        product.setProductId(tbProduct.getId());
        product.setTitle(tbProduct.getTitle());
        product.setSubTitle(tbProduct.getSellPoint());
        product.setPrice(tbProduct.getPrice());
        product.setCategoryId(tbProduct.getCategoryId());
        product.setImage(tbProduct.getImage());
        product.setNum(tbProduct.getNum());
        if (tbProduct.getLimitNum() == null) {
            product.setLimitNum(tbProduct.getNum());
        } else if (tbProduct.getLimitNum() < 0 && tbProduct.getNum() < 0) {
            product.setLimitNum(10);
        } else {
            product.setLimitNum(tbProduct.getLimitNum());
        }
        return product;
    }

    public static TbProduct toTbProduct(Product product) {
        TbProduct tbProduct = new TbProduct();
        tbProduct.setTitle(product.getTitle());
        tbProduct.setPrice(product.getPrice());
        tbProduct.setCategoryId(product.getCategoryId());
        tbProduct.setImage(product.getImage());
        tbProduct.setSellPoint(product.getSubTitle());
        tbProduct.setNum(product.getNum());
        if (product.getLimitNum() == null || product.getLimitNum() < 0) {
            tbProduct.setLimitNum(10);
        } else {
            tbProduct.setLimitNum(product.getLimitNum());
        }
        return tbProduct;
    }
}
