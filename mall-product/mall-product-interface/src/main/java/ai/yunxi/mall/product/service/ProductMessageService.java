package ai.yunxi.mall.product.service;

import ai.yunxi.mall.product.service.dto.ProductLog;

public interface ProductMessageService {

    public boolean send(ProductLog productLog);

    public void receive(String message);
}
