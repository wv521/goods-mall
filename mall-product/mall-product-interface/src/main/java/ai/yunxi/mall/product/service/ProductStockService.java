package ai.yunxi.mall.product.service;

import java.util.List;

public interface ProductStockService {

    /**
     * 扣减商品缓存库存
     *
     * @param keys
     * @param args
     * @return
     */
    Integer decrease(Long orderId, List<String> keys, List<String> args);

    /**
     * 恢复商品缓存库存
     *
     * @param keys
     * @param args
     * @return
     */
    Integer increase(Long orderId, List<String> keys, List<String> args);
}
