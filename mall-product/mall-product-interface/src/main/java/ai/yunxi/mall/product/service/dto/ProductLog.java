package ai.yunxi.mall.product.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
public class ProductLog implements Serializable {
    private Long id;
    private Long productId;
    private Long orderId;
    private Integer num;
    private Integer status;
    private Date createDate;
}
