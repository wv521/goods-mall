package ai.yunxi.mall.product.service.impl;

import ai.yunxi.mall.common.redis.JedisClient;
import ai.yunxi.mall.product.service.ProductMessageService;
import ai.yunxi.mall.product.service.ProductStockService;
import ai.yunxi.mall.product.service.dto.ProductLog;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@DubboService
public class ProductStockServiceImpl implements ProductStockService {

    @Resource
    private ProductMessageService productMessageService;

    @Resource
    private JedisClient jedisClient;
    @Value("${PRODUCT_STOCK}")
    private String PRODUCT_STOCK;

    // 扣减库存脚本
    private static final String DECR_LUA;
    // 恢复库存脚本
    private static final String INCR_LUA;

    static {
        StringBuilder sb = new StringBuilder();
        sb.append("if (redis.call('exists', KEYS[1]) == 1) then");
        sb.append("    local balanceNum = tonumber(redis.call('get', KEYS[1]));");
        sb.append("    local purchaseNum = tonumber(ARGV[1]);");
        sb.append("    if tonumber(balanceNum) == 0 or (balanceNum - purchaseNum) < 0 then");
        sb.append("        return -200;");
        sb.append("    end;");
        sb.append("    return redis.call('incrby', KEYS[1], -purchaseNum);");
        sb.append("end;");
        sb.append("return -100;");
        DECR_LUA = sb.toString();

        sb = new StringBuilder();
        sb.append("if (redis.call('exists', KEYS[1]) == 1) then");
        sb.append("    local recoveryNum = tonumber(ARGV[1]);");
        sb.append("    return redis.call('incrby', KEYS[1], recoveryNum);");
        sb.append("end;");
        sb.append("return -100;");
        INCR_LUA = sb.toString();
    }

    @Override
    public Integer decrease(Long orderId, List<String> keys, List<String> args) {
        List<String> productIds = keys.stream().map(key -> {
            return PRODUCT_STOCK + key;
        }).collect(Collectors.toList());
        String result = jedisClient.eval(DECR_LUA, productIds, args).toString();

        if (result.equals("-200") || result.equals("-100")) {
            return -1;
        } else {
            // 发送MQ，保存库存信息
            for (int i = 0; i < keys.size(); i++) {
                ProductLog productLog = new ProductLog();
                productLog.setOrderId(orderId);
                productLog.setProductId(Long.valueOf(keys.get(i)));
                productLog.setNum(Integer.valueOf(args.get(i)));
                productLog.setStatus(1);
                productMessageService.send(productLog);
            }
            return 1;
        }
    }

    @Override
    public Integer increase(Long orderId, List<String> keys, List<String> args) {
        List<String> productIds = keys.stream().map(key -> {
            return PRODUCT_STOCK + key;
        }).collect(Collectors.toList());
        String result = jedisClient.eval(INCR_LUA, productIds, args).toString();

        if (result.equals("-100")) {
            return -1;
        } else {
            // 发送MQ，保存库存信息。
            for (int i = 0; i < keys.size(); i++) {
                ProductLog productLog = new ProductLog();
                productLog.setOrderId(orderId);
                productLog.setProductId(Long.valueOf(keys.get(i)));
                productLog.setNum(-1 * Integer.valueOf(args.get(i)));
                productLog.setStatus(2);
                productMessageService.send(productLog);
            }
            return 1;
        }
    }
}
