package ai.yunxi.mall.product.service.impl;

import ai.yunxi.mall.common.redis.JedisClient;
import ai.yunxi.mall.product.domain.entity.TbCategory;
import ai.yunxi.mall.product.domain.entity.TbProduct;
import ai.yunxi.mall.product.domain.entity.TbProductDesc;
import ai.yunxi.mall.product.domain.entity.TbProductStock;
import ai.yunxi.mall.product.domain.mapper.TbCategoryMapper;
import ai.yunxi.mall.product.domain.mapper.TbProductDescMapper;
import ai.yunxi.mall.product.domain.mapper.TbProductMapper;
import ai.yunxi.mall.product.domain.mapper.TbProductStockMapper;
import ai.yunxi.mall.product.service.dto.Product;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class ProductCachePreheat implements ApplicationRunner {

    @Resource
    private TbProductMapper tbProductMapper;
    @Resource
    private TbProductDescMapper tbProductDescMapper;
    @Resource
    private TbProductStockMapper tbProductStockMapper;
    @Resource
    private TbCategoryMapper tbCategoryMapper;
    @Resource
    private JedisClient jedisClient;

    @Value("${PRODUCT_INFO}")
    private String PRODUCT_INFO;

    @Value("${PRODUCT_STOCK}")
    private String PRODUCT_STOCK;

    @Value("${ITEM_EXPIRE}")
    private int ITEM_EXPIRE;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        // 预热商品信息
        preheatInfo();
        // 预热库存信息
        preheatStock();
    }

    private void preheatInfo() {
        int start = 0;
        int length = 20;
        ObjectMapper om = new ObjectMapper();
        List<TbProduct> list = null;

        do {
            list = tbProductMapper.selectAllByLimit(start * length, length);
            list.forEach(tbProduct -> {
                Product product = Product.fromTbProduct(tbProduct);
                TbCategory tbCategory = tbCategoryMapper.selectByPrimaryKey(product.getCategoryId());
                product.setCategoryName(tbCategory.getName());
                TbProductDesc tbProductDesc = tbProductDescMapper.selectByPrimaryKey(product.getProductId());
                product.setDetail(tbProductDesc == null ? "" : tbProductDesc.getDescription());
                if (product.getImage() != null && !product.getImage().isEmpty()) {
                    String images[] = product.getImage().split(",");
                    product.setImage(images[0]);
                    List imgList = new ArrayList();
                    for (int i = 0; i < images.length; i++) {
                        imgList.add(images[i]);
                    }
                    product.setSmallImages(imgList);
                }

                try {
                    jedisClient.set(PRODUCT_INFO + tbProduct.getId(), om.writeValueAsString(product));
                    jedisClient.expire(PRODUCT_INFO + product.getProductId(), ITEM_EXPIRE);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            });
            start++;
        }
        while (null != list && list.size() >= length);
    }

    private void preheatStock() {
        int start = 0;
        int length = 20;
        ObjectMapper om = new ObjectMapper();
        List<TbProductStock> list = null;

        do {
            list = tbProductStockMapper.selectAllByLimit(start * length, length);
            list.forEach(tbProductStock -> {
                if (jedisClient.get(PRODUCT_STOCK + tbProductStock.getId()) == null) {
                    jedisClient.set(PRODUCT_STOCK + tbProductStock.getId(),
                            String.valueOf(tbProductStock.getNum()));
                }
            });
            start++;
        }
        while (null != list && list.size() >= length);
    }
}
