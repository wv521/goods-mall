package ai.yunxi.mall.product.service;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication(scanBasePackages = "ai.yunxi")
@MapperScan("ai.yunxi.mall.product.domain.mapper")
@EnableDubbo(scanBasePackages = {"ai.yunxi.mall.product.service.impl"})
public class MallProductServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MallProductServiceApplication.class, args);
	}

}
