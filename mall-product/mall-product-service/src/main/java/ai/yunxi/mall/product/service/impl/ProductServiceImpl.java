package ai.yunxi.mall.product.service.impl;

import ai.yunxi.mall.common.ServiceResult;
import ai.yunxi.mall.common.exception.CommonException;
import ai.yunxi.mall.common.redis.JedisClient;
import ai.yunxi.mall.common.redis.RedisLock;
import ai.yunxi.mall.product.domain.entity.*;
import ai.yunxi.mall.product.domain.mapper.TbCategoryMapper;
import ai.yunxi.mall.product.domain.mapper.TbProductDescMapper;
import ai.yunxi.mall.product.domain.mapper.TbProductMapper;
import ai.yunxi.mall.product.domain.mapper.TbProductStockMapper;
import ai.yunxi.mall.product.service.ProductService;
import ai.yunxi.mall.product.service.dto.Product;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.shardingsphere.core.strategy.keygen.SnowflakeShardingKeyGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@DubboService
public class ProductServiceImpl implements ProductService {

    private final static Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Resource
    private TbProductMapper tbProductMapper;
    @Resource
    private TbProductDescMapper tbProductDescMapper;
    @Resource
    private TbProductStockMapper tbProductStockMapper;
    @Resource
    private TbCategoryMapper tbCategoryMapper;
    @Resource
    private JedisClient jedisClient;
    @Resource
    private RedisLock redisLock;

    @Value("${PRODUCT_INFO}")
    private String PRODUCT_INFO;

    @Value("${ITEM_EXPIRE}")
    private int ITEM_EXPIRE;

    @Override
    public Product getById(Long id) {
        Product product = null;
        //查询缓存
        String json = jedisClient.get(PRODUCT_INFO + id);
        if (json == null) {
            try {
                // 分布式锁解决缓存击穿的问题
                if (redisLock.tryLock(PRODUCT_INFO + id, String.valueOf(id))) {
                    json = jedisClient.get(PRODUCT_INFO + id);
                    if (json != null) {
                        product = getProductFromJson(json);
                        log.info("读取商品：" + product.getProductId() + " 的缓存信息");
                        //重置缓存TTL时间
                        jedisClient.expire(PRODUCT_INFO + id, ITEM_EXPIRE);
                        return product;
                    }

                    product = this.getProductFromDB(id);
                    if (null != product) {
                        // 把结果添加至缓存
                        ObjectMapper om = new ObjectMapper();
                        jedisClient.set(PRODUCT_INFO + id, om.writeValueAsString(product));
                        //设置缓存TTL时间
                        jedisClient.expire(PRODUCT_INFO + id, ITEM_EXPIRE);
                        log.info("添加商品：" + id + " 的缓存信息");
                    }
                }
            } catch (JsonProcessingException e) {
                log.error("解析商品信息出错：" + e.getMessage());
            } finally {
                redisLock.unlock(PRODUCT_INFO + id, String.valueOf(id));
            }
        } else {
            product = getProductFromJson(json);
        }
        return product;
    }

    private Product getProductFromJson(String json) {
        Product product = null;
        try {
            ObjectMapper om = new ObjectMapper();
            product = om.readValue(json, Product.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return product;
    }

    private Product getProductFromDB(Long id) {
        TbProduct tbProduct = tbProductMapper.selectByPrimaryKey(id);
        Product product = Product.fromTbProduct(tbProduct);
        TbCategory tbCategory = tbCategoryMapper.selectByPrimaryKey(product.getCategoryId());
        product.setCategoryName(tbCategory.getName());
        TbProductDesc tbProductDesc = tbProductDescMapper.selectByPrimaryKey(id);
        product.setDetail(tbProductDesc == null ? "" : tbProductDesc.getDescription());
        if (product.getImage() != null && !product.getImage().isEmpty()) {
            String images[] = product.getImage().split(",");
            product.setImage(images[0]);
            List imgList = new ArrayList();
            for (int i = 0; i < images.length; i++) {
                imgList.add(images[i]);
            }
            product.setSmallImages(imgList);
        }
        return product;
    }

    @Override
    public Product getNormalById(Long id) {
        TbProduct tbProduct = tbProductMapper.selectByPrimaryKey(id);
        return Product.fromTbProduct(tbProduct);
    }

    @Override
    public ServiceResult getList(int draw, int start, int length, int cid, String search,
                                 String orderCol, String orderDir) {
        ServiceResult result = new ServiceResult();
        //分页执行查询返回结果
        PageHelper.startPage(start / length + 1, length);
        List<TbProduct> list = tbProductMapper.selectByCondition(cid, "%" + search + "%", orderCol, orderDir);
        PageInfo<TbProduct> pageInfo = new PageInfo<>(list);
        result.setRecordsFiltered((int) pageInfo.getTotal());
        result.setRecordsTotal(getAllCount().getRecordsTotal());
        result.setDraw(draw);
        result.setData(list);
        return result;
    }

    @Override
    public List<Product> getList(int start, int pageSize) {
        List<TbProduct> list = tbProductMapper.selectAllByLimit(start, pageSize);
        return list.stream().map(tbProduct -> {
            return Product.fromTbProduct(tbProduct);
        }).collect(Collectors.toList());
    }

    @Override
    public ServiceResult searchList(int draw, int start, int length, int cid, String search,
                                    String minDate, String maxDate, String orderCol, String orderDir) {
        ServiceResult result = new ServiceResult();
        //分页执行查询返回结果
        PageHelper.startPage(start / length + 1, length);
        List<TbProduct> list = tbProductMapper.selectByMultiCondition(cid, "%" + search + "%", minDate, maxDate, orderCol, orderDir);
        PageInfo<TbProduct> pageInfo = new PageInfo<>(list);
        result.setRecordsFiltered((int) pageInfo.getTotal());
        result.setRecordsTotal(getAllCount().getRecordsTotal());
        result.setDraw(draw);
        result.setData(list);
        return result;
    }

    @Override
    public List<Product> searchList(Long categoryId, String column, String direction,
                                    int priceGt, int priceLte) {
        List<TbProduct> list = tbProductMapper.selectItemFront(categoryId, column,
                direction, priceGt, priceLte);
        return list.stream().map(tbProduct -> {
            return Product.fromTbProduct(tbProduct);
        }).collect(Collectors.toList());
    }

    @Override
    public ServiceResult getAllCount() {
        TbProductExample example = new TbProductExample();
        Long count = tbProductMapper.countByExample(example);
        ServiceResult result = new ServiceResult();
        result.setRecordsTotal(Math.toIntExact(count));
        return result;
    }

    @Override
    public Product updateState(Long id, Integer state) {
        TbProduct tbProduct = tbProductMapper.selectByPrimaryKey(id);
        tbProduct.setStatus(state.byteValue());
        tbProduct.setUpdated(new Date());
        if (tbProductMapper.updateByPrimaryKey(tbProduct) != 1) {
            throw new CommonException("修改商品状态失败");
        }
        return getNormalById(id);
    }

    @Override
    @Transactional
    public int deleteProduct(Long id) {
        if (tbProductMapper.deleteByPrimaryKey(id) != 1) {
            throw new CommonException("删除商品失败");
        }
        if (tbProductDescMapper.deleteByPrimaryKey(id) != 1) {
            throw new CommonException("删除商品详情失败");
        }
        if (tbProductStockMapper.deleteByPrimaryKey(id) != -1) {
            throw new CommonException("删除商品库存失败");
        }

        return 0;
    }

    @Override
    @Transactional
    public Product addProduct(Product product) {
        SnowflakeShardingKeyGenerator keyGenerator = new SnowflakeShardingKeyGenerator();
        Long id = (Long) keyGenerator.generateKey();
        TbProduct tbProduct = Product.toTbProduct(product);
        tbProduct.setId(id);
        tbProduct.setStatus((byte) 1);
        tbProduct.setCreated(new Date());
        tbProduct.setUpdated(new Date());
        if (tbProduct.getImage().isEmpty()) {
            tbProduct.setImage("http://ow2h3ee9w.bkt.clouddn.com/nopic.jpg");
        }
        if (tbProductMapper.insert(tbProduct) != 1) {
            throw new CommonException("添加商品失败");
        }

        TbProductDesc tbProductDesc = new TbProductDesc();
        tbProductDesc.setId(id);
        tbProductDesc.setDescription(product.getDetail());
        tbProductDesc.setCreated(new Date());
        tbProductDesc.setUpdated(new Date());
        if (tbProductDescMapper.insert(tbProductDesc) != 1) {
            throw new CommonException("添加商品详情失败");
        }

        TbProductStock tbProductStock = new TbProductStock();
        tbProductStock.setId(id);
        tbProductStock.setNum(product.getNum());
        tbProductStock.setLimitNum(product.getLimitNum());
        if (tbProductStockMapper.insert(tbProductStock) != 1) {
            throw new CommonException("添加商品库存失败");
        }

        return getNormalById(id);
    }

    @Override
    @Transactional
    public Product updateProduct(Long id, Product product) {
        TbProduct oldTbProduct = tbProductMapper.selectByPrimaryKey(id);
        TbProduct tbProduct = Product.toTbProduct(product);
        if (tbProduct.getImage().isEmpty()) {
            tbProduct.setImage(oldTbProduct.getImage());
        }

        tbProduct.setId(id);
        tbProduct.setStatus(oldTbProduct.getStatus());
        tbProduct.setCreated(oldTbProduct.getCreated());
        tbProduct.setUpdated(new Date());
        if (tbProductMapper.updateByPrimaryKey(tbProduct) != 1) {
            throw new CommonException("更新商品失败");
        }

        TbProductDesc tbProductDesc = new TbProductDesc();
        tbProductDesc.setId(id);
        tbProductDesc.setDescription(product.getDetail());
        tbProductDesc.setUpdated(new Date());
        tbProductDesc.setCreated(oldTbProduct.getCreated());
        if (tbProductDescMapper.updateByPrimaryKey(tbProductDesc) != 1) {
            throw new CommonException("更新商品详情失败");
        }

        return getNormalById(id);
    }
}
