package ai.yunxi.mall.product.service.impl;

import ai.yunxi.mall.common.TreeNode;
import ai.yunxi.mall.common.exception.CommonException;
import ai.yunxi.mall.product.service.CategoryService;
import ai.yunxi.mall.product.domain.entity.TbCategory;
import ai.yunxi.mall.product.domain.entity.TbCategoryExample;
import ai.yunxi.mall.product.domain.mapper.TbCategoryMapper;
import ai.yunxi.mall.product.service.dto.Category;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@DubboService
public class CategoryServiceImpl implements CategoryService {

    @Resource
    private TbCategoryMapper tbCategoryMapper;

    @Override
    public Category getById(Long id) {
        TbCategory tbCategory = tbCategoryMapper.selectByPrimaryKey(id);
        if (tbCategory == null) {
            throw new CommonException("通过id获取商品分类失败");
        }
        Category category = new Category();
        BeanUtils.copyProperties(tbCategory, category);
        return category;
    }

    @Override
    public List<TreeNode> getList(int parentId) {
        TbCategoryExample example = new TbCategoryExample();
        TbCategoryExample.Criteria criteria = example.createCriteria();
        //排序
        example.setOrderByClause("sort_order");
        //条件查询
        criteria.andParentIdEqualTo(new Long(parentId));
        List<TbCategory> list = tbCategoryMapper.selectByExample(example);
        //转换成ZtreeNode
        List<TreeNode> resultList = new ArrayList<>();
        for (TbCategory tbCategory : list) {
            TreeNode node = TbCategory.transform(tbCategory);
            resultList.add(node);
        }

        return resultList;
    }

    @Override
    public int addCategory(Category category) {
        TbCategory tbCategory = new TbCategory();
        BeanUtils.copyProperties(category, tbCategory);
        if (category.getParentId() == 0) {
            //根节点
            tbCategory.setSortOrder(0);
            tbCategory.setStatus(1);
        } else {
            TbCategory parent = tbCategoryMapper.selectByPrimaryKey(category.getParentId());
            tbCategory.setSortOrder(0);
            tbCategory.setStatus(1);
            tbCategory.setCreated(new Date());
            tbCategory.setUpdated(new Date());
        }

        if (tbCategoryMapper.insert(tbCategory) != 1) {
            throw new CommonException("添加商品分类失败");
        }
        return 1;
    }

    @Override
    public int updateCategory(Category category) {
        TbCategory tbCategory = new TbCategory();
        BeanUtils.copyProperties(category, tbCategory);
        Category oldCategory = getById(category.getId());
        tbCategory.setCreated(oldCategory.getCreated());
        tbCategory.setUpdated(new Date());
        if (tbCategoryMapper.updateByPrimaryKey(tbCategory) != 1) {
            throw new CommonException("添加商品分类失败");
        }
        return 1;
    }

    @Override
    public void deleteCategory(Long id) {
        deleteZTree(id);
    }

    @Override
    public void deleteZTree(Long id) {
        //查询该节点所有子节点
        List<TreeNode> node = getList(Math.toIntExact(id));
        if (node.size() > 0) {
            //如果有子节点，遍历子节点
            for (int i = 0; i < node.size(); i++) {
                deleteCategory((long) node.get(i).getId());
            }
        }
        //没有子节点直接删除
        if (tbCategoryMapper.deleteByPrimaryKey(id) != 1) {
            throw new CommonException("删除商品分类失败");
        }
    }
}
