package ai.yunxi.mall.product.service.impl;

import ai.yunxi.mall.common.exception.CommonException;
import ai.yunxi.mall.product.domain.entity.TbProductLog;
import ai.yunxi.mall.product.domain.mapper.TbProductLogMapper;
import ai.yunxi.mall.product.domain.mapper.TbProductStockMapper;
import ai.yunxi.mall.product.service.ProductMessageService;
import ai.yunxi.mall.product.service.dto.ProductLog;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.shardingsphere.core.strategy.keygen.SnowflakeShardingKeyGenerator;
import org.apache.shardingsphere.spi.keygen.ShardingKeyGenerator;
import org.springframework.beans.BeanUtils;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

@Service
@EnableBinding({Source.class, Sink.class})
public class ProductMessageServiceImpl implements ProductMessageService {

    @Resource
    private TbProductStockMapper tbProductStockMapper;
    @Resource
    private TbProductLogMapper tbProductLogMapper;

    @Resource
    private Source source;

    @Override
    public boolean send(ProductLog productLog) {
        ObjectMapper om = new ObjectMapper();
        String message = null;
        try {
            message = om.writeValueAsString(productLog);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        if (StringUtils.isBlank(message))
            return false;
        else {
            Message msg = MessageBuilder.withPayload(message).build();
            return source.output().send(msg);
        }
    }

    @Override
    @Transactional
    @StreamListener(Sink.INPUT)
    public void receive(String message) {
        ObjectMapper om = new ObjectMapper();
        ShardingKeyGenerator keyGenerator = new SnowflakeShardingKeyGenerator();

        // 从MQ中读取消息的对象
        ProductLog productLog = null;

        try {
            productLog = om.readValue(message, ProductLog.class);
            // DB增加扣减流水记录
            TbProductLog tbProductLog = new TbProductLog();
            BeanUtils.copyProperties(productLog, tbProductLog);
            tbProductLog.setId((Long) keyGenerator.generateKey());
            tbProductLog.setCreateDate(new Date());
            tbProductLogMapper.insert(tbProductLog);
            // 扣减DB库存
            tbProductStockMapper.decreaseStockById(productLog.getProductId(),
                    productLog.getNum());
        } catch (JsonProcessingException e) {
            throw new CommonException("生成订单信息失败");
        }
    }
}
