package ai.yunxi.mall.product.domain.entity;

import ai.yunxi.mall.common.TreeNode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
public class TbCategory implements Serializable {
    private Long id;
    private Long parentId;
    private String name;
    private Integer status;
    private Integer sortOrder;
    private Boolean isParent;
    private Date created;
    private Date updated;
    private String icon;
    private String remark;

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public void setIcon(String icon) {
        this.icon = icon == null ? null : icon.trim();
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public static TreeNode transform(TbCategory tbCategory) {
        TreeNode treeNode = new TreeNode();
        treeNode.setId(Math.toIntExact(tbCategory.getId()));
        treeNode.setStatus(tbCategory.getStatus());
        treeNode.setSortOrder(tbCategory.getSortOrder());
        treeNode.setName(tbCategory.getName());
        treeNode.setpId(Math.toIntExact(tbCategory.getParentId()));
        treeNode.setIsParent(tbCategory.getIsParent());
        treeNode.setRemark(tbCategory.getRemark());
        return treeNode;
    }
}