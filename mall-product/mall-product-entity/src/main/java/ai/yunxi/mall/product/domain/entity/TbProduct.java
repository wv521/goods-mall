package ai.yunxi.mall.product.domain.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
public class TbProduct implements Serializable {
    private Long id;
    private String title;
    private String sellPoint;
    private BigDecimal price;
    private Integer num;
    private Integer limitNum;
    private String image;
    private Long categoryId;
    private Byte status;
    private Date created;
    private Date updated;
}