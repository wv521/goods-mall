package ai.yunxi.mall.gateway.limit;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import reactor.core.publisher.Mono;

@Configuration
public class RateLimiterKeyResolver {

    @Bean
    KeyResolver urlKeyResolver(){
        // 根据URL进行限流
        return exchange -> Mono.just(exchange.getRequest().getPath().value());
    }

    @Bean
    @Primary
    KeyResolver ipKeyResolver() {
        // 根据IP地址进行限流
        return exchange -> Mono.just(exchange.getRequest().getRemoteAddress().getHostName());
    }
}
