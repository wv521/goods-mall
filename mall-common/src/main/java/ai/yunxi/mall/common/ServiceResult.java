package ai.yunxi.mall.common;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
public class ServiceResult implements Serializable {
    private Boolean success;
    private int draw;
    private int recordsTotal;
    private int recordsFiltered;
    private String error;
    private List<?> data;
}
