package ai.yunxi.mall.common.utils;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class CommonResult<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 返回代码
     */
    private Integer code;

    /**
     * 详细信息
     */
    private String message;

    /**
     * 结果对象
     */
    private T data;

    /**
     * 时间戳
     */
    private long timestamp;

    public static <T> CommonResult<T> success(T data) {
        return new CommonResult<>(ResultCode.SUCCESS.getCode(), "", data,
                System.currentTimeMillis());
    }

    public static <T> CommonResult<T> error(Integer code, String message) {
        return new CommonResult<>(code, message, null, System.currentTimeMillis());
    }

    public static <T> CommonResult<T> error(Integer code, String message, T data) {
        return new CommonResult<>(code, message, data, System.currentTimeMillis());
    }

    public static <T> CommonResult<T> error(ResultCode resultCode) {
        return new CommonResult<>(resultCode.getCode(), resultCode.getMessage(), null,
                System.currentTimeMillis());
    }

    public static <T> CommonResult<T> error(ResultCode resultCode, T data) {
        return new CommonResult<>(resultCode.getCode(), resultCode.getMessage(),
                data, System.currentTimeMillis());
    }
}
