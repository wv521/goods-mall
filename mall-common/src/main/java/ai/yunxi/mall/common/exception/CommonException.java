package ai.yunxi.mall.common.exception;

import ai.yunxi.mall.common.utils.ResultCode;

public class CommonException extends RuntimeException {
    private Integer code;
    private String messsage;
    private String detail;

    public CommonException() {
    }

    public CommonException(ResultCode code) {
        this.code = code.getCode();
        this.messsage = code.getMessage();
    }

    public CommonException(String messsage) {
        this.messsage = messsage;
    }

    public CommonException(Integer code, String messsage) {
        this.code = code;
        this.messsage = messsage;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMesssage() {
        return messsage;
    }

    public void setMesssage(String messsage) {
        this.messsage = messsage;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
