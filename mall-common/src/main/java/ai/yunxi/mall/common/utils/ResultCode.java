package ai.yunxi.mall.common.utils;

public enum ResultCode {
    SUCCESS(200, "调用成功"),
    FAILED(-300, "调用失败"),
    PARAMS_ERR(-400, "参数校验错误"),

    ORDER_NOT_EXIST(-601, "订单不存在"),

    VALIDATE_ERR(-700, "请求数据校验不合法"),
    INTERNAL_ERR(-705, "系统异常"),

    ALIPAY_TRADE_SUCCESS(900, "支付宝支付成功"),
    ALIPAY_QUERY_ERROR(-901, "支付宝查询错误"),
    ALIPAY_REFUND_ERROR(-902, "支付宝退款失败"),
    ALIPAY_CLOSE_ERROR(-903, "支付宝交易关闭失败");

    private Integer code;
    private String message;

    ResultCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
