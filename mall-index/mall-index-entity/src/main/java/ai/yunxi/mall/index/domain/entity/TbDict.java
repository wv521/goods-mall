package ai.yunxi.mall.index.domain.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class TbDict implements Serializable {
    private Integer id;
    private String dict;
    private Integer type;
}