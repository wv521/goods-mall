package ai.yunxi.mall.index.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class IndexInfo implements Serializable {
    private String cluster_name;
    private String status;
    private String number_of_nodes;
    private Integer count;
}
