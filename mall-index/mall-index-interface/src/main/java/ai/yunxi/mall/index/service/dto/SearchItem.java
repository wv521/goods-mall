package ai.yunxi.mall.index.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
public class SearchItem implements Serializable {
    private Long productId;
    private String productName;
    private String subTitle;
    private BigDecimal salePrice;
    private String productImageBig;
    private Integer categoryId;
    private String categoryName;
}
