package ai.yunxi.mall.index.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
public class SearchResult implements Serializable {
    private Long recordCount;
    private int totalPages;
    private List<SearchItem> itemList;
}
