package ai.yunxi.mall.index.service;

import ai.yunxi.mall.index.service.dto.SearchResult;

public interface SearchProductService {

    /**
     * 商品搜索
     *
     * @param keyword
     * @param page
     * @param size
     * @param sort
     * @param gtePrice
     * @param ltePrice
     * @return
     */
    SearchResult search(String keyword, int page, int size, String sort, int gtePrice, int ltePrice);
}
