package ai.yunxi.mall.index.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Document(indexName = "shopping_mall", type = "product")
public class Product implements Serializable {
    @Id
    private Long id;

    @Field(type = FieldType.Text)
    private String title;
    @Field(type = FieldType.Text)
    private String subTitle;
    @Field(type = FieldType.Float)
    private BigDecimal price;
    @Field(type = FieldType.Text)
    private String image;
    @Field(type = FieldType.Long)
    private Integer categoryId;
    @Field(type = FieldType.Date)
    private Date created;
    @Field(type = FieldType.Date)
    private Date updated;
    @Field(type = FieldType.Text)
    private String description;
}
