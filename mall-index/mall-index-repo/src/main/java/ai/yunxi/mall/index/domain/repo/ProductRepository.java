package ai.yunxi.mall.index.domain.repo;

import ai.yunxi.mall.index.domain.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends ElasticsearchRepository<Product, Long> {

    Page<Product> findByTitle(String title, Pageable pageable);
}
