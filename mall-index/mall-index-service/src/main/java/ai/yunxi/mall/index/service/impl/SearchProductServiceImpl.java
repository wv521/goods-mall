package ai.yunxi.mall.index.service.impl;

import ai.yunxi.mall.index.domain.repo.ProductRepository;
import ai.yunxi.mall.index.domain.model.Product;
import ai.yunxi.mall.index.service.SearchProductService;
import ai.yunxi.mall.index.service.dto.SearchItem;
import ai.yunxi.mall.index.service.dto.SearchResult;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboService;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;

@DubboService
public class SearchProductServiceImpl implements SearchProductService {

    @Resource
    private ProductRepository productRepository;

    private final String DESC = "-1";
    private final String ASC = "1";

    @Override
    public SearchResult search(String key, int page, int size, String sort, int gtePrice,
                               int ltePrice) {
        //
        NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
        BoolQueryBuilder booleanQueryBuilder = new BoolQueryBuilder();
        booleanQueryBuilder.must(rangeQuery("price").from(gtePrice).to(ltePrice));
        if (StringUtils.isNotBlank(key)) {
            booleanQueryBuilder.must(matchQuery("title", key))
                    .should(matchQuery("subTitle", key));
        }

        if (DESC.equals(sort)) {
            builder.withSort(new FieldSortBuilder("price").order(SortOrder.DESC));
        } else if (ASC.equals(sort)) {
            builder.withSort(new FieldSortBuilder("price").order(SortOrder.ASC));
        } else {
            builder.withSort(SortBuilders.scoreSort().order(SortOrder.DESC));
        }
//        NativeSearchQuery builds = builder
//                .withQuery(booleanQueryBuilder)
//                .withPageable(PageRequest.of(page, size))
//                .build();
        SearchQuery searchQuery = builder
                .withQuery(booleanQueryBuilder)
                .withPageable(PageRequest.of(page, size))
                .build();

        Page<Product> result = productRepository.search(searchQuery);
        SearchResult searchResult = new SearchResult();
        List<SearchItem> list = result.getContent().stream().map(product -> {
            SearchItem searchItem = new SearchItem();
            searchItem.setProductId(product.getId());
            searchItem.setProductName(product.getTitle());
            searchItem.setSubTitle(product.getSubTitle());
            searchItem.setSalePrice(product.getPrice());
            searchItem.setProductImageBig(product.getImage());
            searchItem.setCategoryId(product.getCategoryId());
            return searchItem;
        }).collect(Collectors.toList());
        searchResult.setItemList(list);
        searchResult.setRecordCount(result.getTotalElements());
        searchResult.setTotalPages(result.getTotalPages());
        return searchResult;
    }
}
