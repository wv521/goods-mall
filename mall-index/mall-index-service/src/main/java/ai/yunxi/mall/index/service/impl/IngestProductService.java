package ai.yunxi.mall.index.service.impl;

import ai.yunxi.mall.index.domain.model.Product;
import ai.yunxi.mall.index.domain.repo.ProductRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;

@Service
@EnableBinding(Sink.class)
public class IngestProductService {

    @Resource
    private ProductRepository productRepository;

    private static final String DELETE = "delete";
    private static final String INSERT = "insert";
    private static final String UPDATE = "update";

    @StreamListener(Sink.INPUT)
    public void handle(String content) {
        ObjectMapper om = new ObjectMapper();
        // 解析binlog操作
        String operation = null;
        // 将Json解析成JsonNode
        JsonNode jsonNode = null;

        try {
            operation = om.readTree(content).path("type").asText();
            jsonNode = om.readTree(content).path("data");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        if (null != jsonNode) {
            Product product = new Product();
            product.setId(jsonNode.get("id").asLong());
            product.setTitle(jsonNode.get("title").asText());
            product.setSubTitle(jsonNode.get("sell_point").asText());
            product.setPrice(BigDecimal.valueOf(jsonNode.get("price").asDouble()));
            product.setImage(jsonNode.get("image").asText());
            product.setCategoryId(jsonNode.get("category_id").asInt());
            String created = jsonNode.get("created").asText();
            String updated = jsonNode.get("updated").asText();
            String parsePattern = "yyyy-MM-dd HH:mm:ss";
            try {
                product.setCreated(DateUtils.parseDate(created, parsePattern));
                product.setUpdated(DateUtils.parseDate(updated, parsePattern));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            switch (operation) {
                case INSERT:
                case UPDATE:
                    productRepository.save(product);
                    break;
                case DELETE:
                    productRepository.deleteById(product.getId());
                    break;
                default:
                    break;
            }
        }
    }
}
