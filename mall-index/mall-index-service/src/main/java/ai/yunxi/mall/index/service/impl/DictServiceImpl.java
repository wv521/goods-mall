package ai.yunxi.mall.index.service.impl;

import ai.yunxi.mall.common.Constants;
import ai.yunxi.mall.core.service.dto.Dict;
import ai.yunxi.mall.index.domain.entity.TbDict;
import ai.yunxi.mall.index.domain.entity.TbDictExample;
import ai.yunxi.mall.index.domain.mapper.TbDictMapper;
import ai.yunxi.mall.index.service.DictService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.BeanUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@DubboService
public class DictServiceImpl implements DictService {

    @Resource
    private TbDictMapper tbDictMapper;

    @Override
    public List<Dict> getDictList() {
        TbDictExample example = new TbDictExample();
        TbDictExample.Criteria criteria = example.createCriteria();
        //条件查询
        criteria.andTypeEqualTo(Constants.DICT_EXT);
        List<TbDict> list = tbDictMapper.selectByExample(example);
        return list.stream().map(tbDict -> {
            Dict dict = new Dict();
            BeanUtils.copyProperties(tbDict, dict);
            return dict;
        }).collect(Collectors.toList());
    }

    @Override
    public List<Dict> getStopList() {
        TbDictExample example = new TbDictExample();
        TbDictExample.Criteria criteria = example.createCriteria();
        //条件查询
        criteria.andTypeEqualTo(Constants.DICT_STOP);
        List<TbDict> list = tbDictMapper.selectByExample(example);
        return list.stream().map(tbDict -> {
            Dict dict = new Dict();
            BeanUtils.copyProperties(tbDict, dict);
            return dict;
        }).collect(Collectors.toList());
    }

    @Override
    public int addDict(Dict dict) {
        TbDict tbDict = new TbDict();
        BeanUtils.copyProperties(dict, tbDict);
        tbDictMapper.insert(tbDict);
        return 1;
    }

    @Override
    public int updateDict(Dict dict) {
        TbDict tbDict = new TbDict();
        BeanUtils.copyProperties(dict, tbDict);
        tbDictMapper.updateByPrimaryKey(tbDict);
        return 1;
    }

    @Override
    public int deleteDict(int id) {
        tbDictMapper.deleteByPrimaryKey(id);
        return 1;
    }
}
