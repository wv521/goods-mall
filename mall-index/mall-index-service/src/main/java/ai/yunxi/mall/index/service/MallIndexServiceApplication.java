package ai.yunxi.mall.index.service;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@SpringBootApplication
@MapperScan("ai.yunxi.mall.index.domain.mapper")
@EnableElasticsearchRepositories(basePackages = "ai.yunxi.mall.index.domain.repo")
@EnableDubbo(scanBasePackages = {"ai.yunxi.mall.index.service.impl"})
public class MallIndexServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MallIndexServiceApplication.class, args);
    }
}
