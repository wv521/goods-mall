./maxwell --host=192.168.0.101 --port=3306 --user=root --password=123456 \
          --producer=kafka --kafka_topic=shopping_mall_product \
          --kafka.bootstrap.servers=192.168.0.109:9092 \
          --filter='exclude:*.*,include:shopping_mall_product.*'