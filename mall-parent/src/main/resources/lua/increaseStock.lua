---
--- Generated by EmmyLua(https://github.com/EmmyLua)
--- Created by Yunxi.
--- DateTime: 2020/12/23 15:45
--- 取消恢复脚本
---

for i, v in pairs(KEYS) do
    if (redis.call('exists', KEYS[i]) == 1) then
        local recoveryNum = tonumber(ARGV[i]);
        redis.call('incrby', KEYS[i], recoveryNum);
    else
        return -100;
    end;
end;
return 1;