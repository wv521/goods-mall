local uri_args = ngx.req.get_uri_args()

--定义uri来保存链接请求，如：http://localhost/goods/111.html
local uri = ngx.var.uri;

--正则的替换
local productId, err = ngx.re.match(uri, '[0-9]+')
if productId then
    --ngx.print("makcj::", skuId[0])
else
    if err then
        ngx.log(ngx.ERR, "error: ", err)
        return
    end
    --ngx.print("match not found")
end

-- 定义lua本地缓存
local local_cache = ngx.shared.mycache

--换成的key k:v
local productCacheKey = "product_info_"..productId[0]
--从本地cache读取缓存
local productCache = local_cache:get(productCacheKey)

-- 如果缓存里面没有值
if productCache == "" or productCache == nil then
    local http = require("resty.http")
    local httpc = http.new()
    local resp, err = httpc:request_uri("http://localhost/",{
        method = "GET",
        path = "/front/goods/"..productId[0]
    })
    -- 返回数据放到productCache
    productCache = resp.body
    -- 把数据写回缓存
    local_cache:set(productCacheKey, productCache, 10 * 60)
end


local cjson = require("cjson")
-- 解析json
local productCacheJSON = cjson.decode(productCache)

-- 定义一个数据类型供静态页面使用
local context = {
    productId = productCacheJSON.productId,
    productName = productCacheJSON.productName,
    productPrice = productCacheJSON.price
}

-- ngx.say("ok")
local template = require("resty.template")
template.render("product.html", context)
