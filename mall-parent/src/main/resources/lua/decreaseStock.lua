for i, v in pairs(KEYS) do
    if (redis.call('exists', KEYS[i]) == 1) then
        local balanceNum = tonumber(redis.call('get', KEYS[i]));
        local purchaseNum = tonumber(ARGV[i]);
        if tonumber(balanceNum) == 0 or (balanceNum - purchaseNum) < 0 then
            return -200;
        end;
    else
        return -100;
    end;
end;

for i, v in pairs(KEYS) do
    local purchaseNum = tonumber(ARGV[i]);
    redis.call('incrby', KEYS[i], -purchaseNum);
end;
return 1;