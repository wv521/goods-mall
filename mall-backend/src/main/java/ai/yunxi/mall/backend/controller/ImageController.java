package ai.yunxi.mall.backend.controller;

import ai.yunxi.mall.common.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RestController
@Api(tags = "图片上传统一接口")
@RequestMapping("/backend/image")
public class ImageController {

    @RequestMapping(value = "/imageUpload",method = RequestMethod.POST)
    @ApiOperation(value = "WebUploader图片上传")
    public CommonResult uploadFile(@RequestParam("file") MultipartFile files,
                                                  HttpServletRequest request){
        String imagePath=null;
        // 文件保存路径

        return CommonResult.success(imagePath);
    }
}
