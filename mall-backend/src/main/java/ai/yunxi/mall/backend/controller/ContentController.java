package ai.yunxi.mall.backend.controller;

import ai.yunxi.mall.common.ServiceResult;
import ai.yunxi.mall.common.utils.CommonResult;
import ai.yunxi.mall.content.service.ContentService;
import ai.yunxi.mall.content.service.dto.PanelContent;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "板块内容管理")
@RequestMapping("/backend/content")
public class ContentController {

    final static Logger log = LoggerFactory.getLogger(ContentController.class);

    @DubboReference
    private ContentService contentService;

    @RequestMapping(value = "/list/{panelId}", method = RequestMethod.GET)
    @ApiOperation(value = "通过panelId获得板块内容列表")
    public ServiceResult getContentByCid(@PathVariable int panelId) {
        ServiceResult result = contentService.getPanelContentListByPanelId(panelId);
        return result;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ApiOperation(value = "添加板块内容")
    public CommonResult addContent(@ModelAttribute PanelContent panelContent) {
        contentService.addPanelContent(panelContent);
        return CommonResult.success(null);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ApiOperation(value = "编辑板块内容")
    public CommonResult updateContent(@ModelAttribute PanelContent panelContent) {
        contentService.updateContent(panelContent);
        return CommonResult.success(null);
    }

    @RequestMapping(value = "/del/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "删除板块内容")
    public CommonResult addContent(@PathVariable int[] ids) {
        for (int id : ids) {
            contentService.deletePanelContent(id);
        }

        return CommonResult.success(null);
    }
}
