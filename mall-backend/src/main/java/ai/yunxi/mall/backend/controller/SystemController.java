package ai.yunxi.mall.backend.controller;

import ai.yunxi.mall.common.ServiceResult;
import ai.yunxi.mall.common.utils.CommonResult;
import ai.yunxi.mall.core.service.SystemService;
import ai.yunxi.mall.core.service.dto.Base;
import ai.yunxi.mall.core.service.dto.Manager;
import ai.yunxi.mall.core.service.dto.OrderItem;
import ai.yunxi.mall.core.service.dto.ShiroFilter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(tags = "系统配置管理")
@RequestMapping(value = "/backend/sys")
public class SystemController {

    @DubboReference
    private SystemService systemService;

    @ApiOperation(value = "获取shiro过滤链配置")
    @RequestMapping(value = "/shiro/list", method = RequestMethod.GET)
    public ServiceResult getShiroList(@ModelAttribute Manager manager) {
        ServiceResult result = new ServiceResult();
        List<ShiroFilter> list = systemService.getShiroFilter();
        result.setData(list);
        result.setSuccess(true);
        return result;
    }

    @ApiOperation(value = "统计shiro过滤链数")
    @RequestMapping(value = "/shiro/count", method = RequestMethod.GET)
    public CommonResult<Object> getUserCount() {
        Long result = systemService.countShiroFilter();
        return CommonResult.success(result);
    }

    @ApiOperation(value = "添加shiro过滤链")
    @RequestMapping(value = "/shiro/add", method = RequestMethod.POST)
    public CommonResult<Object> addShiro(@ModelAttribute ShiroFilter shiroFilter) {
        systemService.addShiroFilter(shiroFilter);
        return CommonResult.success(null);
    }

    @ApiOperation(value = "更新shiro过滤链")
    @RequestMapping(value = "/shiro/update", method = RequestMethod.POST)
    public CommonResult<Object> updateShiro(@ModelAttribute ShiroFilter shiroFilter) {
        systemService.updateShiroFilter(shiroFilter);
        return CommonResult.success(null);
    }

    @ApiOperation(value = "删除shiro过滤链")
    @RequestMapping(value = "/shiro/del/{ids}", method = RequestMethod.DELETE)
    public CommonResult<Object> delShiro(@PathVariable int[] ids) {
        for (int id : ids) {
            systemService.deleteShiroFilter(id);
        }
        return CommonResult.success(null);
    }

    @ApiOperation(value = "获取基本设置")
    @RequestMapping(value = "/base", method = RequestMethod.GET)
    public CommonResult<Base> getBase() {
        Base base = systemService.getBase();
        return CommonResult.success(base);
    }

    @ApiOperation(value = "编辑基本设置")
    @RequestMapping(value = "/base/update", method = RequestMethod.POST)
    public CommonResult<Object> updateBase(@ModelAttribute Base base) {
        systemService.updateBase(base);
        return CommonResult.success(null);
    }

    @ApiOperation(value = "获取本周热销商品数据")
    @RequestMapping(value = "/weekHot", method = RequestMethod.GET)
    public CommonResult<OrderItem> getWeekHot() {
        OrderItem orderItem = systemService.getWeekHot();
        return CommonResult.success(orderItem);
    }
}
