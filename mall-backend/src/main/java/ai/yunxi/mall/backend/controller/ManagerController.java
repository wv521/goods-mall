package ai.yunxi.mall.backend.controller;

import ai.yunxi.mall.common.annotation.SystemControllerLog;
import ai.yunxi.mall.common.ServiceResult;
import ai.yunxi.mall.common.utils.ResultCode;
import ai.yunxi.mall.common.utils.CommonResult;
import ai.yunxi.mall.core.service.ManagerService;
import ai.yunxi.mall.core.service.dto.Manager;
import ai.yunxi.mall.core.service.dto.Permission;
import ai.yunxi.mall.core.service.dto.Role;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@RestController
@Api(tags = "管理员管理")
@RequestMapping("/backend/manager")
public class ManagerController {

    final static Logger log = LoggerFactory.getLogger(ManagerController.class);

    @DubboReference
    private ManagerService managerService;

    @ApiOperation(value = "用户登录")
    @SystemControllerLog(description = "登录系统")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public CommonResult login(String username, String password,
                              String challenge, String validate, String seccode,
                              HttpServletRequest request) {
        //自定义参数,可选择添加
        HashMap<String, String> param = new HashMap<String, String>();

        int gtResult = 1;
        // 加入验证过程

        if (gtResult == 1) {
            // 验证成功
            Subject subject = SecurityUtils.getSubject();
            //MD5加密
            String md5Pass = DigestUtils.md5DigestAsHex(password.getBytes());
            UsernamePasswordToken token = new UsernamePasswordToken(username, md5Pass);
            try {
                subject.login(token);
                return CommonResult.success(null);
            } catch (Exception e) {
                return CommonResult.error(ResultCode.FAILED, "用户名或密码错误");
            }
        } else {
            // 验证失败
            return CommonResult.error(ResultCode.FAILED, "验证失败");
        }
    }

    @ApiOperation(value = "退出登录")
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public CommonResult logout() {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return CommonResult.success(null);
    }

    @ApiOperation(value = "获取登录用户信息")
    @RequestMapping(value = "/userInfo", method = RequestMethod.GET)
    public CommonResult getUserInfo() {
        String username = SecurityUtils.getSubject().getPrincipal().toString();
        Manager manager = managerService.getUserByUsername(username);
        manager.setPassword(null);
        return CommonResult.success(manager);
    }

    @ApiOperation(value = "获取角色列表")
    @RequestMapping(value = "/roleList", method = RequestMethod.GET)
    public ServiceResult getRoleList() {
        ServiceResult result = managerService.getRoleList();
        return result;
    }

    @ApiOperation(value = "获取所有角色")
    @RequestMapping(value = "/getAllRoles", method = RequestMethod.GET)
    public CommonResult getAllRoles() {
        List<Role> list = managerService.getAllRoles();
        return CommonResult.success(list);
    }

    @ApiOperation(value = "判断角色是否已存在")
    @RequestMapping(value = "/roleName", method = RequestMethod.GET)
    public boolean roleName(String name) {
        if (managerService.getRoleByRoleName(name) != null) {
            return false;
        }
        return true;
    }

    @ApiOperation(value = "判断编辑角色是否已存在")
    @RequestMapping(value = "/edit/roleName/{id}", method = RequestMethod.GET)
    public boolean roleName(@PathVariable int id, String name) {
        return managerService.getRoleByEditName(id, name);
    }

    @ApiOperation(value = "添加角色")
    @RequestMapping(value = "/addRole", method = RequestMethod.POST)
    public CommonResult addRole(@ModelAttribute Role role) {
        managerService.addRole(role);
        return CommonResult.success(null);
    }

    @ApiOperation(value = "更新角色")
    @RequestMapping(value = "/updateRole", method = RequestMethod.POST)
    public CommonResult updateRole(@ModelAttribute Role role) {
        managerService.updateRole(role);
        return CommonResult.success(null);
    }

    @ApiOperation(value = "删除角色")
    @RequestMapping(value = "/delRole/{ids}", method = RequestMethod.DELETE)
    public CommonResult delRole(@PathVariable int[] ids) {
        for (int id : ids) {
            int result = managerService.deleteRole(id);
            if (result == 0) {
                return CommonResult.error(ResultCode.FAILED,
                        "id为" + id + "的角色被使用中，不能删除！");
            }
        }
        return CommonResult.success(null);
    }

    @ApiOperation(value = "统计角色数")
    @RequestMapping(value = "/roleCount", method = RequestMethod.GET)
    public CommonResult getRoleCount() {
        Long result = managerService.countRole();
        return CommonResult.success(null);
    }

    @ApiOperation(value = "获取权限列表")
    @RequestMapping(value = "/permissionList", method = RequestMethod.GET)
    public ServiceResult getPermissionList() {
        ServiceResult result = managerService.getPermissionList();
        return result;
    }

    @ApiOperation(value = "添加权限")
    @RequestMapping(value = "/addPermission", method = RequestMethod.POST)
    public CommonResult addPermission(@ModelAttribute Permission permission) {
        managerService.addPermission(permission);
        return CommonResult.success(null);
    }

    @ApiOperation(value = "更新权限")
    @RequestMapping(value = "/updatePermission", method = RequestMethod.POST)
    public CommonResult updatePermission(@ModelAttribute Permission permission) {
        managerService.updatePermission(permission);
        return CommonResult.success(null);
    }

    @ApiOperation(value = "删除权限")
    @RequestMapping(value = "/delPermission/{ids}", method = RequestMethod.DELETE)
    public CommonResult delPermission(@PathVariable int[] ids) {
        for (int id : ids) {
            managerService.deletePermission(id);
        }
        return CommonResult.success(null);
    }

    @ApiOperation(value = "统计权限数")
    @RequestMapping(value = "/permissionCount", method = RequestMethod.GET)
    public CommonResult getPermissionCount() {
        Long result = managerService.countPermission();
        return CommonResult.success(null);
    }

    @ApiOperation(value = "获取用户列表")
    @RequestMapping(value = "/userList", method = RequestMethod.GET)
    public ServiceResult getUserList() {
        ServiceResult result = managerService.getUserList();
        return result;
    }

    @ApiOperation(value = "判断用户名是否存在")
    @RequestMapping(value = "/username", method = RequestMethod.GET)
    public boolean getUserByName(String username) {
        return managerService.getUserByName(username);
    }

    @ApiOperation(value = "判断手机是否存在")
    @RequestMapping(value = "/phone", method = RequestMethod.GET)
    public boolean getUserByPhone(String phone) {
        return managerService.getUserByPhone(phone);
    }

    @ApiOperation(value = "判断邮箱是否存在")
    @RequestMapping(value = "/email", method = RequestMethod.GET)
    public boolean getUserByEmail(String email) {
        return managerService.getUserByEmail(email);
    }

    @ApiOperation(value = "添加用户")
    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public CommonResult addUser(@ModelAttribute Manager manager) {
        managerService.addUser(manager);
        return CommonResult.success(null);
    }

    @ApiOperation(value = "更新用户")
    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    public CommonResult updateUser(@ModelAttribute Manager manager) {
        managerService.updateUser(manager);
        return CommonResult.success(null);
    }

    @ApiOperation(value = "判断编辑用户名是否存在")
    @RequestMapping(value = "/edit/username/{id}", method = RequestMethod.GET)
    public boolean getUserByEditName(@PathVariable Long id, String username) {
        return managerService.getUserByEditName(id, username);
    }

    @ApiOperation(value = "判断编辑手机是否存在")
    @RequestMapping(value = "/edit/phone/{id}", method = RequestMethod.GET)
    public boolean getUserByEditPhone(@PathVariable Long id, String phone) {
        return managerService.getUserByEditPhone(id, phone);
    }

    @ApiOperation(value = "判断编辑用户名是否存在")
    @RequestMapping(value = "/edit/email/{id}", method = RequestMethod.GET)
    public boolean getUserByEditEmail(@PathVariable Long id, String email) {
        return managerService.getUserByEditEmail(id, email);
    }

    @ApiOperation(value = "停用用户")
    @RequestMapping(value = "/stop/{id}", method = RequestMethod.PUT)
    public CommonResult stopUser(@PathVariable Long id) {
        managerService.changeUserState(id, 0);
        return CommonResult.success(null);
    }

    @ApiOperation(value = "启用用户")
    @RequestMapping(value = "/start/{id}", method = RequestMethod.PUT)
    public CommonResult startUser(@PathVariable Long id) {
        managerService.changeUserState(id, 1);
        return CommonResult.success(null);
    }

    @ApiOperation(value = "修改用户密码")
    @RequestMapping(value = "/changePass", method = RequestMethod.POST)
    public CommonResult changePass(@ModelAttribute Manager manager) {
        managerService.changePassword(manager);
        return CommonResult.success(null);
    }

    @ApiOperation(value = "删除用户")
    @RequestMapping(value = "/delUser/{ids}", method = RequestMethod.DELETE)
    public CommonResult delUser(@PathVariable Long[] ids) {
        for (Long id : ids) {
            managerService.deleteUser(id);
        }
        return CommonResult.success(null);
    }

    @ApiOperation(value = "统计用户数")
    @RequestMapping(value = "/userCount", method = RequestMethod.GET)
    public CommonResult getUserCount() {
        Long result = managerService.countUser();
        return CommonResult.success(null);
    }
}
