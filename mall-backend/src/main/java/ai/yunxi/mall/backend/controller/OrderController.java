package ai.yunxi.mall.backend.controller;

import ai.yunxi.mall.common.ServiceResult;
import ai.yunxi.mall.common.utils.CommonResult;
import ai.yunxi.mall.core.service.OrderService;
import ai.yunxi.mall.core.service.dto.Order;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
@Api(tags = "订单管理")
@RequestMapping("/backend/order")
public class OrderController {

    @DubboReference
    private OrderService orderService;

    @ApiOperation(value = "获取订单列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ServiceResult getOrderList(int draw, int start, int length,
                                      @RequestParam("search[value]") String search,
                                      @RequestParam("order[0][column]") int orderCol,
                                      @RequestParam("order[0][dir]") String orderDir) {
        //获取客户端需要排序的列
        String[] cols = {"checkbox", "order_id", "payment", "shipping_code", "user_id",
                "buyer_nick", "create_time", "update_time", "payment_time", "close_time",
                "end_time", "status"};
        String orderColumn = cols[orderCol];
        //默认排序列
        if (orderColumn == null) {
            orderColumn = "create_time";
        }
        //获取排序方式 默认为desc(asc)
        if (orderDir == null) {
            orderDir = "desc";
        }
        ServiceResult result = orderService.getOrderList(draw, start, length, search,
                orderColumn, orderDir);
        return result;
    }

    @ApiOperation(value = "获取订单总数")
    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public CommonResult<Object> getOrderCount() {
        Long result = orderService.countOrder();
        return CommonResult.success(result);
    }

    @ApiOperation(value = "获取订单详情")
    @RequestMapping(value = "/detail/{orderId}", method = RequestMethod.GET)
    public CommonResult<Order> getOrderDetail(@PathVariable Long orderId) {
        Order order = orderService.getOrder(orderId);
        return CommonResult.success(order);
    }

    @ApiOperation(value = "订单备注")
    @RequestMapping(value = "/remark", method = RequestMethod.POST)
    public CommonResult<Object> remark(@RequestParam Long orderId,
                                              @RequestParam(required = false) String message) {
        orderService.remark(orderId, message);
        return CommonResult.success(null);
    }

    @ApiOperation(value = "订单发货")
    @RequestMapping(value = "/deliver", method = RequestMethod.POST)
    public CommonResult<Object> deliver(@RequestParam Long orderId,
                                               @RequestParam String shippingName,
                                               @RequestParam String shippingCode,
                                               @RequestParam BigDecimal postFee) {
        orderService.deliver(orderId, shippingName, shippingCode, postFee);
        return CommonResult.success(null);
    }

    @ApiOperation(value = "订单取消")
    @RequestMapping(value = "/cancel/{orderId}", method = RequestMethod.GET)
    public CommonResult<Object> cancelOrderByAdmin(@PathVariable Long orderId) {
        orderService.cancelByAdmin(orderId);
        return CommonResult.success(null);
    }
}
