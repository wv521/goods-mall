package ai.yunxi.mall.backend.controller;

import ai.yunxi.mall.common.ServiceResult;
import ai.yunxi.mall.common.utils.CommonResult;
import ai.yunxi.mall.user.service.MemberService;
import ai.yunxi.mall.user.service.dto.Member;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "会员管理")
@RequestMapping("/backend/member")
public class MemberController {
    final static Logger log = LoggerFactory.getLogger(MemberController.class);

    @DubboReference
    private MemberService memberService;

    @ApiOperation(value = "分页多条件搜索获取会员列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ServiceResult getMemberList(int draw, int start, int length, String searchKey,
                                       String minDate, String maxDate,
                                       @RequestParam("search[value]") String search,
                                       @RequestParam("order[0][column]") int orderCol,
                                       @RequestParam("order[0][dir]") String orderDir) {
        //获取客户端需要排序的列
        String[] cols = {"checkbox", "id", "username", "sex", "phone", "email", "address",
                "created", "updated", "state"};
        String orderColumn = cols[orderCol];
        //默认排序列
        if (orderColumn == null) {
            orderColumn = "created";
        }
        //获取排序方式 默认为desc(asc)
        if (orderDir == null) {
            orderDir = "desc";
        }
        if (!search.isEmpty()) {
            searchKey = search;
        }
        ServiceResult result = memberService.getList(draw, start, length, searchKey,
                minDate, maxDate, orderColumn, orderDir);
        return result;
    }

    @ApiOperation(value = "分页多条件搜索已删除会员列表")
    @RequestMapping(value = "/list/remove", method = RequestMethod.GET)
    public ServiceResult getDelMemberList(int draw, int start, int length, String searchKey,
                                          String minDate, String maxDate,
                                          @RequestParam("search[value]") String search,
                                          @RequestParam("order[0][column]") int orderCol,
                                          @RequestParam("order[0][dir]") String direction) {
        //获取客户端需要排序的列
        String[] cols = {"checkbox", "id", "username", "sex", "phone", "email", "address",
                "created", "updated", "state"};
        String orderColumn = cols[orderCol];
        //默认排序列
        if (orderColumn == null) {
            orderColumn = "created";
        }
        //获取排序方式 默认为desc(asc)
        if (direction == null) {
            direction = "desc";
        }
        if (!search.isEmpty()) {
            searchKey = search;
        }
        ServiceResult result = memberService.getRemoveList(draw, start, length, searchKey,
                minDate, maxDate, orderColumn, direction);
        return result;
    }

    @ApiOperation(value = "获得总会员数目")
    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public ServiceResult getMemberCount() {
        return memberService.getMemberCount();
    }

    @ApiOperation(value = "获得移除总会员数目")
    @RequestMapping(value = "/count/remove", method = RequestMethod.GET)
    public ServiceResult getRemoveMemberCount() {
        return memberService.getRemoveCount();
    }

    @ApiOperation(value = "添加会员")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public CommonResult createMember(@ModelAttribute Member member) {
        Member newMember = memberService.addMember(member);
        return CommonResult.success(newMember);
    }

    @ApiOperation(value = "停用会员")
    @RequestMapping(value = "/stop/{id}", method = RequestMethod.PUT)
    public CommonResult stopMember(@PathVariable Long id) {
        Member member = memberService.updateState(id, 0);
        return CommonResult.success(member);
    }

    @ApiOperation(value = "启用会员")
    @RequestMapping(value = "/start/{ids}", method = RequestMethod.PUT)
    public CommonResult<Member> startMember(@PathVariable Long[] ids) {
        for (Long id : ids) {
            memberService.updateState(id, 1);
        }
        return CommonResult.success(null);
    }

    @ApiOperation(value = "移除会员")
    @RequestMapping(value = "/remove/{ids}", method = RequestMethod.PUT)
    public CommonResult<Member> removeMember(@PathVariable Long[] ids) {
        for (Long id : ids) {
            memberService.updateState(id, 2);
        }
        return CommonResult.success(null);
    }

    @ApiOperation(value = "彻底删除会员")
    @RequestMapping(value = "/del/{ids}", method = RequestMethod.DELETE)
    public CommonResult<Member> deleteMember(@PathVariable Long[] ids) {
        for (Long id : ids) {
            memberService.deleteMember(id);
        }
        return CommonResult.success(null);
    }

    @ApiOperation(value = "修改会员密码")
    @RequestMapping(value = "/changePass/{id}", method = RequestMethod.POST)
    public CommonResult<Member> changeMemberPassword(@PathVariable Long id, @ModelAttribute Member member) {
        Member newMember = memberService.changePassword(id, member);
        return CommonResult.success(newMember);
    }

    @ApiOperation(value = "修改会员信息")
    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    public CommonResult<Member> updateMember(@PathVariable Long id, @ModelAttribute Member member) {
        Member newMember = memberService.updateMember(id, member);
        return CommonResult.success(newMember);
    }

    @ApiOperation(value = "通过ID获取会员信息")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public CommonResult<Member> getMemberById(@PathVariable Long id) {
        Member member = memberService.getById(id);
        return CommonResult.success(member);
    }

    @ApiOperation(value = "验证注册名是否存在")
    @RequestMapping(value = "/username", method = RequestMethod.GET)
    public Boolean validateUsername(String username) {
        if (memberService.getByUsername(username) != null) {
            return false;
        }
        return true;
    }

    @ApiOperation(value = "验证注册手机是否存在")
    @RequestMapping(value = "/phone", method = RequestMethod.GET)
    public Boolean validatePhone(String phone) {
        if (memberService.getByPhone(phone) != null) {
            return false;
        }
        return true;
    }

    @ApiOperation(value = "验证注册邮箱是否存在")
    @RequestMapping(value = "/email", method = RequestMethod.GET)
    public Boolean validateEmail(String email) {
        if (memberService.getByEmail(email) != null) {
            return false;
        }
        return true;
    }

    @ApiOperation(value = "验证编辑用户名是否存在")
    @RequestMapping(value = "/edit/{id}/username", method = RequestMethod.GET)
    public Boolean validateEditUsername(@PathVariable Long id, String username) {
        if (memberService.getByEditUsername(id, username) != null) {
            return false;
        }
        return true;
    }

    @ApiOperation(value = "验证编辑手机是否存在")
    @RequestMapping(value = "/edit/{id}/phone", method = RequestMethod.GET)
    public Boolean validateEditPhone(@PathVariable Long id, String phone) {
        if (memberService.getByEditPhone(id, phone) != null) {
            return false;
        }
        return true;
    }

    @ApiOperation(value = "验证编辑邮箱是否存在")
    @RequestMapping(value = "/edit/{id}/email", method = RequestMethod.GET)
    public Boolean validateEditEmail(@PathVariable Long id, String email) {
        if (memberService.getByEditEmail(id, email) != null) {
            return false;
        }
        return true;
    }
}
