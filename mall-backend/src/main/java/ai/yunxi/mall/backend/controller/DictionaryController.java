package ai.yunxi.mall.backend.controller;

import ai.yunxi.mall.common.Constants;
import ai.yunxi.mall.common.ServiceResult;
import ai.yunxi.mall.common.redis.JedisClient;
import ai.yunxi.mall.common.utils.CommonResult;
import ai.yunxi.mall.core.service.dto.Dict;
import ai.yunxi.mall.index.service.DictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@Api(tags = "词典库")
@RequestMapping(value = "/backend/dict")
public class DictionaryController {

    @DubboReference
    private DictService dictService;

    @Autowired
    private JedisClient jedisClient;

    @ApiOperation(value = "获得所有扩展词库")
    @RequestMapping(value = "/extList", method = RequestMethod.GET)
    public String getDictExtList(HttpServletResponse response) {
        String result = "";
        String v = jedisClient.get(Constants.EXT_KEY);
        if (StringUtils.isNotBlank(v)) {
            return v;
        }
        List<Dict> list = dictService.getDictList();
        for (Dict dict : list) {
            result += dict.getDict() + "\n";
        }
        if (StringUtils.isNotBlank(result)) {
            jedisClient.set(Constants.EXT_KEY, result);
        }
        response.addHeader(Constants.LAST_MODIFIED, jedisClient.get(Constants.LAST_MODIFIED));
        response.addHeader(Constants.ETAG, jedisClient.get(Constants.ETAG));
        return result;
    }

    @ApiOperation(value = "获得所有扩展词库")
    @RequestMapping(value = "/stopList", method = RequestMethod.GET)
    public String getStopDictList(HttpServletResponse response) {
        String result = "";
        String v = jedisClient.get(Constants.STOP_KEY);
        if (StringUtils.isNotBlank(v)) {
            return v;
        }
        List<Dict> list = dictService.getStopList();
        for (Dict dict : list) {
            result += dict.getDict() + "\n";
        }
        if (StringUtils.isNotBlank(result)) {
            jedisClient.set(Constants.STOP_KEY, result);
        }
        response.addHeader(Constants.LAST_MODIFIED, jedisClient.get(Constants.LAST_MODIFIED));
        response.addHeader(Constants.ETAG, jedisClient.get(Constants.ETAG));
        return result;
    }

    @ApiOperation(value = "获得所有扩展词库")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ServiceResult getDictList() {
        ServiceResult result = new ServiceResult();
        List<Dict> list = dictService.getDictList();
        result.setData(list);
        result.setSuccess(true);
        return result;
    }

    @ApiOperation(value = "获得所有停用词库")
    @RequestMapping(value = "/stop/list", method = RequestMethod.GET)
    public ServiceResult getStopList() {
        ServiceResult result = new ServiceResult();
        List<Dict> list = dictService.getStopList();
        result.setData(list);
        result.setSuccess(true);
        return result;
    }

    @ApiOperation(value = "添加词典")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public CommonResult addDict(@ModelAttribute Dict dict) {
        dictService.addDict(dict);
        update();
        return CommonResult.success(null);
    }

    @ApiOperation(value = "编辑词典")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public CommonResult updateDict(@ModelAttribute Dict dict) {
        dictService.updateDict(dict);
        update();
        return CommonResult.success(null);
    }

    @ApiOperation(value = "删除词典")
    @RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
    public CommonResult deleteDict(@PathVariable int[] ids) {
        for (int id : ids) {
            dictService.deleteDict(id);
        }
        update();
        return CommonResult.success(null);
    }

    public void update() {
        //更新词典标识
        jedisClient.set(Constants.LAST_MODIFIED, String.valueOf(System.currentTimeMillis()));
        jedisClient.set(Constants.ETAG, String.valueOf(System.currentTimeMillis()));
        //更新缓存
        jedisClient.del(Constants.EXT_KEY);
        jedisClient.del(Constants.STOP_KEY);
    }
}
