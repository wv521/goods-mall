package ai.yunxi.mall.backend.shiro;

import ai.yunxi.mall.core.service.ManagerService;
import ai.yunxi.mall.core.service.dto.Manager;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class Realm extends AuthorizingRealm {

    private static final Logger log = LoggerFactory.getLogger(Realm.class);

    @Autowired
    private ManagerService managerService;

    /**
     * 返回权限信息
     *
     * @param principal
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principal) {
        //获取用户名
        String username = principal.getPrimaryPrincipal().toString();
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        //获得授权角色
        authorizationInfo.setRoles(managerService.getRoles(username));
        //获得授权权限
        authorizationInfo.setStringPermissions(managerService.getPermissions(username));
        return authorizationInfo;
    }

    /**
     * 先执行登录验证
     *
     * @param token
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token)
            throws AuthenticationException {
        //获取用户名密码
        String username = token.getPrincipal().toString();
        Manager manager = managerService.getUserByUsername(username);
        if (manager != null) {
            //得到用户账号和密码存放到authenticationInfo中用于Controller层的权限判断 第三个参数随意不能为null
            AuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(manager.getUsername(),
                    manager.getPassword(),
                    manager.getUsername());
            return authenticationInfo;
        } else {
            return null;
        }
    }
}
