package ai.yunxi.mall.backend.controller;

import ai.yunxi.mall.common.ServiceResult;
import ai.yunxi.mall.common.utils.CommonResult;
import ai.yunxi.mall.index.service.SearchProductService;
import ai.yunxi.mall.product.service.ProductService;
import ai.yunxi.mall.product.service.dto.Product;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "商品列表信息")
@RequestMapping(value = "/backend/product")
public class ProductController {

    private final static Logger log = LoggerFactory.getLogger(ProductController.class);

    @DubboReference
    private ProductService productService;
    @DubboReference
    private SearchProductService searchProductService;

    @ApiOperation(value = "通过ID获取商品")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public CommonResult<Product> getById(@PathVariable Long id) {
        Product product = productService.getById(id);
        return CommonResult.success(product);
    }

    @ApiOperation(value = "分页搜索排序获取商品列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ServiceResult getList(int draw, int start, int length, int cid,
                                 @RequestParam("search[value]") String search,
                                 @RequestParam("order[0][column]") int orderCol,
                                 @RequestParam("order[0][dir]") String orderDir,
                                 String searchItem, String minDate, String maxDate) {
        //获取客户端需要排序的列
        String[] cols = {"checkbox", "id", "image", "title", "sell_point", "price",
                "created", "updated", "status"};
        String orderColumn = cols[orderCol];
        if (orderColumn == null) {
            orderColumn = "created";
        }
        //获取排序方式 默认为desc(asc)
        if (orderDir == null) {
            orderDir = "desc";
        }
        ServiceResult result = productService.getList(draw, start, length, cid, search,
                orderColumn, orderDir);
        return result;
    }

    @ApiOperation(value = "多条件分页搜索排序获取商品列表")
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ServiceResult getItemSearchList(int draw, int start, int length, int cid, String searchKey,
                                           String minDate, String maxDate,
                                           @RequestParam("search[value]") String search,
                                           @RequestParam("order[0][column]") int orderCol,
                                           @RequestParam("order[0][dir]") String orderDir) {
        //获取客户端需要排序的列
        String[] cols = {"checkbox", "id", "image", "title", "sell_point", "price",
                "created", "updated", "status"};
        //默认排序列
        String orderColumn = cols[orderCol];
        if (orderColumn == null) {
            orderColumn = "created";
        }
        //获取排序方式 默认为desc(asc)
        if (orderDir == null) {
            orderDir = "desc";
        }
        if (!search.isEmpty()) {
            searchKey = search;
        }
        ServiceResult result = productService.searchList(draw, start, length, cid, searchKey,
                minDate, maxDate, orderColumn, orderDir);
        return result;
    }

    @ApiOperation(value = "获得商品总数目")
    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public ServiceResult getAllItemCount() {
        ServiceResult result = productService.getAllCount();
        return result;
    }

    @ApiOperation(value = "商品下架")
    @RequestMapping(value = "/remove/{id}", method = RequestMethod.PUT)
    public CommonResult<Product> remove(@PathVariable Long id) {
        Product product = productService.updateState(id, 0);
        return CommonResult.success(product);
    }

    @ApiOperation(value = "商品发布")
    @RequestMapping(value = "/upper/{id}", method = RequestMethod.PUT)
    public CommonResult<Product> upper(@PathVariable Long id) {
        Product product = productService.updateState(id, 1);
        return CommonResult.success(product);
    }

    @ApiOperation(value = "删除商品")
    @RequestMapping(value = "delete/{ids}", method = RequestMethod.DELETE)
    public CommonResult<Product> delete(@PathVariable Long[] ids) {
        for (Long id : ids) {
            productService.deleteProduct(id);
        }
        return CommonResult.success(null);
    }

    @ApiOperation(value = "添加商品")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public CommonResult<Product> add(Product product) {
        Product p = productService.addProduct(product);
        return CommonResult.success(p);
    }

    @ApiOperation(value = "编辑商品")
    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    public CommonResult<Product> update(@PathVariable Long id, Product product) {
        Product p = productService.updateProduct(id, product);
        return CommonResult.success(p);
    }

    @ApiOperation(value = "导入商品索引至ES")
    @RequestMapping(value = "/item/importIndex", method = RequestMethod.GET)
    public CommonResult<Object> importIndex() {
        //searchProductService.importAllItems();
        return CommonResult.success(null);
    }
}
