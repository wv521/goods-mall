package ai.yunxi.mall.backend.controller;

import ai.yunxi.mall.common.utils.CommonResult;
import ai.yunxi.mall.core.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "订单")
@RequestMapping("/backend/pay")
public class PayController {

    private final static Logger log = LoggerFactory.getLogger(PayController.class);

    @DubboReference
    private OrderService orderService;

    @ApiOperation(value = "支付审核通过")
    @RequestMapping(value = "/pass", method = RequestMethod.GET)
    public CommonResult<Object> payPass(String tokenName, String token, String id) {
        return CommonResult.success("处理成功");
    }

    @ApiOperation(value = "支付审核驳回")
    @RequestMapping(value = "/back", method = RequestMethod.GET)
    public CommonResult<Object> backPay(String tokenName, String token, String id) {
        return CommonResult.success("处理成功");
    }

    @ApiOperation(value = "支付审核通过但不展示")
    @RequestMapping(value = "/passNotShow", method = RequestMethod.GET)
    public CommonResult<Object> payNotShow(String tokenName, String token, String id) {
        return CommonResult.success("处理成功");
    }
}
