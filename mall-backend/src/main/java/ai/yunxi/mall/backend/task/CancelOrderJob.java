package ai.yunxi.mall.backend.task;

import ai.yunxi.mall.core.service.OrderService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class CancelOrderJob {

    final static Logger log = LoggerFactory.getLogger(CancelOrderJob.class);

    @DubboReference
    private OrderService orderService;

    /**
     * 每1个小时判断订单是否失效
     */
    @Scheduled(cron = "0 0 */1 * * ?")
    public void run() {
        log.info("执行自动取消订单定时任务");
    }
}
