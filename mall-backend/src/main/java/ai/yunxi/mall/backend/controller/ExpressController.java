package ai.yunxi.mall.backend.controller;

import ai.yunxi.mall.common.ServiceResult;
import ai.yunxi.mall.common.utils.CommonResult;
import ai.yunxi.mall.core.service.ExpressService;
import ai.yunxi.mall.core.service.dto.Express;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(tags = "快递")
@RequestMapping("/backend/express")
public class ExpressController {

    @DubboReference
    private ExpressService expressService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获得所有快递")
    public ServiceResult addressList() {
        ServiceResult result = new ServiceResult();
        List<Express> list = expressService.getExpressList();
        result.setData(list);
        result.setSuccess(true);
        return result;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ApiOperation(value = "添加快递")
    public CommonResult<Object> addExpress(@ModelAttribute Express express) {
        expressService.addExpress(express);
        return CommonResult.success(null);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ApiOperation(value = "编辑快递")
    public CommonResult<Object> updateAddress(@ModelAttribute Express express) {
        expressService.updateExpress(express);
        return CommonResult.success(null);
    }

    @RequestMapping(value = "/del/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "删除快递")
    public CommonResult<Object> delAddress(@PathVariable int[] ids) {
        for (int id : ids) {
            expressService.deleteExpress(id);
        }
        return CommonResult.success(null);
    }
}
