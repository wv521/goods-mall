package ai.yunxi.mall.backend.controller;

import ai.yunxi.mall.common.TreeNode;
import ai.yunxi.mall.common.utils.CommonResult;
import ai.yunxi.mall.product.service.CategoryService;
import ai.yunxi.mall.product.service.dto.Category;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(tags = "商品分类信息")
@RequestMapping(value = "/backend/category")
public class CategoryController {

    @DubboReference
    private CategoryService categoryService;

    @ApiOperation(value = "通过父ID获取商品分类列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<TreeNode> getList(@RequestParam(name = "id", defaultValue = "0") int parentId) {
        List<TreeNode> list = categoryService.getList(parentId);
        return list;
    }

    @ApiOperation(value = "添加商品分类")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public CommonResult addCategory(@ModelAttribute Category category) {
        categoryService.addCategory(category);
        return CommonResult.success(null);
    }

    @ApiOperation(value = "编辑商品分类")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public CommonResult updateCategory(@ModelAttribute Category category) {
        categoryService.updateCategory(category);
        return CommonResult.success(null);
    }

    @ApiOperation(value = "删除商品分类")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public CommonResult deleteCategory(@PathVariable Long id) {
        categoryService.deleteCategory(id);
        return CommonResult.success(null);
    }
}
