package ai.yunxi.mall.backend.controller;

import ai.yunxi.mall.common.utils.CommonResult;
import ai.yunxi.mall.content.service.ContentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "缓存管理")
@RequestMapping(value = "/backend/redis")
public class RedisController {

    @DubboReference
    private ContentService contentService;

    @ApiOperation(value = "获取首页缓存")
    @RequestMapping(value = "/index/list", method = RequestMethod.GET)
    public CommonResult<Object> getIndexRedis() {
        String json = contentService.getIndexRedis();
        return CommonResult.success(json);
    }

    @ApiOperation(value = "刷新首页缓存")
    @RequestMapping(value = "/index/update", method = RequestMethod.GET)
    public CommonResult<Object> updateIndexRedis() {
        contentService.updateIndexRedis();
        return CommonResult.success(null);
    }

    @ApiOperation(value = "获取推荐板块缓存")
    @RequestMapping(value = "/recommend/list", method = RequestMethod.GET)
    public CommonResult<Object> getRecommendRedis() {
        String json = contentService.getRecommendRedis();
        return CommonResult.success(json);
    }

    @ApiOperation(value = "刷新推荐板块缓存")
    @RequestMapping(value = "/recommend/update", method = RequestMethod.GET)
    public CommonResult<Object> updateRecommendRedis() {
        contentService.updateRecommendRedis();
        return CommonResult.success(null);
    }
}
