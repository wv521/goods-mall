package ai.yunxi.mall.backend.controller;

import ai.yunxi.mall.common.TreeNode;
import ai.yunxi.mall.common.utils.CommonResult;
import ai.yunxi.mall.content.service.PanelService;
import ai.yunxi.mall.content.service.dto.Panel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(tags = "板块列表")
@RequestMapping("/backend")
public class PanelController {

    private final static Logger log = LoggerFactory.getLogger(PanelController.class);

    @DubboReference
    private PanelService panelService;

    @RequestMapping(value = "/index/list", method = RequestMethod.GET)
    @ApiOperation(value = "获得首页板块列表不含轮播")
    public List<TreeNode> getIndexPanel() {
        List<TreeNode> list = panelService.getPanelList(0, false);
        return list;
    }

    @RequestMapping(value = "/indexAll/list", method = RequestMethod.GET)
    @ApiOperation(value = "获得首页板块列表含轮播")
    public List<TreeNode> getAllIndexPanel() {
        List<TreeNode> list = panelService.getPanelList(0, true);
        return list;
    }

    @RequestMapping(value = "/indexBanner/list", method = RequestMethod.GET)
    @ApiOperation(value = "获得首页轮播板块列表")
    public List<TreeNode> getIndexBannerPanel() {
        List<TreeNode> list = panelService.getPanelList(-1, true);
        return list;
    }

    @RequestMapping(value = "/other/list", method = RequestMethod.GET)
    @ApiOperation(value = "获得其它添加板块")
    public List<TreeNode> getRecommendPanel() {
        List<TreeNode> list = panelService.getPanelList(1, false);
        list.addAll(panelService.getPanelList(2, false));
        return list;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ApiOperation(value = "添加板块")
    public CommonResult<Object> addContentCategory(@ModelAttribute Panel panel) {
        panelService.addPanel(panel);
        return CommonResult.success(null);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ApiOperation(value = "编辑内容分类")
    public CommonResult<Object> updateContentCategory(@ModelAttribute Panel panel) {
        panelService.updatePanel(panel);
        return CommonResult.success(null);
    }

    @RequestMapping(value = "/del/{ids}", method = RequestMethod.DELETE)
    @ApiOperation(value = "删除内容分类")
    public CommonResult<Object> deleteContentCategory(@PathVariable int[] ids) {
        for (int id : ids) {
            panelService.deletePanel(id);
        }
        return CommonResult.success(null);
    }
}
